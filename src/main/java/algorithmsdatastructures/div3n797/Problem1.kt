package algorithmsdatastructures.div3n797

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val br = BufferedReader(InputStreamReader(System.`in`))
    br.use {
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            val numberOfBlocks = br.readLine().toInt()
            var first = numberOfBlocks / 3 + 1
            var second = first - 1
            var third = numberOfBlocks - first - second
            if (numberOfBlocks % 3 != 0) {
                val reminder = numberOfBlocks % 3
                if (reminder == 1) {
                    first++
                    third--
                } else {
                    first++
                    second++
                    third -= 2
                }
            }
            sb.appendLine("$second $first $third")
        }
        println(sb)
    }
}
