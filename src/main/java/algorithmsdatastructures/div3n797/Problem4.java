package algorithmsdatastructures.div3n797;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem4 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var k = Integer.parseInt(br.readLine().split(" ")[1]);
                var chars = br.readLine().toCharArray();
                sb.append(findMinimum(k, chars) + "\n");
            }
            System.out.println(sb);
        }
    }

    private static int findMinimum(int k, char[] chars) {
        if (chars.length < k) return 0;
        var prefixes = new int[chars.length + 1];
        for (int i = 1; i <= chars.length; i++) {
            prefixes[i] = prefixes[i - 1] + (chars[i-1] == 'W' ? 1 : 0);
        }

        var ans = Integer.MAX_VALUE;
        for (int i = k; i < prefixes.length; i++) {
            ans = Math.min(ans, prefixes[i] - prefixes[i - k]);
        }
        return ans;
    }
}
