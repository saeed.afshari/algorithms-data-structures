package algorithmsdatastructures.div3n797

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val br = BufferedReader(InputStreamReader(System.`in`))
    br.use {
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            br.readLine()
            val a = br.readLine().split(" ").map { it.toInt() }
            val b = br.readLine().split(" ").map { it.toInt() }

            var flag = false
            for (i in a.indices) {
                if (a[i] < b[i]) {
                    flag = true
                    break
                }
            }
            if (flag) {
                sb.appendLine("NO")
                return@repeat
            }

            var diff = Int.MIN_VALUE
            for (i in a.indices) {
                if (diff < (a[i] - b[i])) {
                    diff = a[i] - b[i]
                }
            }
            if (diff == Int.MIN_VALUE) {
                sb.appendLine("YES")
            } else {
                var flag = false
                for (i in a.indices) {
                    if (b[i] == 0) continue
                    if (a[i] - b[i] != diff) {
                        flag = true
                        break
                    }
                }
                if (flag) {
                    sb.appendLine("NO")
                } else {
                    sb.appendLine("YES")
                }
            }
        }
        println(sb)
    }
}
