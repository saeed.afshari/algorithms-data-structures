package algorithmsdatastructures.div3n797

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val br = BufferedReader(InputStreamReader(System.`in`))
    br.use {
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            val (_, k) = br.readLine().split(" ").map { it.toInt() }
            val str = br.readLine()
            val splitted = str.split("W")
            val numbers = if (splitted.any { it.length >= k }) {
                0
            } else {
                val aList = mutableListOf<Int>()
                for (i in str.indices) {
                    var n = 0
                    if (str.length - i - k >= 0) {
                        for (j in i until i+k) {
                            if (str[j] == 'W') n++
                        }
                        aList.add(n)
                    }
                }
                aList.minOf { it }
            }
            sb.appendLine(numbers)
        }
        println(sb)
    }
}
