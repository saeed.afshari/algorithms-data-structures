package algorithmsdatastructures.div3n797

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val br = BufferedReader(InputStreamReader(System.`in`))
    br.use {
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            br.readLine()
            val s = br.readLine().split(" ").map { it.toInt() }
            val f = br.readLine().split(" ").map { it.toInt() }
            val result = mutableListOf(f[0] - s[0])
            for (i in 1 until s.size) {
                if (s[i] < f[i - 1]) {
                    result.add(f[i] - s[i] - (f[i - 1] - s[i]))
                } else {
                    result.add(f[i] - s[i])
                }
            }
            sb.appendLine(result.joinToString(" "))
        }
        println(sb)
    }
}
