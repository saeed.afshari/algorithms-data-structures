package algorithmsdatastructures.div3n797

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val br = BufferedReader(InputStreamReader(System.`in`))
    br.use {
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            val (_, k) = br.readLine().split(" ").map { it.toInt() }
            val weights = br.readLine().split(" ").map { it.toInt() }

            var generalSum = 0;
            val aSet = mutableSetOf<Int>()
            for (i in weights.indices) {
                var min = Int.MAX_VALUE
                var minI = -1
                var minJ = -1
                var sum = 0
                for (j in i + 1 until weights.size) {
                    if (((weights[i] + weights[j]) % k) < min && !aSet.contains(i) && !aSet.contains(j)) {
                        min = (weights[i] + weights[j]) % k
                        minI = i
                        minJ = j
                        sum = (weights[i] + weights[j]) / k
                    }
                }
                aSet.add(minI)
                aSet.add(minJ)
                generalSum += sum
            }

            sb.appendLine(generalSum)
        }
        println(sb)
    }
}
