package algorithmsdatastructures.codeforces790

import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.FileDescriptor
import java.io.FileOutputStream
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import kotlin.math.max
import kotlin.math.min


fun main() {
    val out = BufferedWriter(OutputStreamWriter(FileOutputStream(FileDescriptor.out), "ASCII"), 512)
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        repeat(numberOfTests) { test ->
            val k = br.readLine().split(" ")[1].toInt()
            val a = br.readLine().split(" ").map { item -> item.toInt() }
            val (l, r) = findLAndR(a, k)
            if (l == -1) out.write("-1\n") else out.write("$l $r\n")
        }
        out.flush()
    }
}

fun findLAndR(a: List<Int>, k: Int): Pair<Int, Int> {
    val dp = mutableMapOf<Int, Int>()
    for (i in a.indices) {
        dp[a[i]] = dp.getOrDefault(a[i], 0) + 1
    }
    var totalMax = Int.MIN_VALUE
    var lAndR = -1 to -1
    for (i in a.indices) {
        for (j in i until a.size) {
            val min = min(a[i], a[j])
            val max = max(a[i], a[j])
            if ((min..max).all { dp.getOrDefault(it, 0) >= k }) {
                if (totalMax < max - min) {
                    totalMax = max - min
                    lAndR = if (a[i] < a[j]) {
                        a[i] to a[j]
                    } else {
                        a[j] to a[i]
                    }
                }
            }
        }
    }
    return lAndR
}
