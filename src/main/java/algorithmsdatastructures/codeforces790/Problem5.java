package algorithmsdatastructures.codeforces790;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Problem5 {

    public static void main(String[] args) throws IOException {
        try(var br = new BufferedReader(new InputStreamReader(System.in))){
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while(numberOfTests>0){
                numberOfTests--;
                var splitted = br.readLine().split(" ");
                var numberOfQueries = Integer.parseInt(splitted[1]);
                var a = Arrays.stream(br.readLine().split(" ")).map(Integer::parseInt).sorted().collect(Collectors.toList());

                var suffixes = new int[a.size() + 1];
                suffixes [0] = 0;
                var j = 1;
                for(int i = a.size() - 1; i >= 0; i--){
                    suffixes[j] = suffixes[j - 1] + a.get(i);
                    j++;
                }

                while(numberOfQueries > 0){
                    numberOfQueries--;
                    var q = Integer.parseInt(br.readLine());
                    var index = lowerBound(suffixes, q);
                    if(index < suffixes.length && suffixes[index] >= q){
                        sb.append(index + "\n");
                    }else{
                        sb.append("-1\n");
                    }
                }
            }
            System.out.println(sb);
        }
    }

    private static int lowerBound(int[] a, int q){
        var l = 0;
        var r = a.length;
        while(l < r){
            var mid = (l + r) / 2;
            if(a[mid]>=q){
                r = mid;
            } else{
                l = mid + 1;
            }
        }
        return l;
    }

}
