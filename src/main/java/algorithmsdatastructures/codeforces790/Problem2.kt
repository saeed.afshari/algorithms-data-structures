package algorithmsdatastructures.codeforces790

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            br.readLine()
            val numberOfCandiesPerBox = br.readLine().split(" ").map { it.toInt() }.toMutableList()
            sb.appendLine(minimumTotalCandies2(numberOfCandiesPerBox))
        }
        print(sb)
    }
}

fun minimumTotalCandies(numberOfCandiesPerBox: List<Int>): Int {
    var min = Int.MAX_VALUE
    for (i in numberOfCandiesPerBox.indices) {
        if (numberOfCandiesPerBox[i] < min) {
            min = numberOfCandiesPerBox[i]
        }
    }
    var total = 0
    for (i in numberOfCandiesPerBox.indices) {
        total += (numberOfCandiesPerBox[i] - min)
    }
    return total
}

fun minimumTotalCandies2(numberOfCandiesPerBox: List<Int>): Int {
    val min = numberOfCandiesPerBox.minOf { it }
    return numberOfCandiesPerBox.sumOf { it - min }
}

