package algorithmsdatastructures.codeforces790

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            val numberOfRows = br.readLine().split(" ")[0].toInt()
            val adjacencyMap = mutableMapOf<Int, List<Int>>()
            repeat(numberOfRows) {
                if (!adjacencyMap.containsKey(it)) {
                    adjacencyMap[it] = mutableListOf()
                }
                adjacencyMap[it] = br.readLine().split(" ").map { item -> item.toInt() }
            }
            sb.appendLine(findMaximumAttacks(adjacencyMap))
        }
        print(sb)
    }
}

fun findMaximumAttacks(adjacencyMap: MutableMap<Int, List<Int>>): Any {
    var maxTotal = Int.MIN_VALUE
    for (row in 0 until adjacencyMap.keys.size) {
        for (cell in 0 until adjacencyMap.getValue(row).size) {
            val dfsTotal = dfsTotal(adjacencyMap, row, cell)
            if (dfsTotal > maxTotal) {
                maxTotal = dfsTotal
            }
        }
    }
    return maxTotal
}

fun dfsTotal(adjacencyMap: MutableMap<Int, List<Int>>, row: Int, cell: Int): Int {
    var total = adjacencyMap.getValue(row)[cell]
    var currentRow = row - 1
    var currentCell = cell - 1
    while (currentRow >= 0 && currentCell >= 0) {
        total += adjacencyMap.getValue(currentRow)[currentCell]
        currentRow--
        currentCell--
    }
    currentRow = row + 1
    currentCell = cell - 1
    while (currentRow < adjacencyMap.keys.size && currentCell >= 0) {
        total += adjacencyMap.getValue(currentRow)[currentCell]
        currentRow++
        currentCell--
    }
    currentRow = row + 1
    currentCell = cell + 1
    while (currentRow < adjacencyMap.keys.size && currentCell < adjacencyMap.getValue(currentRow).size) {
        total += adjacencyMap.getValue(currentRow)[currentCell]
        currentRow++
        currentCell++
    }
    currentRow = row - 1
    currentCell = cell + 1
    while (currentRow >= 0 && currentCell < adjacencyMap.getValue(currentRow).size) {
        total += adjacencyMap.getValue(currentRow)[currentCell]
        currentRow--
        currentCell++
    }
    return total
}
