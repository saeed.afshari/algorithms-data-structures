package algorithmsdatastructures.codeforces790

import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.max
import kotlin.math.min

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            val numberOfStrings = br.readLine().split(" ")[0].toInt()
            val strings = mutableListOf<String>()
            repeat(numberOfStrings) {
                strings.add(br.readLine())
            }
            sb.appendLine(findDifferencesTotal(strings))
        }
        print(sb)
    }
}

fun findDifferencesTotal(strings: MutableList<String>): Int {
    var minTotal = Int.MAX_VALUE
    for (i in strings.indices) {
        for (j in i + 1 until strings.size) {
            val difference = calculateDifference(strings[i], strings[j])
            if (difference < minTotal) {
                minTotal = difference
            }
        }
    }
    return minTotal
}

fun calculateDifference(s1: String, s2: String): Int {
    var totalDifferences = 0
    for (i in s1.indices) {
        val s1Number = s1[i] - 'a'
        val s2Number = s2[i] - 'a'
        val min = min(s1Number, s2Number)
        val max = max(s1Number, s2Number)
        totalDifferences += max - min
    }
    return totalDifferences
}
