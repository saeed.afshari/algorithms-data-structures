package algorithmsdatastructures.codeforces790

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            val numberOfQueries = br.readLine().split(" ")[1].toInt()
            val candiesSugarQuantityList = br.readLine().split(" ").map { item -> item.toInt() }
            val sortedDescendingList = candiesSugarQuantityList.sortedByDescending { it }
            val dp = mutableMapOf<Int, Int>()
            repeat(numberOfQueries) {
                val query = br.readLine().toInt()
                sb.appendLine(calculateMinimumQuantityOrNegativeNumber(sortedDescendingList, query, dp))
            }
        }
        print(sb)
    }
}

fun calculateMinimumQuantityOrNegativeNumber(
    candiesSugarQuantityList: List<Int>,
    query: Int,
    dp: MutableMap<Int, Int>
): Int {
    if (dp.containsKey(query)) return dp.getValue(query)
    if (candiesSugarQuantityList.sum() < query) {
        dp[query] = -1
        return -1
    }
    var total = 0
    var numberOfSugars = 0
    for (i in candiesSugarQuantityList.indices) {
        total += candiesSugarQuantityList[i]
        numberOfSugars++
        if (total >= query) {
            dp[query] = numberOfSugars
            return numberOfSugars
        }
    }
    dp[query] = -1
    return -1
}
