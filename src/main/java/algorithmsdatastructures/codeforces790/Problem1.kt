package algorithmsdatastructures.codeforces790

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            val ticket = br.readLine()
            sb.appendLine(isLuckyTicket2(ticket))
        }
        print(sb)
    }
}

fun isLuckyTicket(ticket: String): String {
    var sum1 = 0
    var sum2 = 0
    for (i in 0..5) {
        if (i <= 2) {
            sum1 += ticket[i] - 'a'
        } else {
            sum2 += ticket[i] - 'a'
        }
    }
    return if (sum1 == sum2) "YES" else "NO"
}

fun isLuckyTicket2(ticket: String): String {
    val sumOf = ticket.substring(0..2).sumOf { it - '0' }
    val sumOf2 = ticket.substring(3..5).sumOf { it - '0' }
    return if(sumOf == sumOf2) "YES" else "NO"
}
