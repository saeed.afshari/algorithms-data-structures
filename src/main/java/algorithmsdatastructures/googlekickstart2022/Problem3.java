package algorithmsdatastructures.googlekickstart2022;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// https://codingcompetitions.withgoogle.com/kickstart/round/00000000008f4a94/0000000000b54d3b
public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            var caseNumber = 0;
            while (numberOfTests > 0) {
                numberOfTests--;
                caseNumber++;
                var splitted = br.readLine().split(" ");
                var n = Integer.parseInt(splitted[0]);
                var q = Integer.parseInt(splitted[1]);
                var str = br.readLine();
                for (int i = 0; i < q; i++) {
                    var query = br.readLine();
                }
//                if (found) sb.append(String.format("Case #%d: Spell!\n", caseNumber));
//                else sb.append(String.format("Case #%d: Nothing.\n", caseNumber));
            }
            System.out.println(sb);
        }
    }
}
