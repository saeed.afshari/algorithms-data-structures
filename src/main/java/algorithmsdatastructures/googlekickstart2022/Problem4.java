package algorithmsdatastructures.googlekickstart2022;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;

// https://codingcompetitions.withgoogle.com/kickstart/round/00000000008f4a94/0000000000b55464
public class Problem4 {

    private static Set<Character> vowels = Set.of('a', 'e', 'i', 'o', 'u');

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            var caseNumber = 0;
            while (numberOfTests > 0) {
                numberOfTests--;
                caseNumber++;
                var str = br.readLine();
//                var found = false;
//                for (int i = 0; i < str.length(); i++) {
//                    for (int j = i + 1; j <= str.length(); j++) {
//                        if (isSpell(str.substring(i, j))) {
//                            found = true;
//                            break;
//                        }
//                    }
//                }
                var found = isSpellRecursive(str, 0, str.length());
                if (found) sb.append(String.format("Case #%d: Spell!\n", caseNumber));
                else sb.append(String.format("Case #%d: Nothing.\n", caseNumber));
            }
            System.out.println(sb);
        }
    }

    private static boolean isSpellRecursive(String str, int start, int end) {
        if (start >= end) return false;
        System.out.println(str.substring(start, end));
        if (isSpell(str.substring(start, end))) return true;
        if (isSpellRecursive(str, start + 1, end)) return true;
        if (isSpellRecursive(str, start, end - 1)) return true;
        return false;
    }

    private static boolean isSpell(String str) {
        if (numberOfVowels(str) < 5) return false;
        var size = str.length();
        var mid = size / 2;
        for (int j = 0; j <= mid; j++) {
            var left = str.substring(0, j);
            var right = str.substring(size - j, size);
            var center = str.substring(j, size - j);
            if (left.equals(right) && numberOfVowels(left) >= 2 && numberOfVowels(center) >= 1) {
                return true;
            }
        }
        return false;
    }

    // Selection r from n -> r loop
    // Number of states
    //
    //
    //
    //
    // abcde

    // r = 0 -> 1
    // r = 1 -> 5
    // r = 2 -> 10
    // r = 3 -> n = 5 -> 10
    // r = 4 -> 5
    // r = 5 -> 1
    // 2 ** n -> sub sets (knapsack is all subsets)
    // Permutation
    // coin change -> n*k


    private static int numberOfVowels(String str) {
        int sum = 0;
        for (int i = 0; i < str.length(); i++) {
            if (vowels.contains(str.charAt(i))) sum++;
        }
        return sum;
    }
}
