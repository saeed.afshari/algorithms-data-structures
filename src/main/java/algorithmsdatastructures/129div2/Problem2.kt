package algorithmsdatastructures.`129div2`

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            bufferedReader.readLine()
            var cards = bufferedReader.readLine().split(" ").map { it.toInt() }
            bufferedReader.readLine()
            val shuffles = bufferedReader.readLine().split(" ").map { it.toInt() }
            var sum = 0L
            for (i in shuffles.indices) {
                sum += shuffles[i]
            }
            cards = cards.subList((sum % cards.size).toInt(), cards.size) + cards.subList(0, (sum % cards.size).toInt())
            sb.appendLine(cards[0])
        }
        println(sb)
    }
}
