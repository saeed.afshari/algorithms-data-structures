package algorithmsdatastructures.`129div2`

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        repeat(numberOfTests) {
            bufferedReader.readLine()
            val alices = bufferedReader.readLine().split(" ").map { it.toInt() }
            bufferedReader.readLine()
            val bobs = bufferedReader.readLine().split(" ").map { it.toInt() }
            solve(alices, bobs)
        }
    }
}

fun solve(alices: List<Int>, bobs: List<Int>) {
    val alicSorted = alices.sorted()
    val bobSorted = bobs.sorted()

    var winner1 = ""
    var res1 = binarySearch(bobSorted, alicSorted[alices.size - 1])
    if (res1 == -1 || res1 >= bobSorted.size) {
        winner1 = "Alice"
    } else {
        val res2 = binarySearch(alicSorted, bobSorted[res1])
        if (res2 == -1) {
            winner1 = "Bob"
        }
    }
    println(winner1)

    winner1 = ""
    res1 = binarySearch(alicSorted, bobSorted[bobSorted.size - 1])
    if (res1 == -1 || res1 >= alicSorted.size) {
        winner1 = "Bob"
    } else{
        val res2 = binarySearch(bobSorted, alicSorted[res1])
        if (res2 == -1) {
            winner1 = "Alice"
        }
    }

    println(winner1)

}

fun binarySearch(aList: List<Int>, x: Int): Int {
    var l = 0
    var r = aList.size

    while (l < r) {
        val m = l + ((r - l) / 2)
        if (aList[m] <= x) {
            l = m + 1
        } else if (aList[m] > x) {
            r = m
        }
    }
    if (l >= aList.size) return -1
    return l
}
