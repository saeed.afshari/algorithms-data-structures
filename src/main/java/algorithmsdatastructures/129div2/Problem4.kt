package algorithmsdatastructures.`129div2`

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val (n, x) = bufferedReader.readLine().split(" ").map { it.toInt() }
        println(solve(n, x))
    }
}

fun solve(n: Int, x: Int): Int {
    var l = 1
    var r = 19
    while (l < r) {
        var mid = l + ((r - l) / 2)
        var tmp = x
        var y = 0
        for (i in 1..mid) {
            y++
            tmp *= x
            if (tmp.toString().length == n) {
                return y
            }
            if (tmp.toString().length > n) {
                break
            }
        }
        if (tmp.toString().length < n) {
            l = mid + 1
        } else {
            r = mid - 1
        }
    }
    return -1
}
