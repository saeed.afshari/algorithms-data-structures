package algorithmsdatastructures.`129div2`

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.Collections.swap

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            bufferedReader.readLine()
            var a = bufferedReader.readLine().split(" ").map { it.toInt() }
            val b = bufferedReader.readLine().split(" ").map { it.toInt() }
            val result = insertionSort(a, b)
            if (result != null) {
                if (result.size > 10 * 10 * 10 * 10) sb.append("-1")
                else {
                    sb.appendLine(result.size)
                    result.forEach { sb.appendLine("${it.first + 1} ${it.second + 1}") }
                }
            } else {
                sb.appendLine("-1")
            }
        }
        println(sb.trimEnd())
    }
}

fun insertionSort(a: List<Int>, b: List<Int>): List<Pair<Int, Int>>? {
    val result = mutableListOf<Pair<Int, Int>>()
    for (i in a.indices) {
        var j = 0
        while (j < i) {
            val swapItems = swapItems(a, b, i, j)
            if (swapItems.first == -1) return null
            if (swapItems.first != -2) {
                result.add(swapItems)
            }
            j++
        }
    }
    return result
}

fun swapItems(a: List<Int>, b: List<Int>, i: Int, j: Int): Pair<Int, Int> {
    val aI = a[i]
    val aJ = a[j]
    val bI = b[i]
    val bJ = b[j]
    if (aJ > aI || bJ > bI) {
        if (aJ < aI || bJ < bI) {
            return -1 to -1
        }
        swap(a, i, j)
        swap(b, i, j)
        return i to j
    }
    return -2 to 0
}
