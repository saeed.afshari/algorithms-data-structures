package algorithmsdatastructures.codeforce784div4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int numberOfTests = Integer.parseInt(reader.readLine());
        for (int i = 0; i < numberOfTests; i++) {
            reader.readLine();
            var array = reader.readLine().split(" ");
            System.out.println(findTripleOrNegativeOne(array));
        }
    }

    private static String findTripleOrNegativeOne(String[] array) {
        var itemsMap = new HashMap<String, Integer>();
        for (var item : array) {
            Integer numberOfItem = itemsMap.getOrDefault(item, 0);
            itemsMap.put(item, numberOfItem + 1);
            if (numberOfItem == 2) return item;
        }
        return "-1";
    }
}
