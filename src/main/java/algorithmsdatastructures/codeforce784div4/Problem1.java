package algorithmsdatastructures.codeforce784div4;

import java.util.Scanner;

public class Problem1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfTests = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < numberOfTests; i++) {
            var rating = Integer.parseInt(scanner.nextLine());
            if (rating >= 1900) {
                System.out.println("Division 1");
            } else if (rating <= 1899 && rating >= 1600) {
                System.out.println("Division 2");
            } else if (rating <= 1599 && rating >= 1400) {
                System.out.println("Division 3");
            } else if (rating <= 1399) {
                System.out.println("Division 4");
            }
        }
    }
}
