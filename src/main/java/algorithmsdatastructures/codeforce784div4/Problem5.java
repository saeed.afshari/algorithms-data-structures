package algorithmsdatastructures.codeforce784div4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Problem5 {

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(reader.readLine());
            for (int numberOfTest = 0; numberOfTest < numberOfTests; numberOfTest++) {
                int numberOfStrings = Integer.parseInt(reader.readLine());
                var hashSetOfStrings = new HashMap<String, Long>(numberOfStrings);
                for (int j = 0; j < numberOfStrings; j++) {
                    var str = reader.readLine();
                    var value = hashSetOfStrings.getOrDefault(str, 0L);
                    hashSetOfStrings.put(str, value + 1);
                }
                var listOfStrings = hashSetOfStrings.keySet().stream().collect(Collectors.toList());
                System.out.println(getNumberOfSimilarStringsInOnePosition(listOfStrings, hashSetOfStrings));
            }
        }
    }

    private static long getNumberOfSimilarStringsInOnePosition(List<String> listOfStrings, Map<String, Long> hashSetOfStrings) {
        Long numberOfFindings = 0L;
        for (int i = 0; i < listOfStrings.size(); i++) {
            for (int j = i + 1; j < listOfStrings.size(); j++) {
                Long count = 0L;
                if (listOfStrings.get(i).charAt(0) == listOfStrings.get(j).charAt(0)) count++;
                if (listOfStrings.get(i).charAt(1) == listOfStrings.get(j).charAt(1)) count++;

                if (count == 1)
                    numberOfFindings += hashSetOfStrings.get(listOfStrings.get(i)) * hashSetOfStrings.get(listOfStrings.get(j));
            }
        }
        return numberOfFindings;
    }
}
