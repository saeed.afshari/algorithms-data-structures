package algorithmsdatastructures.codeforce784div4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem4 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                int strLength = Integer.parseInt(br.readLine());
                String match = br.readLine();

                if (strLength != match.length()) {
                    sb.append("NO\n");
                } else {
                    sb.append(findsMatch(match));
                }
            }
            System.out.println(sb);
        }
    }

    private static String findsMatch(String match) {
        var splitted = match.split("W");
        if (splitted.length == 0) return "YES\n";
        for (var item : splitted) {
            if (item.length() == 0) continue;
            var indexOfB = item.indexOf("B");
            var indexOfR = item.indexOf("R");
            if (indexOfB == -1 || indexOfR == -1) return "NO\n";
        }
        return "YES\n";
    }
}
