package algorithmsdatastructures.codeforce784div4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Problem6 {

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(reader.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                reader.readLine();
                var a = Arrays.stream(reader.readLine().split(" ")).map(it -> Integer.parseInt(it)).collect(Collectors.toList());

                var prefixes = new int[a.size() + 1];
                prefixes[0] = 0;
                for (int i = 1; i <= a.size(); i++) {
                    prefixes[i] = prefixes[i - 1] + a.get(i - 1);
                }
                var suffixes = new int[a.size() + 1];
                suffixes[0] = 0;
                int j = 1;
                for (int i = a.size() - 1; i >= 0; i--) {
                    suffixes[j] = suffixes[j - 1] + a.get(i);
                    j++;
                }

                //Stop conditions:
                if (a.size() == 1) {
                    sb.append("0\n");
                } else {
                    sb.append(getNumberOfCandiesONLogN(prefixes, suffixes) + "\n");
                }
            }
            System.out.println(sb);
        }
    }

    private static int getNumberOfEatenCandiesON(List<Integer> a, int[] prefixes, int[] suffixes) {
        var l = 1;
        var r = 1;
        var indexL = -1;
        var indexR = -1;
        while (l + r <= a.size()) {
            if (prefixes[l - 1] == suffixes[r - 1]) {
                indexL = l;
                indexR = r;
                l++;
            } else if (prefixes[l] < suffixes[r]) {
                l++;
            } else {
                r++;
            }
        }
        if (indexL == -1) {
            return 0;
        } else {
            return indexL + indexR;
        }
    }

    private static int getNumberOfCandiesON2(int[] prefixes, int[] suffixes) {
        //O(n**2):
        // 2 1 4 2 4 1
        // [0,2,3,7,9,13,14]
        // [0,1,5,7,11,12,14]
        var eaten = 0;
        for (int i = 1; i < prefixes.length; i++) {
            for (int j = 1; j < prefixes.length; j++) {
                //2 -> 7
                if (i + j >= prefixes.length) {
                    return eaten;
                }
                if (prefixes[i] == suffixes[j]) {
                    var lastNumberOfCandies = i + j;
                    eaten = Math.max(eaten, lastNumberOfCandies);
                    break;
                } else if (suffixes[j] > prefixes[i]) {
                    break;
                }
            }
        }
        return eaten;
    }

    private static int getNumberOfCandiesONLogN(int[] prefixes, int[] suffixes) {
        //O(n*logn):
        // 2 1 4 2 4 1
        // [0,2,3,7,9,13,14]
        // [0,1,5,7,11,12,14]
        var eaten = 0;
        for (int i = 1; i < prefixes.length; i++) {
            var l = 0;
            var r = prefixes.length;
            int x = prefixes[i];
            while (l < r) {
                var mid = (l + r) / 2;
                if (x <= suffixes[mid]) {
                    r = mid;
                } else {
                    l = mid + 1;
                }
            }
            if (l < prefixes.length && suffixes[l] == x && (l + i) < prefixes.length) {
                var lastNumberOfCandies = i + l;
                eaten = Math.max(eaten, lastNumberOfCandies);
            }
        }
        return eaten;
    }
}
