package algorithmsdatastructures.codeforce784div4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Problem3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfTests = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < numberOfTests; i++) {
            Integer lengthOfTheArray = scanner.nextInt();
            scanner.nextLine();
            var array = new ArrayList<Integer>(lengthOfTheArray);
            for (int j = 0; j < lengthOfTheArray; j++) {
                array.add(scanner.nextInt());
            }
            System.out.println(isItPossibleToMakeItOddOrEven(array));
            scanner.nextLine();
        }
    }

    private static String isItPossibleToMakeItOddOrEven(List<Integer> array) {
        if (array.size() <= 1) return "YES";
        String oddIndexType = array.get(0) % 2 == 0 ? "EVEN" : "ODD";
        String evenIndexType = array.get(1) % 2 == 0 ? "EVEN" : "ODD";
        for (int i = 2; i < array.size(); i++) {
            var type = array.get(i) % 2 == 0 ? "EVEN" : "ODD";
            if (i % 2 == 0 && !type.equals(oddIndexType)) {
                return "NO";
            }
            if (i % 2 != 0 && !type.equals(evenIndexType)) {
                return "NO";
            }
        }
        return "YES";
    }
}
