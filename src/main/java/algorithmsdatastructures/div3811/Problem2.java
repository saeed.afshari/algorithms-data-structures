package algorithmsdatastructures.div3811;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var n = Integer.parseInt(br.readLine());
                var splitted = br.readLine().split(" ");
                var a = new int[n];
                var aHashMap = new HashMap<Integer, Integer>();
                for (int i = 0; i < n; i++) {
                    a[i] = Integer.parseInt(splitted[i]);
                    var value = aHashMap.getOrDefault(a[i], 0);
                    value++;
                    aHashMap.put(a[i], value);
                }
                int index = 0;
                for (int i = 0; i < n; i++) {
                    if (aHashMap.get(a[i]) > 1) {
                        index = i + 1;
                        var v = aHashMap.get(a[i]);
                        v--;
                        aHashMap.put(a[i], v);
                    }
                }
                sb.append(index + "\n");
            }

            System.out.println(sb);
        }
    }
}
