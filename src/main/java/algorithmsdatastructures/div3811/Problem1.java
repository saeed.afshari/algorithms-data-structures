package algorithmsdatastructures.div3811;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem1 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var splitted = br.readLine().split(" ");
                var alarms = Integer.parseInt(splitted[0]);
                var sleepHour = Integer.parseInt(splitted[1]);
                var sleepMin = Integer.parseInt(splitted[2]);
                int min = Integer.MAX_VALUE;
                for (int i = 0; i < alarms; i++) {
                    splitted = br.readLine().split(" ");
                    var nh = Integer.parseInt(splitted[0]);
                    var nm = Integer.parseInt(splitted[1]);

                    var nv = getDiff(sleepHour, sleepMin, nh, nm);
                    min = Integer.min(nv, min);
                }
                sb.append(toHour(min));
            }

            System.out.println(sb);
        }
    }

    private static int getDiff(int sleepHour, int sleepMin, int hh, int mm) {
        if (hh > sleepHour) {
            var diff = (hh - sleepHour) * 60;
            return mm - sleepMin + diff;
        } else if (hh < sleepHour) {
            var diff = (24 - (sleepHour - hh)) * 60;
            return mm - sleepMin + diff;
        } else {
            if (mm < sleepMin) {
                return (24 * 60) - (sleepMin - mm);
            } else if (mm > sleepMin) {
                return mm - sleepMin;
            } else {
                return 0;
            }
        }
    }

    private static String toHour(int diff) {
        var hours = diff / 60;
        var mins = diff % 60;
        return hours + " " + mins+ "\n";
    }
}
