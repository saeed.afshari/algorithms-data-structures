package algorithmsdatastructures.div3811;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var n = Integer.parseInt(br.readLine());
                answer = new ArrayList<>();
                if (n <= 9) {
                    sb.append(n + "\n");
                } else {
                    calculatePermutations(new ArrayList<>(), List.of(1, 2, 3, 4, 5, 6, 7, 8, 9), n);
                    sb.append(answer.stream().map(String::valueOf).collect(Collectors.joining("")) + "\n");
                }
            }

            System.out.println(sb);
        }
    }

    private static List<Integer> answer;

    private static void calculatePermutations(List<Integer> prefix, List<Integer> remaining, int target) {
        int sum = 0;
        for (int i = 0; i < prefix.size(); i++) {
            sum += prefix.get(i);
        }
        if (sum == target) {
            if(answer.isEmpty() || answer.size() > prefix.size())
                answer = prefix;
            return;
        }
        if(sum>target) return;
        if (remaining.isEmpty()) {
            return;
        }
        int remainingSize = remaining.size();
        for (int i = 0; i < remainingSize; i++) {
            var newPrefix = new ArrayList<>(prefix);
            newPrefix.add(remaining.get(i));
            var newRemaining = new ArrayList<>(remaining.subList(0, i));
            if (i + 1 < remainingSize)
                newRemaining.addAll(remaining.subList(i + 1, remainingSize));
            calculatePermutations(newPrefix, newRemaining, target);
        }
    }
}
