package algorithmsdatastructures.codeforces126div2

import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.min

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            bufferedReader.readLine()
            val initialTreeHeights = bufferedReader.readLine().split(" ").map { it.toInt() }
            sb.appendLine(solve(initialTreeHeights))
        }
        println(sb)
    }
}

fun solve(initialTreeHeights: List<Int>): Long {
    if (initialTreeHeights.size == 1) return 0
    val max = initialTreeHeights.maxOf { it }
    //[1,1,2,2,4]
//    val days = calculateDays(initialTreeHeights, max)
    val days2 = calculateDays(initialTreeHeights, max+1)
//    return min(days, days2)
    return days2
}

private fun calculateDays(
    initialTreeHeights: List<Int>,
    max: Int
): Long {
    var startDayValue = 1
    var days = 0L
    var evens = 0
    var odds = 0
    for (i in initialTreeHeights.indices) {
        val diff = max - initialTreeHeights[i]
        if (diff == 0) continue
        if (diff <= 2) {
            if (diff == 2) {
                if (startDayValue % 2 == 0) {
                    days++
                    startDayValue = 1
                } else {
                    evens++
                }
                continue
            } else {
                if (startDayValue % 2 != 0) {
                    days++
                    startDayValue = 2
                } else {
                    odds++
                }
                continue
            }
        }
        var requiredDays = diff * 2 / 3
        if (startDayValue % 2 != 0) {
            if (requiredDays % 2 == 0) {
                startDayValue = 1
            } else {
                startDayValue = 2
            }
        } else {
            if (requiredDays % 2 == 0) {
                startDayValue = 2
            } else {
                startDayValue = 1
            }
        }
        days += requiredDays

        val reminder = diff * 2 % 3
        if (reminder > 0) {
            if (reminder == 2) {
                if (startDayValue % 2 == 0) {
                    days++
                    startDayValue = 1
                } else {
                    evens++
                }
            } else {
                if (startDayValue % 2 != 0) {
                    days++
                    startDayValue = 2
                } else {
                    odds++
                }
            }
        }
    }
    days += min(odds, evens) * 2

    if (odds > evens) {
        val diff = odds - evens
        days += diff * 2
        if (startDayValue % 2 != 0) {
            days--
        }
    } else if (evens > odds) {
        val diff = evens - odds
        days += diff * 2
        if (startDayValue % 2 == 0) {
            days--
        }
    }
    return days
}
