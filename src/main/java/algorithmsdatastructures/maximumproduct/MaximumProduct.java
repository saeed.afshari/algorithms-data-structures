package algorithmsdatastructures.maximumproduct;

import java.util.*;
import java.util.stream.Collectors;

public class MaximumProduct {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfTests = Integer.parseInt(scanner.nextLine());
        var tests = new ArrayList<List<Integer>>();
        for (int i = 0; i < numberOfTests; i++) {
            var numberOfItems = Integer.parseInt(scanner.nextLine());
            if (numberOfItems > 0) {
                tests.add(Arrays.stream(scanner.nextLine().split(" ")).map(it -> Integer.parseInt(it)).collect(Collectors.toList()));
            } else {
                tests.add(Collections.emptyList());
            }
        }
        List<Product> maximumProducts = calculateMaximumProduct(tests);

        maximumProducts.forEach(item -> System.out.println(item.x + " " + item.y));
    }

    private static class Product {
        private final Integer x;
        private final Integer y;

        Product(Integer x, Integer y) {
            this.x = x;
            this.y = y;
        }
    }

    static List<Product> calculateMaximumProduct(List<List<Integer>> tests) {
        if (tests.size() == 0) {
            return new ArrayList<>(0);
        }
        var result = new ArrayList<Product>(tests.size());
        for (var test : tests) {
            if (test.size() == 0) {
                result.add(new Product(0, 0));
                continue;
            }
            int firstZeroIndex = -1;
            int lastZeroIndex = -1;
            for (int i = 0; i < test.size(); i++) {
                if (test.get(i) == 0 && firstZeroIndex == -1) {
                    firstZeroIndex = i;
                } else if (test.get(i) == 0) {
                    lastZeroIndex = i;
                }
            }

            // it can be -1
            int startIndexFromRight = firstZeroIndex >= 0 ? firstZeroIndex - 1 : test.size() - 1;
            // it can be test.size()
            int startIndexFromLeft = lastZeroIndex >= 0 ? lastZeroIndex + 1 : (firstZeroIndex >= 0 ? firstZeroIndex + 1 : 0);

            int negativeNumbersCountFromRight = 0;
            for (int i = startIndexFromRight; i >= 0; i--) {
                if (test.get(i) < 0) {
                    negativeNumbersCountFromRight++;
                }
            }

            int negativeNumbersCountFromLeft = 0;
            for (int i = startIndexFromLeft; i < test.size(); i++) {
                if (test.get(i) < 0) {
                    negativeNumbersCountFromLeft++;
                }
            }

            if (negativeNumbersCountFromRight % 2 != 0) {
                int numberOfExtraNegatives = 0;
                for (int i = startIndexFromRight; i >= 0; i--) {
                    numberOfExtraNegatives++;
                    if (test.get(i) < 0) {
                        break;
                    }
                }
                startIndexFromRight -= numberOfExtraNegatives;
            }

            if (negativeNumbersCountFromLeft % 2 != 0) {
                int numberOfExtraNegatives = 0;
                for (int i = startIndexFromLeft; i < test.size(); i++) {
                    numberOfExtraNegatives++;
                    if (test.get(i) < 0) {
                        break;
                    }
                }
                startIndexFromLeft += numberOfExtraNegatives;
            }

            int leftMul = multiplication(test, startIndexFromLeft, test.size() - 1);
            int rightMul = multiplication(test, 0, startIndexFromRight);

            if (leftMul >= rightMul) {
                result.add(new Product(startIndexFromLeft, 0));
            } else {
                result.add(new Product(0, test.size() - startIndexFromRight - 1));
            }
        }
        return result;
    }

    static int multiplication(List<Integer> a, int start, int end) {
        if (start < 0 || start >= a.size() || end < 0 || start > end) return 1;
        var result = 1;
        for (int i = start; i <= end; i++) {
            result *= a.get(i);
        }
        return result;
    }
}
