package algorithmsdatastructures.practice.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//https://codingcompetitions.withgoogle.com/kickstart/round/00000000008caa74/0000000000acef55#problem
public class UnlockThePadLock {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            int caseNumber = 0;
            while (numberOfTests > 0) {
                caseNumber++;
                numberOfTests--;
                var splitted = br.readLine().split(" ");
                var n = Integer.parseInt(splitted[0]);
                var d = Integer.parseInt(splitted[1]);
                var numbers = new int[n];
                splitted = br.readLine().split(" ");
                for (int i = 0; i < splitted.length; i++) {
                    numbers[i] = Integer.parseInt(splitted[i]);
                }
                var min = Long.MAX_VALUE;
                var dp = new long[n][n][n];
                for (int k = 0; k < n; k++) {
                    for (int l = 0; l < n; l++) {
                        for (int m = 0; m < n; m++) {
                            dp[k][l][m] = -1;
                        }
                    }
                }
                for (int i = 0; i < n; i++) {
                    min = Long.min(min, calc(i, i, i, n, d, numbers, dp));
                }
                sb.append(String.format("Case #%d: %d\n", caseNumber, min));
            }
            System.out.println(sb);
        }
    }

    private static long calc(int left, int right, int currentIndex, int n, long d, int[] values, long[][][] dp) {
        if (right - left == n - 1) {
            return Long.min(values[currentIndex], d - values[currentIndex]);
        }
        if (dp[left][right][currentIndex] != -1) return dp[left][right][currentIndex];

        var leftVal = Long.MAX_VALUE;
        var rightVal = Long.MAX_VALUE;
        if (left > 0) {
            var nextValue = values[left - 1];
            leftVal = transitionCost(values[currentIndex], d, nextValue) + calc(left - 1, right, left - 1, n, d, values, dp);
        }
        if (right < n - 1) {
            var nextValue = values[right + 1];
            rightVal = transitionCost(values[currentIndex], d, nextValue) + calc(left, right + 1, right + 1, n, d, values, dp);
        }

        dp[left][right][currentIndex] = Long.min(leftVal, rightVal);
        return dp[left][right][currentIndex];
    }

    private static long transitionCost(long currentValue, long d, long nextValue) {
        return Long.min(Math.abs(currentValue - nextValue), Long.min(Math.abs(d - currentValue + nextValue), Math.abs(currentValue + d - nextValue)));
    }
}
