package algorithmsdatastructures.practice.dp;

import java.io.IOException;

//https://codingcompetitions.withgoogle.com/kickstart/round/00000000008caa74/0000000000acef55
public class BinaryTreePractice {

    private static class Node {
        private final Node left;
        private final Node right;

        Node(Node left, Node right) {
            this.left = left;
            this.right = right;
        }
    }

    public static void main(String[] args) throws IOException {
        Node root = new Node(new Node(new Node(null, null), null), new Node(null, null));
        System.out.println(getMaxTreeDepth(root));
    }

    private static int getMaxTreeDepth(Node node) {
        if (node.left == null && node.right == null) {
            return 1;
        }
        var left = 0;
        var right = 0;
        if (node.left != null)
            left = getMaxTreeDepth(node.left);
        if (node.right != null)
            right = getMaxTreeDepth(node.right);
        return Integer.max(left, right) + 1;
    }
}
