package algorithmsdatastructures.practice.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LongestIncreasingPath {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            int caseNumber = 0;
            while (numberOfTests > 0) {
                caseNumber++;
                numberOfTests--;
                var n = Integer.parseInt(br.readLine());
                var splitted = br.readLine().split(" ");
                var numbers = new int[n];
                for (int i = 0; i < n; i++) {
                    numbers[i] = Integer.parseInt(splitted[i]);
                }
                int max = Integer.MIN_VALUE;
                var dp = new int[n + 1];
                for (int i = 0; i < n; i++) {
                    var maxLength = calculateMaxLength(i, n, numbers, dp);
                    max = Integer.max(max, maxLength);
                }
                sb.append(String.format("Case #%d: %d\n", caseNumber, max));
            }
            System.out.println(sb);
        }
    }

    private static int calculateMaxLength(int index, int n, int[] numbers, int[] dp) {
        if (index == n) {
            return 0;
        }
//        if (dp[index] != 0) {
//            return dp[index];
//        }
        if(index > 0 && numbers[index - 1] > numbers[index]) return 0;
        dp[index] = 1 + calculateMaxLength(index + 1, n, numbers, dp);
        return dp[index];
    }
}
