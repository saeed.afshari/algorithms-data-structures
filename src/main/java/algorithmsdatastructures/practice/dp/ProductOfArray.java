package algorithmsdatastructures.practice.dp;//package algorithmsdatastructures.practice.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

//https://leetcode.com/problems/product-of-array-except-self/
public class ProductOfArray {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            int caseNumber = 0;
            while (numberOfTests > 0) {
                caseNumber++;
                numberOfTests--;
                var splitted = br.readLine().split(" ");
                splitted = br.readLine().split(" ");
                var numbers = new int[splitted.length];
                var result = new int[splitted.length];
                for (int i = 0; i < splitted.length; i++) {
                    numbers[i] = Integer.parseInt(splitted[i]);
                }
                var suffix = new int[numbers.length + 1];
                suffix[0] = 1;
                int j = 1;
                for (int i = numbers.length - 1; i >= 0; i--) {
                    suffix[j] = numbers[i] * suffix[j - 1];
                    j++;
                }
                for (int i = 0; i < numbers.length; i++) {
                    result[i] = suffix[suffix.length - i - 1];
                }

                sb.append(String.format("Case #%d: %d\n", caseNumber, Arrays.stream(result).mapToObj(String::valueOf).collect(Collectors.joining(","))));
            }
            System.out.println(sb);
        }
    }
}
