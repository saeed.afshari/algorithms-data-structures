package algorithmsdatastructures.practice.dp;//package algorithmsdatastructures.practice.dp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

//https://leetcode.com/problems/contiguous-array/
public class ContinuesArray {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            int caseNumber = 0;
            while (numberOfTests > 0) {
                caseNumber++;
                numberOfTests--;
                var splitted = br.readLine().split(" ");
                var numbers = new int[splitted.length];
                for (int i = 0; i < splitted.length; i++) {
                    numbers[i] = Integer.parseInt(splitted[i]);
                }

                sb.append(String.format("Case #%d: %d\n", caseNumber, findMaxLength(numbers)));
            }
            System.out.println(sb);
        }
    }

    private static int findMaxLength(int[] nums) {
        int max = 0;
        var numberOfZeros = nums[0] == 0 ? 1 : 0;
        var numberOfOnes = nums[0] == 1 ? 1 : 0;
        for(int i = 1; i < nums.length; i++){
            if(nums[i] == nums[i-1]){
                if(nums[i] == 0) numberOfZeros++;
                else numberOfOnes++;
            } else{
                if(numberOfZeros == 0 && nums[i] == 0){
                    numberOfZeros++;
                    continue;
                }
                if(numberOfOnes == 0 && nums[i] == 1){
                    numberOfOnes++;
                    continue;
                }
                max = Integer.max(Integer.min(numberOfZeros, numberOfOnes) * 2, max);
                if(nums[i] == 0) numberOfZeros = 1;
                else numberOfOnes = 1;
            }
        }
        max = Integer.max(Integer.min(numberOfZeros, numberOfOnes) * 2, max);
        return max;
    }
}
