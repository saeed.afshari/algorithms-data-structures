package algorithmsdatastructures.practice.string;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class WildcardMatching {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
//                sb.append(isMatch(br.readLine(), br.readLine()) + "\n");
                var s = br.readLine();
                var pattern = br.readLine();
                var splitted = pattern.split("[*]");
                if(splitted.length == 1 && splitted[0].length() != s.length()&& !pattern.endsWith("*")) {
                    sb.append("NO\n");
                } else{
                    sb.append(isMatch(0, 0, splitted, s) ? "YES\n" : "NO\n");
                }
            }
            System.out.println(sb);
        }
    }

    private static boolean isMatch(int index, int charIndex, String[] parts, String originalString){
        if(index >= parts.length) return true;
        var currentPart = parts[index];
        var currentPartSize = currentPart.length();
        var sum = 0;
        int j = 0;
        int i = charIndex;
        if(currentPartSize > 0){
            for(; i < originalString.length(); i++){
                if(j == currentPartSize) break;
                if(currentPart.charAt(j) == '?' || originalString.charAt(i) == currentPart.charAt(j)){
                    j++;
                    continue;
                }
                j = 0;
            }
        }
        if(j != currentPartSize) return false;
        return isMatch(index + 1, i, parts, originalString);
    }

    private static boolean isMatch(String input, String pattern) {
        if (pattern.equals(input)) return true;
        var firstWildcardIndex = pattern.indexOf("*");
        if (firstWildcardIndex < 0) return pattern.equals(input);
        var lastWildcardIndex = 0;
        for (int i = pattern.length() - 1; i >= 0; i--) {
            if (pattern.charAt(i) == '*') {
                lastWildcardIndex = i;
                break;
            }
        }
        var patterns = Arrays.stream(pattern.split("[*]")).filter(it -> it != "").toList();
        if (firstWildcardIndex > 0) {
            if (!startsMatch(input, patterns.get(0))) return false;
        }
        if (pattern.charAt(pattern.length() - 1) != '*') {
            if (!endsMatch(input, patterns.get(patterns.size() - 1))) return false;
        }

        if (firstWildcardIndex == lastWildcardIndex) return true;

        int jStart = 0;
        int jEnd = patterns.size();
        if (firstWildcardIndex != 0) {
            jStart = 1;
        }
        if (lastWildcardIndex != pattern.length() - 1) {
            jEnd = patterns.size() - 1;
        }
        int i = 0;
        for (int j = jStart; j < jEnd; j++) {
            var currentPattern = patterns.get(j);
            var index = checkMatch(input, currentPattern, i);
            if(index == -1) return false;
            i = index;
        }
        return true;
    }

    private static boolean startsMatch(String input, String pattern) {
        return input.startsWith(pattern);
    }

    private static boolean endsMatch(String input, String pattern) {
        return input.endsWith(pattern);
    }

    private static int checkMatch(String str, String pattern, int startIndex) {
        if (str.length() - startIndex < pattern.length()) return -1;
        int patternIndex = 0;
        for (int i = startIndex; i < str.length(); i++) {
            if (str.charAt(i) == pattern.charAt(patternIndex) || pattern.charAt(patternIndex) == '?') {
                patternIndex++;
                if (patternIndex == pattern.length() - 1) return i + 1;
            } else {
                patternIndex = 0;
            }
        }
        if (patternIndex == pattern.length() - 1) return str.length();
        return -1;
    }

}
