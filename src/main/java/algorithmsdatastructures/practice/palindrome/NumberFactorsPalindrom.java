package algorithmsdatastructures.practice.palindrome;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

//https://codingcompetitions.withgoogle.com/kickstart/round/00000000008caa74/0000000000acee89
public class NumberFactorsPalindrom {
    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            int caseNumber = 0;
            while (numberOfTests > 0) {
                caseNumber++;
                numberOfTests--;
                List<Long> factors = getFactors(Long.parseLong(br.readLine()));
                long count = factors.stream().filter(it -> isPalindrom(it)).count();
                sb.append(String.format("Case #%d: %d\n", caseNumber, count));
            }
            System.out.println(sb);
        }
    }

    private static List<Long> getFactors(long number) {
        var result = new HashSet<Long>();
        for (long i = 1; i <= number; i++) {
            if (number % i == 0) {
                result.add(i);
                if (number / i != i)
                    result.add(number / i);
            }
            if (number / i <= i) break;
        }
        return result.stream().collect(Collectors.toList());
    }

    private static boolean isPalindrom(Long number) {
        String numberStr = number.toString();
        return new StringBuilder(numberStr).reverse().toString().equals(numberStr);
    }
}
