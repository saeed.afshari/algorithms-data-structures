package algorithmsdatastructures.practice.graph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class InfectedTree {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var numberOfNodes = Integer.parseInt(br.readLine());
                Map<Integer, List<Integer>> adjacencyMap = new HashMap<>();
                for (int i = 1; i < numberOfNodes; i++) {
                    var splitted = br.readLine().split(" ");
                    var key = Integer.parseInt(splitted[0]);
                    var value = Integer.parseInt(splitted[1]);
                    List<Integer> aList = adjacencyMap.getOrDefault(key, new ArrayList<>());
                    aList.add(value);
                    adjacencyMap.put(key, aList);
                    List<Integer> aListValue = adjacencyMap.getOrDefault(value, new ArrayList<>());
                    aListValue.add(key);
                    adjacencyMap.put(value, aListValue);
                }

                adjacencyMap = getValidAdjacencyMap(adjacencyMap);

                var result = calculateMaximumSaves(adjacencyMap, 1, numberOfNodes);

                sb.append(result + "\n");
            }

            System.out.println(sb);
        }
    }

    private static Map<Integer, List<Integer>> getValidAdjacencyMap(Map<Integer, List<Integer>> adjacencyMap) {
        var aStack = new LinkedList<Integer>();
        aStack.add(1);
        var visited = new HashSet<Integer>();
        var result = new HashMap<Integer, List<Integer>>();
        while (!aStack.isEmpty()) {
            Integer node = aStack.pop();
            if (visited.contains(node)) continue;
            visited.add(node);
            List<Integer> values = result.getOrDefault(node, new ArrayList<>());
            for (int child : adjacencyMap.get(node)) {
                if (!visited.contains(child)) {
                    values.add(child);
                    aStack.add(child);
                }
            }
            if (values.size() > 0)
                result.put(node, values);
        }
        return result;
    }

    private static int calculateMaximumSaves(Map<Integer, List<Integer>> adjacencyMap, int root, int n) {
        var nodeChildren = new int[n + 1];
        nodeChildren = calculateNodeChildren(adjacencyMap, root, nodeChildren);

        return dfs(adjacencyMap, nodeChildren, root);
    }

    private static int dfs(Map<Integer, List<Integer>> adjacencyMap, int[] nodeChildren, int node) {
        List<Integer> children = adjacencyMap.get(node);
        if (children == null) {
            return 0;
        }
        if (children.size() == 1) {
            return nodeChildren[node] - 1;
        }
        var left = children.get(0);
        var right = children.get(1);
        var leftResult = nodeChildren[left] + dfs(adjacencyMap, nodeChildren, right);
        var rightResult = nodeChildren[right] + dfs(adjacencyMap, nodeChildren, left);
        return Integer.max(leftResult, rightResult);
    }

    private static int[] calculateNodeChildren(Map<Integer, List<Integer>> adj, int node, int[] nodeChildren) {
        List<Integer> children = adj.get(node);
        if (children == null) {
            nodeChildren[node] = 0;
            return nodeChildren;
        }
        for (var child : children) {
            calculateNodeChildren(adj, child, nodeChildren);
        }
        nodeChildren[node] = children.size();
        for (var child : children) {
            nodeChildren[node] += nodeChildren[child];
        }
        return nodeChildren;
    }
}
