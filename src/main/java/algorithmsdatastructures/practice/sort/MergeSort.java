package algorithmsdatastructures.practice.sort;

import java.util.Arrays;
import java.util.stream.Collectors;

public class MergeSort {

    public static void main(String[] args) {
        int[] items = {5, 4, 1, 0};
        new MergeSort().sort(items);
        System.out.println(Arrays.stream(items).mapToObj(it -> Integer.valueOf(it).toString()).collect(Collectors.joining(",")));
    }

    public void sort(int[] items) {
        mergeSort(0, items.length - 1, items);
    }

    private void mergeSort(int start, int end, int[] items) {
        if (start >= end) return;
        var mid = (start + end) / 2;
        mergeSort(start, mid, items);
        mergeSort(mid + 1, end, items);
        merge(start, mid, end, items);
    }

    private void merge(int start, int mid, int end, int[] items) {
        var sortedArray = new int[end - start + 1];
        int i = start;
        int j = mid + 1;
        int index = 0;
        while (i <= mid && j <= end) {
            if (items[i] <= items[j]) {
                sortedArray[index] = items[i];
                i++;
            } else {
                sortedArray[index] = items[j];
                j++;
            }
            index++;
        }
        for (; i <= mid; i++) {
            sortedArray[index] = items[i];
            index++;
        }
        for (; j <= end; j++) {
            sortedArray[index] = items[j];
            index++;
        }
        index = 0;
        for (i = start; i <= end; i++) {
            items[i] = sortedArray[index++];
        }
    }
}
