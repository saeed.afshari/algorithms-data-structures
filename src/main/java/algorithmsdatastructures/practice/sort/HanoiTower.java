package algorithmsdatastructures.practice.sort;

import java.util.Arrays;
import java.util.stream.Collectors;

public class HanoiTower {

    private static StringBuilder sb = new StringBuilder();

    public static void main(String[] args) {
        int[] items = {5, 4, 1, 0};
        new HanoiTower().printHanoiMoves(5, "S", "H", "G");
        System.out.println(Arrays.stream(items).mapToObj(it -> Integer.valueOf(it).toString()).collect(Collectors.joining(",")));
    }

    public void printHanoiMoves(int disks, String start, String help, String goal) {
        if (disks == 1) {
            sb.append(String.format("1 move from %s to %s", start, goal));
            return;
        }
        printHanoiMoves(disks - 1, start, help, goal);
    }
}
