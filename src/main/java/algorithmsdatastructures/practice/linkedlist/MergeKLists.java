package algorithmsdatastructures.practice.linkedlist;

public class MergeKLists {

    static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public ListNode mergeKLists(ListNode[] lists) {
        return findMinNode(lists);
    }

    private ListNode findMinNode(ListNode[] lists) {
        ListNode minNode = null;
        int index = 0;
        for (int i = 0; i < lists.length; i++) {
            var currentNode = lists[i];
            if (currentNode == null) {
                continue;
            }
            if (minNode == null) {
                minNode = currentNode;
                index = i;
                continue;
            }
            if (minNode.val > currentNode.val) {
                index = i;
                minNode = currentNode;
            }
        }
        if (minNode == null) return null;
        lists[index] = minNode.next;

        minNode.next = findMinNode(lists);
        return minNode;
    }
}
