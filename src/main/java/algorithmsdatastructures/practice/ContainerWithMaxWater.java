package algorithmsdatastructures.practice;

//https://leetcode.com/problems/container-with-most-water/
public class ContainerWithMaxWater {

    public static void main(String[] args){
        System.out.println(maxArea(new int[]{1,8,6,2,5,4,8,3,7}));
        System.out.println(maxArea(new int[]{1,1}));
        System.out.println(maxArea(new int[]{1,2,1}));
    }

    private static int maxArea(int[] height) {
        int left = maxIndexFromLeft(height);
        int right = maxIndexFromRight(height, left);
        return Integer.min(height[left], height[right]) * (right - left);
    }

    private static int maxIndexFromLeft(int[] height){
        int max = 0;
        int maxValue = height[0] * (height.length - 2);
        var stopIndex = height.length - 2;
        for(int i = 1; i <= stopIndex; i++){
            var currentHeight = height[i] * (height.length - i - 1);
            if(currentHeight>maxValue){
                max = i;
                maxValue = height[i] * (height.length - i - 1);
            } else if(currentHeight==maxValue && height[i] > height[max]){
                max = i;
                maxValue = height[i] * (height.length - i - 1);
            }
        }
        return max;
    }

    private static int maxIndexFromRight(int[] height, int leftIndex){
        int max = height.length - 1;
        int maxValue = height[max] * (height.length - 2);
        var stopIndex = leftIndex + 1;
        for(int i = height.length - 2; i >= stopIndex; i--){
            var currentHeight = height[i] * i;
            if(currentHeight>maxValue){
                max = i;
                maxValue = height[i] * max;
            } else if(currentHeight==maxValue && height[i] > height[max]){
                max = i;
                maxValue = height[i] * max;
            }
        }
        return max;
    }
}
