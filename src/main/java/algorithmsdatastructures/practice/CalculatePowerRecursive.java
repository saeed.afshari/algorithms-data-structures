package algorithmsdatastructures.practice;

public class CalculatePowerRecursive {

    //Mul = ((4%3)* 6)%3 = 0
    //Plus = (4%3) + (6%3) = 1 + 0 = 1
    //Minus = (4%3) - (6%3) = 1 - 0 = 1
    public static void main(String[] args) {
        System.out.println(powRecursive(5, 2, 11));
    }

    private static int powRecursive(int a, int n, int md) {
        if (n == 0) return 1;
        var x = powRecursive(a, n / 2, md);
        if (n % 2 == 0) {
            return ((x % md) * x) % md;
        } else {
            var result = (((x % md) * x) % md);
            return (((result % md) * a) % md);
        }
    }
}
