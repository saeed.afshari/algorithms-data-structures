package algorithmsdatastructures.codeforce786

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        repeat(numberOfTests) {
            val (x, y) = bufferedReader.readLine().split(" ").map { it.toInt() }
            val (a, b) = solve(x, y)
            println("$a $b")
        }
    }
}

fun solve(x: Int, y: Int): Pair<Int, Int> {
    if (y % x != 0) {
        return 0 to 0
    }

    return 1 to y / x
}
