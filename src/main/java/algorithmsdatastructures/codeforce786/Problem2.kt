package algorithmsdatastructures.codeforce786

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        repeat(numberOfTests) {
            val word = bufferedReader.readLine()
            println(solve(word))
        }
    }
}

fun solve(word: String): Int {
    val numberOfChars = 'z' - 'a' //25
    val firstChar = word[0] - 'a' //b -> 1 * 25
    val secondChar = if (word[1] < word[0]) {
        word[1] - 'a' + 1
    } else {
        word[1] - 'a'
    }
    return numberOfChars * firstChar + secondChar
}
