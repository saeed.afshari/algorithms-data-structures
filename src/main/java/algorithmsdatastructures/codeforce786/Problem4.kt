package algorithmsdatastructures.codeforce786

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        repeat(numberOfTests) {
            val size = bufferedReader.readLine().toInt()
            val a = bufferedReader.readLine().split(" ").map { it.toInt() }
            println(solve(a.toMutableList()))
        }
    }
}

fun solve(a: MutableList<Int>): String {
    for (i in (a.size % 2) until a.size step 2) {
        if (a[i] > a[i + 1])
            a[i] = a[i + 1].also { a[i + 1] = a[i] }
    }
    var sorted = true
    for (i in a.indices)
        if (i > 0 && a[i - 1] > a[i])
            sorted = false
    return if (sorted) "YES" else "NO"
}
