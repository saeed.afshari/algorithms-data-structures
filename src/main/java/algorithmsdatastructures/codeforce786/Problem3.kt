package algorithmsdatastructures.codeforce786

import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.pow

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        repeat(numberOfTests) {
            val s = bufferedReader.readLine()
            val t = bufferedReader.readLine()
            println(solve(s, t))
        }
    }
}

fun solve(s: String, t: String): Long {
    if (t.length == 1 && t == "a") return 1
    val numberOfACharsInT = t.toCharArray().filter { it == 'a' }.size
    if (numberOfACharsInT == t.length) {
        if (t.length > 1) return -1
        if (s.length >= t.length) return 0
        return -1
    }
    if (numberOfACharsInT > 0) return -1
    return 2.00.pow(s.length).toLong()
}
