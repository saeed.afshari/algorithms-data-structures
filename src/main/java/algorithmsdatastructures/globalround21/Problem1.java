package algorithmsdatastructures.globalround21;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Problem1 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var z = Integer.parseInt(br.readLine().split(" ")[1]);
                var a = Arrays.stream(br.readLine().split(" ")).map(it -> Integer.parseInt(it)).collect(Collectors.toList());

                int max = Integer.MIN_VALUE;
                for (int i = 0; i < a.size(); i++){
                    if((a.get(i) | z) > max){
                        max = a.get(i) | z;
                    }
                }

                sb.append((max | z) + "\n");



            }
            System.out.println(sb);
        }
    }
}
