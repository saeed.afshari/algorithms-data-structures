package algorithmsdatastructures.globalround21;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class Problem4 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                br.readLine();
                var a = Arrays.stream(br.readLine().split(" ")).map(it -> Integer.parseInt(it)).collect(Collectors.toList());
                var adjacencyMap = new HashMap<Integer, List<Integer>>();
                for (int i = 0; i < a.size(); i++) {
                    for (int j = i + 1; j < a.size(); j++) {
                        var aI = a.get(i);
                        var aJ = a.get(j);
                        int min = findMin(a, i, j);
                        int max = findMax(a, i, j);
                        if ((min == aI && max == aJ) || (min == aJ && max == aI)) {
                            var valueI = adjacencyMap.getOrDefault(i + 1, new ArrayList<>());
                            valueI.add(j + 1);
                            adjacencyMap.put(i + 1, valueI);

                            var valueJ = adjacencyMap.getOrDefault(j + 1, new ArrayList<>());
                            valueJ.add(i + 1);
                            adjacencyMap.put(j + 1, valueJ);
                        }
                    }
                }

                int[] path = bfs(adjacencyMap, 1, a.size());
                var sum = 0;
                int i = a.size();
                if (path[i] == 0) {
                    sb.append("0\n");
                } else if (path[i] == 1) {
                    sb.append("1\n");
                } else {
                    while (i != 0) {
                        sum++;
                        if (path[i] == 1)
                            break;
                        i = path[i];
                    }
                    sb.append(sum + "\n");
                }
            }
            System.out.println(sb);
        }
    }

    private static int[] bfs(HashMap<Integer, List<Integer>> adjacencyMap, int root, int n) {
        var visitedSet = new HashSet<Integer>();
        var queue = new LinkedList<Integer>();
        queue.add(root);
        var path = new int[n + 1];
        while (!queue.isEmpty()) {
            Integer node = queue.remove();
            visitedSet.add(node);
            if (adjacencyMap.containsKey(node)) {
                for (var child : adjacencyMap.get(node)) {
                    if (visitedSet.contains(child)) continue;
                    queue.offer(child);
                    if(path[child] == 0) path[child] = node;
                }
            }
        }
        return path;
    }

    private static int findMin(List<Integer> a, int i, int j) {
        int min = Integer.MAX_VALUE;
        for (var x = i; x <= j; x++) {
            min = Math.min(min, a.get(x));
        }
        return min;
    }

    private static int findMax(List<Integer> a, int i, int j) {
        int max = Integer.MIN_VALUE;
        for (var x = i; x <= j; x++) {
            max = Math.max(max, a.get(x));
        }
        return max;
    }
}
