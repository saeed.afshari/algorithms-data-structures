package algorithmsdatastructures.dp;

public class CoinChange {
    private static int[][] dp;

    public static void main(String[] args) {
        var values = new int[]{1, 4, 5};
        dp = new int[3][14];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 14; j++) {
                dp[i][j] = -1;
            }
        }
        System.out.println(coinChange(0, 3, 13, values));
    }

    private static int coinChange(int index, int n, int amount, int[] values) {
        if (amount == 0) {
            return 0;
        }
        if (index == n) return dp[0].length + 1;
        if (dp[index][amount] != -1) {
            return dp[index][amount];
        }
        int min = Integer.MAX_VALUE;
        for (int i = 0; i * values[index] <= amount; i++) {
            int newAmount = amount - i * values[index];
            min = Integer.min(i + coinChange(index + 1, n, newAmount, values), min);
        }
        dp[index][amount] = min;
        return dp[index][amount];
    }
}
