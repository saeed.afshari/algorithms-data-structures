package algorithmsdatastructures.div4806;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Problem6 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var n = Integer.parseInt(br.readLine());
                var splitted = br.readLine().split(" ");
                var a = new int[n+1];
                for (int i = 1; i <= n; i++) {
                    int value = Integer.parseInt(splitted[i-1]);
                    a[i] = value;
                }
                var items = new ArrayList<Integer>();
                for (int i = 1; i <= n; i++) {
                    int value = a[i];
                    if (value < i) {
                        items.add(i);
                    }
                }
                int[] arraySum = new int[items.size()];
                for (int i = 1; i < items.size(); i++) {
                    arraySum[i] = arraySum[i - 1];
                    if(items.get(i-1) < a[items.get(i)])
                        arraySum[i] += 1;
                }
                int sum = 0;
                for (int i = 1; i < arraySum.length; i++) {
                    sum += arraySum[i];
                }
                sb.append(sum + "\n");
            }

            System.out.println(sb);
        }
    }
}
