package algorithmsdatastructures.div4806;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                br.readLine();
                var str = br.readLine();
                var aSet = new HashSet<Character>();
                var sum = 0;
                for (int i = 0; i < str.length(); i++) {
                    if (aSet.contains(str.charAt(i))) sum++;
                    else {
                        sum += 2;
                        aSet.add(str.charAt(i));
                    }
                }
                sb.append(sum + "\n");
            }

            System.out.println(sb);
        }
    }
}
