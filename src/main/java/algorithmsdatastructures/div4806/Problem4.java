package algorithmsdatastructures.div4806;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Problem4 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var numberOfStrings = Integer.parseInt(br.readLine());
                String[] strs = new String[numberOfStrings];
                for (int i = 0; i < numberOfStrings; i++) {
                    strs[i] = br.readLine();
                }

                var strLengthMap = new HashMap<Integer, Set<String>>();
                for (int i = 0; i < numberOfStrings; i++) {
                    Set<String> value = strLengthMap.getOrDefault(strs[i].length(), new HashSet<>());
                    value.add(strs[i]);
                    strLengthMap.put(strs[i].length(), value);
                }

                var sbResult = new StringBuilder();
                for (int i = 0; i < strs.length; i++) {
                    var pairs = getPairs(strs[i].length());
                    var result = "0";
                    for (var pair : pairs) {
                        if (find(strLengthMap, pair, strs[i])) {
                            result = "1";
                            break;
                        }
                    }
                    sbResult.append(result);
                }

                sb.append(sbResult + "\n");
            }

            System.out.println(sb);
        }
    }

    private static boolean find(HashMap<Integer, Set<String>> strLengthMap, Pair pair, String item) {
        if(!strLengthMap.containsKey(pair.i) || !strLengthMap.containsKey(pair.j)) return false;
        for (String left : strLengthMap.get(pair.i)) {
            for (String right : strLengthMap.get(pair.j)) {
                if((left + right).equals(item)) return true;
            }
        }
        return false;
    }

    private static List<Pair> getPairs(int length) {
        var pairs = new ArrayList<Pair>();
        for (int i = 1; i <= length - 1; i++) {
            for (int j = length - 1; j >= 1; j--) {
                if (i + j == length)
                    pairs.add(new Pair(i, j));
            }
        }
        return pairs;
    }

    private static class Pair {

        private final int i;
        private final int j;

        Pair(int i, int j) {
            this.i = i;
            this.j = j;
        }
    }
}
