package algorithmsdatastructures.div4806;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem8 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                String[] splitted = br.readLine().split(" ");
                var n = Integer.parseInt(splitted[0]);
                var k = Integer.parseInt(splitted[1]);
                var chests = new long[n];
                splitted = br.readLine().split(" ");

                for (int i = 0; i < n; i++) {
                    chests[i] = Long.parseLong(splitted[i]);
                }
                long[][] dp = new long[n][30];
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < 30; j++) {
                        dp[i][j] = Integer.MIN_VALUE;
                    }
                }
                sb.append(gainMaximumCoins(chests, 0, k, 0, dp) + "\n");
            }

            System.out.println(sb);
        }
    }

    private static long gainMaximumCoins(long[] chests, int index, int k, int divider, long[][] dp) {
        if (index >= chests.length || divider >= 30) return 0;

        if (dp[index][divider] != Integer.MIN_VALUE) return dp[index][divider];

        long chestValue = chests[index];
        for (int i = 0; i < divider; i++) {
            chestValue /= 2;
        }
        long useGoodKeyCost = chestValue + gainMaximumCoins(chests, index + 1, k, divider, dp) - k;
        long useBadKeyCost = chestValue / 2 + gainMaximumCoins(chests, index + 1, k, divider + 1, dp);
        dp[index][divider] = Long.max(useGoodKeyCost, useBadKeyCost);
        return dp[index][divider];
    }
}
