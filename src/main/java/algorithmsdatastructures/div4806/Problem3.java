package algorithmsdatastructures.div4806;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var numberOfWheels = Integer.parseInt(br.readLine());
                var splitted = br.readLine().split(" ");
                int[] values = new int[numberOfWheels];
                for (int i = 0; i < numberOfWheels; i++) {
                    values[i] = Integer.parseInt(splitted[i]);
                }
                var result = new ArrayList<Integer>();
                for (int i = 0; i < numberOfWheels; i++) {
                    var str = br.readLine().split(" ")[1];
                    var sum = values[i];
                    for (int ch = 0; ch < str.length(); ch++) {
                        if (str.charAt(ch) == 'U') {
                            sum--;
                            if (sum == -1) sum = 9;
                        } else {
                            sum++;
                            if (sum == 10) sum = 0;
                        }
                    }
                    result.add(sum);
                }
                sb.append(result.stream().map(String::valueOf).collect(Collectors.joining(" ")) + "\n");
            }

            System.out.println(sb);
        }
    }
}
