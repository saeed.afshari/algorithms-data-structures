package algorithmsdatastructures.vasyaandcoins;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class VasyaAndCoins {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        var lines = new ArrayList<String>();
        int numberOfTests = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < numberOfTests; i++) {
            lines.add(scanner.nextLine());
        }
        List<Integer> minimumList = findMinimum(lines);

        System.out.println(minimumList.stream().map(it -> it.toString()).collect(Collectors.joining("\n")));
    }

    private static List<Integer> findMinimum(List<String> lines) {
        List<Integer> result = new ArrayList<>(lines.size());
        for (var line : lines) {
            String[] coinsAmount = line.split(" ");
            Integer numberOf1Burle = Integer.parseInt(coinsAmount[0]);
            Integer numberOf2Burle = Integer.parseInt(coinsAmount[1]);
            if (numberOf1Burle == 0) {
                result.add(1);
            } else if (numberOf2Burle == 0) {
                result.add(numberOf1Burle + 1);
            } else {
                result.add(numberOf2Burle * 2 + numberOf1Burle + 1);
            }
        }
        return result;
    }
}
