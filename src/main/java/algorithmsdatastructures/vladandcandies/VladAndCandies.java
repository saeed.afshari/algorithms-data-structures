package algorithmsdatastructures.vladandcandies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class VladAndCandies {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfTests = Integer.parseInt(scanner.nextLine());
        var tests = new ArrayList<List<Candy>>();
        for (int i = 0; i < numberOfTests; i++) {
            var numberOfTypes = Integer.parseInt(scanner.nextLine());
            var candyList = new ArrayList<Candy>(numberOfTypes);
            var types = scanner.nextLine().split(" ");
            for (int j = 0; j < numberOfTypes; j++) {
                candyList.add(new Candy(j, Integer.parseInt(types[j])));
            }
            tests.add(candyList);
        }
        List<String> minimumList = possibleToEatAllCandies(tests);

        System.out.println(String.join("\n", minimumList));
    }

    private static class Candy implements Comparable<Candy> {
        private final Integer type;
        private final Integer numberOfCandies;

        Candy(Integer type, Integer numberOfCandies) {
            this.type = type;
            this.numberOfCandies = numberOfCandies;
        }

        @Override
        public int compareTo(Candy second) {
            return second.numberOfCandies.compareTo(this.numberOfCandies);
        }
    }

    private static List<String> possibleToEatAllCandies(List<List<Candy>> tests) {
        var result = new ArrayList<String>();
        for (var test : tests) {
            if (test.size() == 1) {
                result.add(test.get(0).numberOfCandies > 1 ? "NO" : "YES");
                continue;
            }
            Collections.sort(test);

            var biggest = test.get(0);
            var smaller = test.get(1);
            result.add((biggest.numberOfCandies - smaller.numberOfCandies) <= 1 ? "YES" : "NO");
        }
        return result;
    }
}
