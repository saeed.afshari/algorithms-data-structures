package algorithmsdatastructures.codeforces.div2.number819;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                var splitted = br.readLine().split(" ");
                var n = Integer.parseInt(splitted[0]);
                var k = Integer.parseInt(splitted[1]);

                if (n > k) {
                    sb.append("NO\n");
                    continue;
                } else if (n == 1) {
                    sb.append("YES\n");
                    sb.append(k + "\n");
                    continue;
                }
                var a = new ArrayList<Integer>();
                if (n % 2 != 0) {
                    for (int i = 0; i < n - 1; i++) {
                        a.add(1);
                    }
                    a.add(k - n + 1);
                    sb.append("YES\n");
                    sb.append(a.stream().map(it -> String.valueOf(it)).collect(Collectors.joining(" ")));
                    sb.append("\n");
                } else {
                    if (k % 2 != 0) {
                        sb.append("NO\n");
                    } else {
                        for (int i = 0; i < n - 2; i++) {
                            a.add(1);
                        }
                        a.add((k - n + 2) / 2);
                        sb.append("YES\n");
                        sb.append(a.stream().map(it -> String.valueOf(it)).collect(Collectors.joining(" ")));
                        sb.append("\n");
                    }
                }
            }

            System.out.println(sb);
        }
    }
}
