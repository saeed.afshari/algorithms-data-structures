package algorithmsdatastructures.codeforces.div2.number819;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem1 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                var n = Integer.parseInt(br.readLine());
                var a = new int[n];
                var splitted = br.readLine().split(" ");
                int min = Integer.MAX_VALUE;
                int max = 0;
                int minIndex = 0;
                int maxIndex = 0;
                for (int i = 0; i < n; i++) {
                    var value = Integer.parseInt(splitted[i]);
                    a[i] = value;
                    if (value < min) {
                        min = value;
                        minIndex = i;
                    }
                    if (value >= max) {
                        max = value;
                        maxIndex = i;
                    }
                }

                if (minIndex == 0 && maxIndex == n) {
                    sb.append((max - min) + "\n");
                } else if (minIndex == 0) {
                    sb.append((max - min) + "\n");
                } else if (maxIndex == n - 1) {
                    sb.append((max - min) + "\n");
                } else if (n <= 2) {
                    sb.append((max - min) + "\n");
                } else {
                    var r = 0;
                    for (int i = 0; i < n; i++) {
                        if (i != n - 1) {
                            if (a[i] >= a[i + 1]) {
                                r = Integer.max(r, a[i] - a[i + 1]);
                            }
                        } else {
                            if (a[i] >= a[0]) {
                                r = Integer.max(r, a[i] - a[0]);
                            }
                        }
                    }

                    if ((a[0] == min || a[n - 1] == max) && a[0] <= a[n - 1]) {
                        var tmpMax = 0;
                        var tmpMin = a[0];
                        for (int i = 1; i < n; i++) {
                            var value = a[i];
                            if (value >= tmpMax) {
                                tmpMax = value;
                            }
                        }

                        var tmpMax1 = 0;
                        var tmpMin1 = a[n - 1];
                        for (int i = 0; i < n - 1; i++) {
                            var value = a[i];
                            if (value >= tmpMax1) {
                                tmpMax1 = value;
                            }
                        }

                        var tmpMax2 = a[0];
                        var tmpMin2 = Integer.MAX_VALUE;
                        for (int i = 1; i < n; i++) {
                            var value = a[i];
                            if (value < tmpMin2) {
                                tmpMin2 = value;
                            }
                        }

                        var tmpMax3 = a[n - 1];
                        var tmpMin3 = Integer.MAX_VALUE;
                        for (int i = 0; i < n - 1; i++) {
                            var value = a[i];
                            if (value < tmpMin3) {
                                tmpMin3 = value;
                            }
                        }

                        var r1 = tmpMax - tmpMin;
                        var r2 = tmpMax1 - tmpMin1;
                        var r3 = tmpMax2 - tmpMin2;
                        var r4 = tmpMax3 - tmpMin3;

                        r = Integer.max(r, Integer.max(Integer.max(Integer.max(r1, r2), r3), r4));
                    }
                    sb.append(r + "\n");
                }
            }

            System.out.println(sb);
        }
    }
}
