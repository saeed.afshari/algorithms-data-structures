package algorithmsdatastructures.codeforces.div2.number818;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                var n = Integer.parseInt(br.readLine());
                var splitted = br.readLine().split(" ");
                var a = new long[n];
                var b = new long[n];
                for (int i = 0; i < n; i++) {
                    a[i] = Long.parseLong(splitted[i]);
                }
                splitted = br.readLine().split(" ");
                for (int i = 0; i < n; i++) {
                    b[i] = Long.parseLong(splitted[i]);
                }
                boolean impossible = false;
                var prefix = new ArrayList<Long>();
                for (int i = 0; i < n; i++) {
                    if (a[i] != b[i]) {
                        if (a[i] > b[i]) {
                            impossible = true;
                            break;
                        }
                        if (i == n - 1) {
                            if (a[i] > b[0]) {
                                impossible = true;
                                break;
                            }
                            var rooms = b[0] - a[i] + 1;
                            prefix.add(rooms);
                        } else {
                            if (a[i] > b[i + 1]) {
                                impossible = true;
                                break;
                            }
                            var rooms = b[i + 1] - a[i] + 1;
                            prefix.add(rooms);
                        }
                    }
                }
                if (prefix.size() == 0 && !impossible) {
                    sb.append("YES\n");
                    continue;
                }
                if (impossible) {
                    sb.append("NO\n");
                    continue;
                }
                int index = 0;
                for (int i = 0; i < n; i++) {
                    if (a[i] != b[i]) {
                        if (a[i] + prefix.get(index) < b[i]) {
                            impossible = true;
                            break;
                        }
                        index++;
                    }
                }
                if (impossible) {
                    sb.append("NO\n");
                    continue;
                }
                sb.append("YES\n");
            }

            System.out.println(sb);
        }
    }
}
