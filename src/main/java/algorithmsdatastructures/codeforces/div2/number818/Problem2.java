package algorithmsdatastructures.codeforces.div2.number818;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                String[] splitted = br.readLine().split(" ");
                var n = Integer.parseInt(splitted[0]);
                var k = Integer.parseInt(splitted[1]);
                var r = Integer.parseInt(splitted[2]);
                var c = Integer.parseInt(splitted[3]);

                var rowIndex = r == k || (r > k && r % k == 0) ? k : r % k;
                var colIndex = c == k || (c > k && c % k == 0) ? k : c % k;

                var startXIndex = colIndex;
                if (rowIndex != 1) {
                    var tmp = colIndex - (rowIndex - 1);
                    if (tmp <= 0) {
                        startXIndex = k + tmp;
                    } else {
                        startXIndex = tmp;
                    }
                }

                for (int i = 1; i <= n; i++) {
                    var tmpStartIndex = startXIndex;
                    for (int j = 1; j <= n; j++) {
                        int rem = j % k != 0 ? j % k : k;
                        if (rem == tmpStartIndex) {
                            sb.append("X");
                        } else {
                            sb.append(".");
                        }
                    }
                    startXIndex++;
                    if (startXIndex > k) {
                        startXIndex = startXIndex % k != 0 ? startXIndex % k : k;
                    }
                    sb.append("\n");
                }
            }

            System.out.println(sb);
        }
    }
}
