package algorithmsdatastructures.codeforces.div2.number815;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem1 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                var parts = br.readLine().split(" ");
                var a = Long.parseLong(parts[0]);
                var b = Long.parseLong(parts[1]);
                var c = Long.parseLong(parts[2]);
                var d = Long.parseLong(parts[3]);

                long ad = a * d;
                long bc = b * c;
                if (ad == bc) {
                    sb.append("0\n");
                    continue;
                }
                if (a == 0 || c == 0) {
                    sb.append("1\n");
                    continue;
                }
                var min = Long.min(ad, bc);
                var max = Long.max(ad, bc);
                if (max % min == 0)
                    sb.append("1\n");
                else
                    sb.append("2\n");
            }

            System.out.println(sb);
        }
    }
}
