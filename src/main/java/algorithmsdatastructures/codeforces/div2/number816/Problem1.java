package algorithmsdatastructures.codeforces.div2.number816;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem1 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var parts = br.readLine().split(" ");
                var n = Long.parseLong(parts[0]);
                var m = Long.parseLong(parts[1]);
                if (n < m) {
                    var tmp = n;
                    n = m;
                    m = tmp;
                }
                if (n == 1 && m == 1) {
                    sb.append("0\n");
                } else if (n == 1 || m == 1) {
                    sb.append(Long.max(n, m) + "\n");
                } else {
                    if (m % 2 == 0) {
                        sb.append(((m / 2 - 1) * 2) + n + m + "\n");
                    } else {
                        sb.append(((m / 2) * 2) + n + ((m / 2) * 2) + "\n");
                    }
                }
            }

            System.out.println(sb);
        }
    }
}
