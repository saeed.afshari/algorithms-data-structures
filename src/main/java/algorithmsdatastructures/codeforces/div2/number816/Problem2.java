package algorithmsdatastructures.codeforces.div2.number816;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var parts = br.readLine().split(" ");
                var n = Long.parseLong(parts[0]);
                var k = Long.parseLong(parts[1]);
                var b = Long.parseLong(parts[2]);
                var s = Long.parseLong(parts[3]);

//                if (s == 0L) {
//                    sb.append("0\n");
//                } else if ((k * b <= s) && (s / k >= b && )) {
//                    sb.append("1\n");
//                } else {
//                    sb.append("-1\n");
//                }
            }

            System.out.println(sb);
        }
    }
}
