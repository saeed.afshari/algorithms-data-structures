package algorithmsdatastructures.codeforces.div4.number817;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                var n = Integer.parseInt(br.readLine());

                var firstLine = br.readLine();
                var secondLine = br.readLine();
                var flag = true;
                for (int i = 0; i < n; i++) {
                    if (firstLine.charAt(i) == 'R' || secondLine.charAt(i) == 'R') {
                        if (firstLine.charAt(i) != secondLine.charAt(i)) {
                            flag = false;
                            break;
                        }
                    }
                }

                if (flag) {
                    sb.append("YES\n");
                } else {
                    sb.append("NO\n");
                }
            }

            System.out.println(sb);
        }
    }
}
