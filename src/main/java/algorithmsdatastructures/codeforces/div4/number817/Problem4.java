package algorithmsdatastructures.codeforces.div4.number817;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem4 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                var n = Integer.parseInt(br.readLine());
                var people = br.readLine();

                var mid = n / 2;
                var prefixes = new long[mid];
                var suffixes = new long[n % 2 != 0 ? mid + 1 : mid];
                var pIndex = 0;
                for (int i = 0; i < mid; i++) {
                    if (people.charAt(i) == 'L') {
                        prefixes[pIndex++] = (people.length() - i - 1) - i;
                    }
                }
                var sIndex = 0;
                if (n % 2 != 0) {
                    mid = mid + 1;
                }
                for (int i = people.length() - 1; i >= mid; i--) {
                    if (people.charAt(i) == 'R') {
                        suffixes[sIndex++] = i - (people.length() - i - 1);
                    }
                }

                int p = 0;
                int s = 0;
                int index = 0;
                var mix = new long[n];
                while (p < prefixes.length && s < suffixes.length) {
                    if (prefixes[p] >= suffixes[s]) {
                        mix[index] = prefixes[p];
                        p++;
                    } else {
                        mix[index] = suffixes[s];
                        s++;
                    }
                    index++;
                }
                while (p < prefixes.length) {
                    mix[index] = prefixes[p];
                    p++;
                    index++;
                }
                while (s < suffixes.length) {
                    mix[index] = suffixes[s];
                    s++;
                    index++;
                }

                var sum = new long[mix.length + 1];
                for (int i = 1; i <= mix.length; i++) {
                    sum[i] = sum[i - 1] + mix[i - 1];
                }

                var currentValue = getValue(people);

                for (int k = 1; k <= n; k++) {
                    var r = getMaxValue(k, currentValue, mix, sum);
                    sb.append(r + " ");
                }
                sb.append("\n");
            }

            System.out.println(sb);
        }
    }

    private static long getValue(String people) {
        long value = 0L;
        for (int i = 0; i < people.length(); i++) {
            if (people.charAt(i) == 'L') {
                value += i;
            } else {
                value += people.length() - i - 1;
            }
        }
        return value;
    }

    private static long getMaxValue(int k, long currentValue, long[] mix, long[] sum) {
        if (k > mix.length) {
            return sum[sum.length - 1] + currentValue;
        }
        return sum[k] - sum[0] + currentValue;
    }
}
