package algorithmsdatastructures.codeforces.div4.number817;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                var n = Integer.parseInt(br.readLine());

                var firstPersonWords = br.readLine().split(" ");
                var secondPersonWords = br.readLine().split(" ");
                var thirdPersonWords = br.readLine().split(" ");

                var aScore = 0;
                var bScore = 0;
                var cScore = 0;

                var scoreMap = new HashMap<String, Integer>();

                for (int i = 0; i < n; i++) {
                    var a = firstPersonWords[i];
                    var b = secondPersonWords[i];
                    var c = thirdPersonWords[i];

                    Integer value = scoreMap.getOrDefault(a, 0);
                    value++;
                    scoreMap.put(a, value);
                    value = scoreMap.getOrDefault(b, 0);
                    value++;
                    scoreMap.put(b, value);
                    value = scoreMap.getOrDefault(c, 0);
                    value++;
                    scoreMap.put(c, value);
                }

                for (int i = 0; i < n; i++) {
                    var a = firstPersonWords[i];
                    var b = secondPersonWords[i];
                    var c = thirdPersonWords[i];

                    if (scoreMap.get(a) == 2) {
                        aScore++;
                    } else if (scoreMap.get(a) == 1) {
                        aScore += 3;
                    }

                    if (scoreMap.get(b) == 2) {
                        bScore++;
                    } else if (scoreMap.get(b) == 1) {
                        bScore += 3;
                    }

                    if (scoreMap.get(c) == 2) {
                        cScore++;
                    } else if (scoreMap.get(c) == 1) {
                        cScore += 3;
                    }
                }

                sb.append(String.format("%d %d %d\n", aScore, bScore, cScore));
            }

            System.out.println(sb);
        }
    }
}
