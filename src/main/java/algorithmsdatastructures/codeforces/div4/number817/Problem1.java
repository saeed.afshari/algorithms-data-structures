package algorithmsdatastructures.codeforces.div4.number817;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;

public class Problem1 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                var n = Integer.parseInt(br.readLine());
                var prem = br.readLine();
                if (n != 5) {
                    sb.append("NO\n");
                } else {
                    var set = new HashSet<Character>();
                    set.add('T');
                    set.add('i');
                    set.add('m');
                    set.add('u');
                    set.add('r');
                    var map = new HashMap<Character, Integer>();
                    var flag = true;
                    for (int i = 0; i < prem.length(); i++) {
                        if (!set.contains(prem.charAt(i))) {
                            flag = false;
                            break;
                        }
                        Integer value = map.getOrDefault(prem.charAt(i), 0);
                        if (value != 0) {
                            flag = false;
                            break;
                        }
                        value++;
                        map.put(prem.charAt(i), value);
                    }
                    if (!flag) {
                        sb.append("NO\n");
                    } else {
                        sb.append("YES\n");
                    }
                }
            }

            System.out.println(sb);
        }
    }
}
