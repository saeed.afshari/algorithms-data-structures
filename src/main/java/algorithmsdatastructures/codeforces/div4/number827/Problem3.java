package algorithmsdatastructures.codeforces.div4.number827;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            String splitted = "";
            while (numberOfTests > 0) {
                numberOfTests--;
                if (splitted.isEmpty() || splitted.isBlank())
                    splitted = br.readLine();
                while (splitted.isBlank() || splitted.isEmpty()) {
                    splitted = br.readLine();
                }
                char[][] colors = new char[8][8];
                for (int i = 0; i < 8; i++) {
                    if (splitted.isBlank() || splitted.isEmpty()) break;
                    for (int j = 0; j < 8; j++) {
                        colors[i][j] = splitted.charAt(j);
                    }
                    if (numberOfTests == 0 && i == 7) break;

                    splitted = br.readLine();
                }

                sb.append(getLastColor(colors) + "\n");
            }

            System.out.println(sb);
        }
    }

    private static char getLastColor(char[][] colors) {
        int sumR = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (colors[i][j] == 'R') {
                    sumR++;
                } else {
                    break;
                }
            }
            if (sumR == 8) {
                return 'R';
            }
            sumR = 0;
        }
        return 'B';
    }
}
