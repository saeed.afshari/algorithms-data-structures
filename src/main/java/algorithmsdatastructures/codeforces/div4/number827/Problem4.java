package algorithmsdatastructures.codeforces.div4.number827;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Problem4 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                int n = Integer.parseInt(br.readLine());
                String[] splitted = br.readLine().split(" ");
                int[] items = new int[n];
                for (int i = 0; i < n; i++) {
                    items[i] = Integer.parseInt(splitted[i]);
                }

                var aMap = new HashMap<Integer, Integer>();
                for (int i = n - 1; i >= 0; i--) {
                    var value = aMap.getOrDefault(items[i], -1);
                    if (value == -1) {
                        aMap.put(items[i], i);
                    }
                }

                for (int i = 1; i <= 1000; i++) {
                    for (int j = 1; j <= 1000; j++) {
                        isCoPrime(i, j);
                    }
                }

                int maxIJ = Integer.MIN_VALUE;
                for (int i = 1; i < 1001; i++) {
                    for (int j = 1; j < 1001; j++) {
                        if (aMap.containsKey(i) && aMap.containsKey(j) && dp[i][j]) {
                            maxIJ = Integer.max(aMap.get(j) + aMap.get(i) + 2, maxIJ);
                        }
                    }
                }

                if (maxIJ != Integer.MIN_VALUE) {
                    sb.append(maxIJ + "\n");
                } else {
                    sb.append("-1\n");
                }
            }

            System.out.println(sb);
        }
    }

    private static boolean[][] dp = new boolean[1001][1001];

    private static boolean isCoPrime(int a, int b) {
        if (dp[a][b]) return dp[a][b];
        if (dp[b][a]) return dp[b][a];
        boolean result = gcd(a, b) == 1;
        dp[a][b] = result;
        dp[b][a] = result;
        return result;
    }

    private static int gcd(int a, int b) {
        if (a == 0 || b == 0)
            return 0;

        if (a == b)
            return a;

        if (a > b)
            return gcd(a - b, b);

        return gcd(a, b - a);
    }
}
