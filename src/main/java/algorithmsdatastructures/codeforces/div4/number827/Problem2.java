package algorithmsdatastructures.codeforces.div4.number827;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                int n = Integer.parseInt(br.readLine());
                String[] splitted = br.readLine().split(" ");
                int[] items = new int[n];
                for (int i = 0; i < n; i++) {
                    items[i] = Integer.parseInt(splitted[i]);
                }

                Arrays.sort(items);

                var flag = true;
                for (int i = 1; i < n; i++) {
                    if (items[i - 1] >= items[i]) {
                        flag = false;
                        break;
                    }
                }

                if (flag) sb.append("YES\n");
                else sb.append("NO\n");
            }

            System.out.println(sb);
        }
    }
}
