package algorithmsdatastructures.codeforces.div4.number827;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem1 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                String[] s = br.readLine().split(" ");
                int a = Integer.parseInt(s[0]);
                int b = Integer.parseInt(s[1]);
                int c = Integer.parseInt(s[2]);

                if (a == b + c) {
                    sb.append("YES\n");
                } else if (b == a + c) {
                    sb.append("YES\n");
                } else if (c == a + b) {
                    sb.append("YES\n");
                } else {
                    sb.append("NO\n");
                }

            }

            System.out.println(sb);
        }
    }
}
