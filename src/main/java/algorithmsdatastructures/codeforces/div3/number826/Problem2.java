package algorithmsdatastructures.codeforces.div3.number826;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                int n = Integer.parseInt(br.readLine());

                if (n == 3) {
                    sb.append("-1\n");
                } else {
                    sb.append(n + " ");
                    n--;
                    if (n == 0) {
                        sb.append("\n");
                        continue;
                    }
                    sb.append(n + " ");
                    if (n == 0) {
                        sb.append("\n");
                        continue;
                    }
                    int i = 1;
                    n--;
                    while (n > 0) {
                        sb.append(i + " ");
                        n--;
                        i++;
                    }
                    sb.append("\n");
                }
            }

            System.out.println(sb);
        }
    }

    private static String compareSize(String size1, String size2) {
        var aMap = new HashMap<Character, Integer>();
        aMap.put('S', 1);
        aMap.put('M', 2);
        aMap.put('L', 3);
        char lastChar1 = size1.charAt(size1.length() - 1);
        char lastChar2 = size2.charAt(size2.length() - 1);
        if (aMap.get(lastChar1) > aMap.get(lastChar2)) {
            return ">";
        } else if (aMap.get(lastChar1) < aMap.get(lastChar2)) {
            return "<";
        }

        if (size1.length() == size2.length()) return "=";
        if (size1.length() > size2.length()) {
            if (lastChar1 == 'S') return "<";
            return ">";
        } else {
            if (lastChar1 == 'S') return ">";
            return "<";
        }
    }
}
