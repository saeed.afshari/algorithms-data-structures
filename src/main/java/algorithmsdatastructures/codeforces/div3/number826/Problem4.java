package algorithmsdatastructures.codeforces.div3.number826;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem4 {

    private static int numberOfSwaps;

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                int n = Integer.parseInt(br.readLine());
                String[] splitted = br.readLine().split(" ");
                if (n == 1) {
                    sb.append("0\n");
                    numberOfSwaps = 0;
                    continue;
                }
                int[] items = new int[n];
                for (int i = 0; i < n; i++) {
                    items[i] = Integer.parseInt(splitted[i]);
                }

                numberOfSwaps = sortLeaves(items);

                swapRecursively(items, 0, n);

                if (isSorted(items)) sb.append(numberOfSwaps + "\n");
                else sb.append("-1\n");
            }

            System.out.println(sb);
        }
    }

    private static int sortLeaves(int[] items) {
        int numberOfSwaps = 0;
        for (int i = 0; i < items.length; i += 2) {
            if (items[i] > items[i + 1]) {
                int tmp = items[i];
                items[i] = items[i + 1];
                items[i + 1] = tmp;
                numberOfSwaps++;
            }
        }
        return numberOfSwaps;
    }

    private static void swapRecursively(int[] items, int left, int rightExclusive) {
        if (left >= rightExclusive) return;
        int mid = (left + rightExclusive) / 2;
        if (mid == 0 || mid == rightExclusive - 1) return;
        swapRecursively(items, left, mid);
        swapRecursively(items, mid, rightExclusive);
        if (items[mid - 1] > items[mid]) {
            swapLeftAndRight(items, left, mid, rightExclusive);
            numberOfSwaps++;
        }
    }

    private static void swapLeftAndRight(int[] items, int left, int mid, int rightExclusive) {
        if (left >= mid || rightExclusive <= mid) return;
        int[] tmp = new int[mid - left];
        for (int i = left; i < mid; i++) {
            tmp[i - left] = items[i];
        }
        int rightIndex = mid;
        for (int i = left; i < mid; i++) {
            items[i] = items[rightIndex++];
        }
        for (int i = mid; i < rightExclusive; i++) {
            items[i] = tmp[i - mid];
        }
    }

    private static boolean isSorted(int[] items) {
        for(int i = 1; i < items.length; i++){
            if(items[i] < items[i-1]) return false;
        }
        return true;
    }
}
