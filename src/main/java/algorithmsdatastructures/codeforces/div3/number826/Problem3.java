package algorithmsdatastructures.codeforces.div3.number826;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                int length = Integer.parseInt(br.readLine());
                int[] items = new int[length];
                String[] splitted = br.readLine().split(" ");
                for (int i = 0; i < length; i++) {
                    items[i] = Integer.parseInt(splitted[i]);
                }

                Set<Integer> processed = new HashSet<>();
                Integer common = getNewCommon(processed, items);
                int maxLength = length;
                while (common != null) {
                    Integer checkedValue = check(common, items);
                    if (checkedValue != null) {
                        maxLength = Integer.min(checkedValue, maxLength);
                        processed.add(common);
                    } else {
                        processed.add(common);
                    }
                    common = getNewCommon(processed, items);
                }

                if (maxLength == 0) {
                    sb.append(length + "\n");
                } else {
                    sb.append(maxLength + "\n");
                }
            }

            System.out.println(sb);
        }
    }

    private static Integer getNewCommon(Set<Integer> processed, int[] items) {
        int leftSum = items[0];
        int rightSum = items[items.length - 1];
        int left = 1;
        int right = items.length - 2;
        while (left <= right) {
            if (leftSum == rightSum && !processed.contains(leftSum)) {
                return leftSum;
            }
            if (leftSum < rightSum) {
                leftSum += items[left];
                left++;
            } else if (leftSum > rightSum) {
                rightSum += items[right];
                right--;
            } else {
                leftSum += items[left];
                rightSum += items[right];
                left++;
                right--;
            }
        }
        if (leftSum == rightSum && !processed.contains(leftSum)) return leftSum;
        return null;
    }

    private static Integer check(Integer common, int[] items) {
        int index = 0;
        int size = 0;
        int sum = 0;
        int maxLength = 0;
        while (index < items.length) {
            if (sum == common) {
                maxLength = Integer.max(maxLength, size);
                sum = 0;
                size = 0;
            }
            sum += items[index];
            size++;
            index++;
        }
        if (sum == common) {
            maxLength = Integer.max(maxLength, size);
            return maxLength;
        }
        return null;
    }
}
