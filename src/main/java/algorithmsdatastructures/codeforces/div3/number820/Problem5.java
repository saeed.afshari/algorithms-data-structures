package algorithmsdatastructures.codeforces.div3.number820;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem5 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            long n = findIndexUpperBound(18L, br);
            long e = 1L;
            for (int i = 0; i < n; i++) {
                e *= 10;
            }
            long value = findIndexUpperBound2(e, br);
            System.out.printf("! %d", value);
            System.out.flush();

            br.readLine();
        }
    }

    private static long findIndexUpperBound(long end, BufferedReader br) throws IOException {
        long l = 1L;
        long r = end;
        while (l < r) {
            long e = 1;
            long mid = l + (r - l) / 2;
            for (int i = 0; i < mid; i++) {
                e *= 10;
            }

            System.out.println(String.format("? 1 %d", e));
            System.out.flush();

            long answer = Long.parseLong(br.readLine());

            if (answer > 0) {
                r = mid;
            } else {
                e = 1;
                for (int i = 0; i < mid; i++) {
                    e *= 10;
                }
                l = e + 1;
            }
        }
        return l - 1;
    }

    private static long findIndexUpperBound2(long end, BufferedReader br) throws IOException {
        long l = 1L;
        long r = end;
        while (l < r) {
            long e = 1;
            long mid = l + (r - l) / 2;

            System.out.println(String.format("? 1 %d", e));
            System.out.flush();

            long answer = Long.parseLong(br.readLine());

            if (answer > 0) {
                r = mid;
            } else {
                l = mid + 1;
            }
        }
        return l - 1;
    }
}
