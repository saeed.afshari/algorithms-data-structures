package algorithmsdatastructures.codeforces.div3.number820;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                var n = Integer.parseInt(br.readLine());
                var str = br.readLine();

                var sb1 = new StringBuilder();
                for (int i = str.length() - 1; i >= 0; i--) {
                    if (str.charAt(i) == '0') {
                        var d = 0;
                        i--;
                        d += Integer.parseInt(str.charAt(i) + "");
                        i--;
                        d += Integer.parseInt(str.charAt(i) + "") * 10;
                        sb1.insert(0, (char) ('a' + d - 1));
                    } else {
                        sb1.insert(0, (char) ('a' + Integer.parseInt(str.charAt(i) + "") - 1));
                    }
                }

                sb.append(sb1 + "\n");
            }

            System.out.println(sb);
        }
    }
}
