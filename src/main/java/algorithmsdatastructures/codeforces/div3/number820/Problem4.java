package algorithmsdatastructures.codeforces.div3.number820;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Problem4 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                var n = Integer.parseInt(br.readLine());
                var splitted = br.readLine().split(" ");
                var a = new int[n];
                for (int i = 0; i < n; i++) {
                    a[i] = Integer.parseInt(splitted[i]);
                }
                splitted = br.readLine().split(" ");
                var b = new int[n];
                for (int i = 0; i < n; i++) {
                    b[i] = Integer.parseInt(splitted[i]);
                }

                int[] c = new int[n];
                for (int i = 0; i < n; i++) {
                    c[i] = b[i] - a[i];
                }
                Arrays.sort(c);

                var numberOfDays = 0;
                int i = 0;
                int j = n - 1;
                while (i < j) {
                    if (c[j] + c[i] >= 0) {
                        numberOfDays++;
                        i++;
                        j--;
                        continue;
                    }
                    if (c[j] + c[i] < 0) {
                        i++;
                    }
                }

                sb.append(numberOfDays + "\n");
            }

            System.out.println(sb);
        }
    }
}
