package algorithmsdatastructures.codeforces.div3.number820;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem1 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;

                var splitted = br.readLine().split(" ");
                var a = Integer.parseInt(splitted[0]);
                var b = Integer.parseInt(splitted[1]);
                var c = Integer.parseInt(splitted[2]);

                var el1 = a - 1;

                int el2;
                if (b == 1) {
                    el2 = 2 * (c - 1);
                } else {
                    el2 = Math.abs(b - c) + (c - 1);
                }

                if (el1 < el2) {
                    sb.append("1\n");
                } else if (el2 < el1) {
                    sb.append("2\n");
                } else {
                    sb.append("3\n");
                }
            }

            System.out.println(sb);
        }
    }
}
