package algorithmsdatastructures.google.hashcode.mentorshipandteam.project;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.shared.SkillDto;

import java.util.List;

public record ProjectDto(String name, List<SkillDto> requiredSkills, Integer
        duration, Integer award, Integer dayBeforeTime, int evaluationRound) {
}
