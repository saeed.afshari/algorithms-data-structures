# Google Hash code 2022 Challenge

In this package I am going to solve Google hash code Mentorship and Teamwork challenge which is described in the
[Challenge.md](./Challenge.md).

## Summary

The goal of the challenge is completing the most projects and earning mord awards for the contributors. The domain
objects includes:

- Contributor
    - Each contributor can only work on one and only one project at the same time
    - Each contributor has a list of skills at a specific level and a name
    - Each contributor's skill level develops by 1 when he works on a project in which the required skill level which
      assigned to the contributor is greater than or equals with the contributor current skill level.
    - Each contributor has level 0 in all skills by default
    - Each contributor which has the required skill level for a project can mentor the team
- Skill
    - Each skill has a name
- Project
    - Each project has a name and a list of required skills at the specific level
    - Each project has an award
    - Each project has a duration
    - Each project has a "best before time" in days
    - Each project starts when all the required skills are filled by contributors

## Goal

The goal of the challenge is to earn rewards as much as possible

## Example input 1

3 3 Anna 1 C++ 2 Bob 2 HTML 5 CSS 5 Maria 1 Python 3 Logging 5 10 5 1 C++ 3 WebServer 7 10 7 2 HTML 3 C++ 2 WebChat 10
20 20 2 Python 3 HTML 3

### Input Interpretation

- 3 contributors
    - Anna has C++ skill at level 2
    - Bob has HTML skill at level 5 and CSS at level 5
    - Maria has Python skill at level 3
- 3 projects
    - Project Logging {duration: 5, award: 10, best before time: 5, requiredSkills: 1 [C++ at level 3]}
    - Project WebServer {duration: 7, award: 10, best before time: 7, requiredSkills:
      2 [HTML at level 3, C++ at level 2]}
    - Project WebChat {duration: 10, award: 20, best before time: 20, requiredSkills:
      2 [Python at level 3, HTML at level 3]}

### Example output 1

3 WebServer Bob Anna Logging Anna WebChat Maria Bob

### Output Interpretation

- 3 projects are planned
- WebServer: Bob, Anna (0-6 -> Award: 10)
- Logging: Anna (7-11 -> Award: 10 - (12 - 5) = 3)
- WebChat: Maria, Bob (7-16 -> Award: 20)

Total award: 33

## Step 1: System interfaces and data models

```java
record SkillDto(String name, Integer level) {
}

record ContributorDto(String name, Map<SkillDto, Integer> skillsMap) {
}

record ProjectDto(String name, Map<SkillDto, Integer> requiredSkillsMap, int retried) {
}

record PlanningDataSource(List<ContributorDto> contributors, List<ProjectDto> projects) {
}

record PlanDto(ProjectDto project, List<ContributorDto> contributors) {
}

interface InputFileParser {
    PlanningDataSource parse(String inputPath);
}

interface ContributorPool {
  bool hasContributors();
  Set<ContributorDto> getContributors(List<SkillDto> requiredSkills);
  void add(Set<ContributorDto> contributors);
}

interface Planner {
    Set<ProjectDto> plan(PlanningDataSource dataSource);
}

interface OutputFileWriter {
    String generate(Set<ProjectContributors> projectContributors, String filePath);
}
```

## Step 2: Algorithms and data structures

Insert all projects into a max heap with in the following order:
- Doable + Highest award + minimum duration + minimum day before time + lowest contribution level

Project contributor selection
- Create a map of skill to contributors
- Order the map with level ascending

Busy contributors

PROGRESS_DAYS

Algorithms:
- create a queue of projects
- Create a map of skills to contributors queue
- pop a project and try to assign all contributors to the project in the ongoingProjectsQueue
- if a project is not doable enqueue it and increase the attempts. 
- if there is no further project for the same round is available or there is no contributors available
- accomplish the projects and increase the contributors skills and enqueue them again into the skill to contributors queue
- if at the end of a round there were no doable project then write the outputs into the file
- else increase the round and try again

### ContributorPoolService
Data structures
- holds a map of skills to contributor set
- holds the number of all contributors

#### bool hasContributors()
returns availableContributors > 0

#### void add(List<ContributorDto> contributors);
- availableContributors + contributors.size()
- for each of skills add the contributor to the min heap which the  

#### Set<ContributorDto> getContributors(List<SkillDto> requiredSkills);
- availableContributors-= number of contributors
- for each skill remove the contributor from the rest of the skills  


## Step 3: implementation
I will now start implementing the interfaces one by one.

### Implementing ContributorPool interface and required models
### Implementing Planner interface and required models

