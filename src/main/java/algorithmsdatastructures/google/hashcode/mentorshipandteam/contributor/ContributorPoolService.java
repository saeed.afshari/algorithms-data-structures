package algorithmsdatastructures.google.hashcode.mentorshipandteam.contributor;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.shared.SkillDto;

import java.util.*;
import java.util.stream.Collectors;

public class ContributorPoolService implements ContributorPool {

    private final Set<ContributorDto> contributors = new HashSet<>();

    @Override
    public boolean hasContributors() {
        return contributors.size() > 0;
    }

    @Override
    public List<ContributorDto> getContributors(List<SkillDto> requiredSkills) {
        var result = new ContributorDto[requiredSkills.size()];
        var skillsWhichNoContributorsFound = new SkillDto[requiredSkills.size()];
        int i = 0;
        for (var requiredSkillDto : requiredSkills) {
            Optional<ContributorDto> contributorDtoOptional =
                    contributors.stream().filter(it -> it.skillsMap().entrySet().stream().anyMatch(s -> s.getKey().equals(requiredSkillDto.name()) && s.getValue() >= requiredSkillDto.level())).findFirst();
            if (contributorDtoOptional.isEmpty()) {
                skillsWhichNoContributorsFound[i] = requiredSkillDto;
                i++;
                continue;
            }

            ContributorDto contributorDto = contributorDtoOptional.get();
            result[i] = new ContributorDto(contributorDto.name(), contributorDto.skillsMap(), requiredSkillDto);
            contributors.remove(contributorDto);
            i++;
        }

        i = 0;
        for (var requiredSkillDto : skillsWhichNoContributorsFound) {
            if (requiredSkillDto == null) {
                i++;
                continue;
            }
            Optional<ContributorDto> contributorDtoOptional =
                    contributors.stream().filter(it -> {
                                if (requiredSkillDto.level() <= 1) {
                                    return checkMentorInTheTeam(Arrays.asList(result).stream().filter(Objects::nonNull).toList(), requiredSkillDto);
                                } else if (it.skillsMap().containsKey(new SkillDto(requiredSkillDto.name(), requiredSkillDto.level() - 1))) {
                                    return checkMentorInTheTeam(Arrays.asList(result).stream().filter(Objects::nonNull).toList(), requiredSkillDto);
                                }
                                return false;
                            })
                            .findFirst();
            if (contributorDtoOptional.isEmpty()) {
                contributors.addAll(Arrays.asList(result).stream().filter(Objects::nonNull).toList());
                return Collections.emptyList();
            }

            ContributorDto contributorDto = contributorDtoOptional.get();
            result[i] = new ContributorDto(contributorDto.name(), contributorDto.skillsMap(), requiredSkillDto);
            contributors.remove(contributorDto);
        }
        List<ContributorDto> contributorDtoList = Arrays.asList(result);
        return contributorDtoList;
    }

    private boolean checkMentorInTheTeam(List<ContributorDto> contributorDtoList, SkillDto requiredSkillDto) {
        return contributorDtoList.stream().anyMatch(it -> it.skillsMap().entrySet().stream().anyMatch(skillDto -> skillDto.getKey().equals(requiredSkillDto.name()) && skillDto.getValue() >= requiredSkillDto.level()));
    }

    @Override
    public void add(List<ContributorDto> contributors) {
        this.contributors.addAll(contributors);
    }
}
