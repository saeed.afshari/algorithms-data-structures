package algorithmsdatastructures.google.hashcode.mentorshipandteam.contributor;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.shared.SkillDto;

import java.util.List;

public interface ContributorPool {
    boolean hasContributors();

    List<ContributorDto> getContributors(List<SkillDto> requiredSkills);

    void add(List<ContributorDto> contributors);
}
