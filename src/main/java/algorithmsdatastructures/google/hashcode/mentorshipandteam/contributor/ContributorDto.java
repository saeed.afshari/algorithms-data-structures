package algorithmsdatastructures.google.hashcode.mentorshipandteam.contributor;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.shared.SkillDto;

import java.util.Map;

public record ContributorDto(String name, Map<String, Integer> skillsMap, SkillDto requiredSkillDto) {
}
