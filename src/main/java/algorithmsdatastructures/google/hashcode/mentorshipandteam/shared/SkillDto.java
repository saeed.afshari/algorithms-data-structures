package algorithmsdatastructures.google.hashcode.mentorshipandteam.shared;

public record SkillDto(String name, Integer level) {
}
