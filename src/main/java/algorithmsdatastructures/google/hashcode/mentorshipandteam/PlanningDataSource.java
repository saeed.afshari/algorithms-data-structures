package algorithmsdatastructures.google.hashcode.mentorshipandteam;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.contributor.ContributorDto;
import algorithmsdatastructures.google.hashcode.mentorshipandteam.project.ProjectDto;

import java.util.List;

public record PlanningDataSource(
        List<ContributorDto> contributors, List<ProjectDto> projects
) {
}
