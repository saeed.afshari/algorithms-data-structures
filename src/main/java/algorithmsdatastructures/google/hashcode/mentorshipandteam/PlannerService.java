package algorithmsdatastructures.google.hashcode.mentorshipandteam;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.contributor.ContributorPool;
import algorithmsdatastructures.google.hashcode.mentorshipandteam.project.ProjectDto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class PlannerService implements Planner {

    private final ContributorPool contributorPoolService;

    PlannerService(ContributorPool contributorPoolService) {
        this.contributorPoolService = contributorPoolService;
    }

    @Override
    public List<ProjectContributors> plan(PlanningDataSource dataSource) {
        contributorPoolService.add(dataSource.contributors());

        var projectDtoQueue = new LinkedList<ProjectDto>();
        dataSource.projects().sort(Comparator.comparing(s -> s.dayBeforeTime(), (p1, p2) -> p1.compareTo(p2)));
        projectDtoQueue.addAll(dataSource.projects());
        List<ProjectContributors> result = new ArrayList<>();
        List<ProjectContributors> roundResult = new ArrayList<>();
        int evaluationRound = 0;
        while (projectDtoQueue.peek() != null) {
            var projectDto = projectDtoQueue.pop();
            if (evaluationRound < projectDto.evaluationRound()) {
                if (roundResult.isEmpty())
                    break;
                for (var r : roundResult) {
                    contributorPoolService.add(r.getAndIncreaseSkillLevelOfContributors());
                }
                result.addAll(roundResult);
                roundResult.clear();
                evaluationRound++;
            }
            var contributors = contributorPoolService.getContributors(projectDto.requiredSkills());
            if (contributors.isEmpty()) {
                projectDtoQueue.add(new ProjectDto(
                        projectDto.name(),
                        projectDto.requiredSkills(),
                        projectDto.duration(),
                        projectDto.award(),
                        projectDto.dayBeforeTime(),
                        projectDto.evaluationRound() + 1
                ));
                continue;
            }
            roundResult.add(new ProjectContributors(projectDto, contributors.stream().toList()));
        }
        if (!roundResult.isEmpty())
            result.addAll(roundResult);
        return result;
    }
}
