package algorithmsdatastructures.google.hashcode.mentorshipandteam.input;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.PlanningDataSource;

import java.io.IOException;

interface InputFileParser {
    PlanningDataSource parse(String inputPath) throws IOException;
}
