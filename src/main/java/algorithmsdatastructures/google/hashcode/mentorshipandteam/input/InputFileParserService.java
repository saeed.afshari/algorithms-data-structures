package algorithmsdatastructures.google.hashcode.mentorshipandteam.input;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.PlanningDataSource;
import algorithmsdatastructures.google.hashcode.mentorshipandteam.contributor.ContributorDto;
import algorithmsdatastructures.google.hashcode.mentorshipandteam.project.ProjectDto;
import algorithmsdatastructures.google.hashcode.mentorshipandteam.shared.SkillDto;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InputFileParserService implements InputFileParser {

    @Override
    public PlanningDataSource parse(String inputPath) throws IOException {
        var contributors = new ArrayList<ContributorDto>();
        var projects = new ArrayList<ProjectDto>();
        List<String> lines = Files.readAllLines(Path.of(inputPath));
        String[] firstLine = lines.get(0).split(" ");
        int numberOfContributors = Integer.parseInt(firstLine[0]);
        int numberOfProjects = Integer.parseInt(firstLine[1]);
        int lineNumber = 1;
        for (int i = 0; i < numberOfContributors; i++) {
            String[] contributorSkill = lines.get(lineNumber).split(" ");
            String name = contributorSkill[0];
            int numberOfSkills = Integer.parseInt(contributorSkill[1]);
            var skillsMap = new HashMap<String, Integer>();
            for (int j = 0; j < numberOfSkills; j++) {
                lineNumber++;
                String[] skillLevel = lines.get(lineNumber).split(" ");
                String skillName = skillLevel[0];
                Integer level = Integer.parseInt(skillLevel[1]);
                skillsMap.put(skillName, level);
            }
            contributors.add(new ContributorDto(name, skillsMap, null));
            lineNumber++;
        }
        for (int i = 0; i < numberOfProjects; i++) {
            String[] projectDetails = lines.get(lineNumber).split(" ");
            String name = projectDetails[0];
            Integer duration = Integer.parseInt(projectDetails[1]);
            Integer award = Integer.parseInt(projectDetails[2]);
            Integer lastBeforeTime = Integer.parseInt(projectDetails[3]);
            Integer numberOfSkills = Integer.parseInt(projectDetails[4]);
            var requiredSkills = new ArrayList<SkillDto>();
            for (int j = 0; j < numberOfSkills; j++) {
                lineNumber++;
                String[] skillLevel = lines.get(lineNumber).split(" ");
                String skillName = skillLevel[0];
                Integer level = Integer.parseInt(skillLevel[1]);
                requiredSkills.add(new SkillDto(skillName, level));
            }
            projects.add(new ProjectDto(name, requiredSkills, duration, award, lastBeforeTime, 0));
            lineNumber++;
        }
        return new PlanningDataSource(contributors, projects);
    }
}
