package algorithmsdatastructures.google.hashcode.mentorshipandteam.output;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.ProjectContributors;
import algorithmsdatastructures.google.hashcode.mentorshipandteam.contributor.ContributorDto;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class OutputFileWriterService implements OutputFileWriter {

    @Override
    public String generate(List<ProjectContributors> projectContributors, String filePath) throws IOException {
        List<ProjectContributors> projectContributorsList = projectContributors.stream().toList();
        var sb = new StringBuilder();
        sb.append(projectContributorsList.size() + "\n");
        for (int i = 0; i < projectContributorsList.size(); i++) {
            ProjectContributors pc = projectContributorsList.get(i);
            sb.append(pc.projectDto().name() + "\n");
            String collect = pc.contributorDtoList().stream().map(ContributorDto::name).collect(Collectors.joining(" "));
            sb.append(collect + "\n");
        }

        String result = sb.toString();

        Files.writeString(Path.of(filePath), result);

        return result;
    }
}
