package algorithmsdatastructures.google.hashcode.mentorshipandteam.output;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.ProjectContributors;

import java.io.IOException;
import java.util.List;

public interface OutputFileWriter {
    String generate(List<ProjectContributors> projectContributors, String filePath) throws IOException;
}
