package algorithmsdatastructures.google.hashcode.mentorshipandteam.youtube;

public class RemoveNotConnectedIslands {

    /*
    1 1 1 1
    1 0 0 0
    1 0 1 0
    0 0 0 0
     */

    /*
    1 0 0 0 0 0
    0 1 0 1 1 1
    0 0 1 0 1 0
    1 1 0 0 1 0
    1 0 1 1 0 0
    1 0 0 0 0 1
     */

    public static void main(String[] args) {
        int[][] matrix = {{1, 1, 1, 1}, {1, 0, 0, 0}, {1, 0, 1, 0}, {0, 0, 0, 0}};
        var result = removeIslands(matrix);
        System.out.println(result);
        System.out.println("##############");

        var matrix2 = new int[][]{{1, 0, 0, 0, 0, 0}, {0, 1, 0, 1, 1, 1}, {0, 0, 1, 0, 1, 0}, {1, 1, 0, 0, 1, 0}, {1, 0, 1, 1, 0, 0}, {1, 0, 0, 0, 0, 1}};
        result = removeIslands(matrix2);
        System.out.println(result);
    }

    public static String removeIslands(int[][] matrix) {
        int maxCols = matrix[0].length;
        int maxRows = matrix.length;
        var visited = new boolean[maxRows][maxCols];
        for (int row = 0; row < maxRows; row++) {
            dfs(matrix, visited, row, 0);
            dfs(matrix, visited, row, maxCols - 1);
        }
        for (int col = 0; col < maxCols; col++) {
            dfs(matrix, visited, 0, col);
            dfs(matrix, visited, maxRows - 1, col);
        }
        var sb = new StringBuilder();
        for (int row = 1; row < maxRows - 1; row++) {
            for (int col = 1; col < maxCols - 1; col++) {
                if (matrix[row][col] == 1 && !visited[row][col]) {
                    sb.append(String.format("item [%d][%d] is removed.\n", row, col));
                    matrix[row][col] = 0;
                }
            }
        }
        return sb.toString();
    }

    private static void dfs(int[][] matrix, boolean[][] visited, int row, int col) {
        if (matrix[row][col] == 0) return;
        if (visited[row][col]) return;
        var maxColumns = matrix[0].length;
        var maxRows = matrix.length;
        visited[row][col] = true;
        if (row > 0 && matrix[row - 1][col] == 1) {
            dfs(matrix, visited, row - 1, col);
        }
        if (row < maxRows - 1 && matrix[row + 1][col] == 1) {
            dfs(matrix, visited, row + 1, col);
        }
        if (col > 0 && matrix[row][col - 1] == 1) {
            dfs(matrix, visited, row, col - 1);
        }
        if (col < maxColumns - 1 && matrix[row][col + 1] == 1) {
            dfs(matrix, visited, row, col + 1);
        }
    }
}
