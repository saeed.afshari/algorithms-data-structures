package algorithmsdatastructures.google.hashcode.mentorshipandteam;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.contributor.ContributorDto;
import algorithmsdatastructures.google.hashcode.mentorshipandteam.project.ProjectDto;

import java.util.ArrayList;
import java.util.List;

public record ProjectContributors(ProjectDto projectDto, List<ContributorDto> contributorDtoList) {

    public List<ContributorDto> getAndIncreaseSkillLevelOfContributors() {
        var result = new ArrayList<ContributorDto>();
        for (var contributorDto : contributorDtoList) {

            var skillDto = contributorDto.skillsMap().keySet().stream().filter(it -> it.equals(contributorDto.requiredSkillDto().name())).findFirst();
            if (skillDto.isPresent()) {
                Integer level = contributorDto.skillsMap().get(skillDto.get());
                if (level <= contributorDto.requiredSkillDto().level()) {
                    contributorDto.skillsMap().put(skillDto.get(), level + 1);
                    result.add(new ContributorDto(contributorDto.name(), contributorDto.skillsMap(), null));
                } else {
                    result.add(new ContributorDto(contributorDto.name(), contributorDto.skillsMap(), null));
                }
            }

        }

        return result;
    }
}
