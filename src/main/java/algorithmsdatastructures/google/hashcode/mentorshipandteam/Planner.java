package algorithmsdatastructures.google.hashcode.mentorshipandteam;

import java.util.List;

public interface Planner {
    List<ProjectContributors> plan(PlanningDataSource dataSource);
}
