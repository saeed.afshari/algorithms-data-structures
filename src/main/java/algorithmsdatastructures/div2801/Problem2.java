package algorithmsdatastructures.div2801;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var n = Integer.parseInt(br.readLine());
                var piles = Arrays.stream(br.readLine().split(" ")).map(it -> Integer.parseInt(it) % 2 == 0 ? Integer.parseInt(it) % 10 : 1).collect(Collectors.toList());
                var numberOfRepeats = piles.stream().mapToInt(it -> it).min().getAsInt();
                var minIndex = piles.indexOf(numberOfRepeats);

                var found = false;
                if (n == 1) {
                    sb.append("Mike\n");
                } else {
                    if (n % 2 == 0) {
                        for (int i = 0; i < n - 1; i += 2) {
                            if (piles.get(i) < piles.get(i + 1)) {
                                sb.append("Mike\n");
                                found = true;
                                break;
                            }
                        }
                        if(!found) sb.append("Joe\n");
                    } else {
                        if (numberOfRepeats % 2 != 0) {
                            for (int i = 0; i < n - 1; i += 2) {
                                if (piles.get(i) < piles.get(i + 1)) {
                                    sb.append("Joe\n");
                                    found = true;
                                    break;
                                }
                            }
                            if(!found)
                                sb.append("Mike\n");
                        } else {
                            for (int i = 0; i < n - 1; i += 2) {
                                if (piles.get(i) < piles.get(i + 1)) {
                                    sb.append("Mike\n");
                                    found = true;
                                    break;
                                }
                            }
                            if(!found)
                                sb.append("Joe\n");
                        }
                    }
                }
            }

            System.out.println(sb);
        }
    }

}
