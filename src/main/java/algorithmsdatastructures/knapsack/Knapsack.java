package algorithmsdatastructures.knapsack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Knapsack {

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(reader.readLine());
            for (int i = 0; i < numberOfTests; i++) {
                var capacity = Integer.parseInt(reader.readLine());
                var profits = Arrays.stream(reader.readLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());
                var weights = Arrays.stream(reader.readLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());

                var integers = solveKnapsack(capacity, profits, weights);
                System.out.println(String.join(",", integers.stream().map(it -> Integer.toString(it)).collect(Collectors.toList())));
            }
        }
    }

    private static List<Integer> solveKnapsack(int capacity, List<Integer> profits, List<Integer> weights) {
        var result = new ArrayList<Integer>();
        int max = -1;
        for (int i = 0; i < profits.size(); i++) {
            for (int j = i; j < profits.size(); j++) {
                var a = new ArrayList<Integer>();
                int sum = 0;
                int profit = 0;
                for (int k = i; k < j + 1; k++) {
                    sum += weights.get(k);
                    profit += profits.get(k);
                    a.add(k);
                }
                if (sum <= capacity) {
                    if (max < profit) {
                        result = a;
                        max = profit;
                    }
                }
            }
        }
        return result;
    }
}
