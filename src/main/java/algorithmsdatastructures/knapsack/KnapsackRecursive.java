package algorithmsdatastructures.knapsack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class KnapsackRecursive {

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(reader.readLine());
            for (int i = 0; i < numberOfTests; i++) {
                var capacity = Integer.parseInt(reader.readLine());
                var profits = Arrays.stream(reader.readLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());
                var weights = Arrays.stream(reader.readLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());

                var max = solveKnapsack(capacity, profits, weights);
                System.out.println(max);
            }
        }
    }

    private static Integer solveKnapsack(int capacity, List<Integer> profits, List<Integer> weights) {
        return solveKnapsackRecursive(capacity, profits, weights, 0);
    }

    private static int solveKnapsackRecursive(int capacity, List<Integer> profits, List<Integer> weights, int currentIndex) {
        if (currentIndex >= profits.size() || capacity <= 0) {
            return 0;
        }

        int profit0 = 0;
        if (capacity >= weights.get(currentIndex)) {
            profit0 = profits.get(currentIndex) + solveKnapsackRecursive(capacity - weights.get(currentIndex), profits, weights, currentIndex + 1);
        }
        var profit1 = solveKnapsackRecursive(capacity, profits, weights, currentIndex + 1);
        return Math.max(profit0, profit1);
    }

//    Var dp = new Integer[k][operations.length];
//for(int i = 0; i< k; i++){
//        for(int j = 0; j< operations.length; j++){
//            dp[i][j] = -1;
//        }
//    }
//    int calculate(operationsLeft: int, int index){
//        if(index >= operations.length) return 0;
//        if(dp[operationsLeft][index] != -1) return dp[operationsLeft][index];
//        if(operations[index] <= operationsLeft){
//            var take = c[index] + calculate(operationLeft - operations[index], index+1);
//        }
//        var skip = calculate(operationLeft, index+1);
//        dp[operationsLeft][index] = Math.max(take, skip);
//        return dp[operationsLeft][index];
//    }

}
