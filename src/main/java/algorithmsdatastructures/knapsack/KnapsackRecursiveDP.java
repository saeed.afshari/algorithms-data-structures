package algorithmsdatastructures.knapsack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class KnapsackRecursiveDP {

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(reader.readLine());
            for (int i = 0; i < numberOfTests; i++) {
                var capacity = Integer.parseInt(reader.readLine());
                var profits = Arrays.stream(reader.readLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());
                var weights = Arrays.stream(reader.readLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());

                var max = solveKnapsack(capacity, profits, weights);
                System.out.println(max);
            }
        }
    }

    private static Integer solveKnapsack(int capacity, List<Integer> profits, List<Integer> weights) {
        var dp = new int[profits.size()][capacity + 1];
        return solveKnapsackRecursive(capacity, profits, weights, dp, 0);
    }

    private static int solveKnapsackRecursive(int capacity, List<Integer> profits, List<Integer> weights, int[][] dp, int currentIndex) {
        if (currentIndex >= profits.size() || capacity <= 0) {
            return 0;
        }

        if (dp[currentIndex][capacity] != 0) {
            return dp[currentIndex][capacity];
        }

        int profit0 = 0;
        if (capacity >= weights.get(currentIndex)) {
            profit0 = profits.get(currentIndex) + solveKnapsackRecursive(capacity - weights.get(currentIndex), profits, weights, dp, currentIndex + 1);
        }
        var profit1 = solveKnapsackRecursive(capacity, profits, weights, dp, currentIndex + 1);
        int max = Math.max(profit0, profit1);
        dp[currentIndex][capacity] = max;
        return max;
    }
}
