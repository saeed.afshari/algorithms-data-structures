package algorithmsdatastructures.knapsack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class TwoEqualSubsets {

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(reader.readLine());
            for (int i = 0; i < numberOfTests; i++) {
                var aList = Arrays.stream(reader.readLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());

                var subsets = solve(aList);
                if (subsets.size() < 2) {
                    System.out.println("Impossible");
                } else {
                    System.out.println(String.join(",", subsets.get(0).stream().map(it -> Integer.toString(it)).collect(Collectors.toList())));
                    System.out.println(String.join(",", subsets.get(1).stream().map(it -> Integer.toString(it)).collect(Collectors.toList())));
                }
            }
        }
    }

    private static List<List<Integer>> solve(List<Integer> aList) {
        var result = new ArrayList<List<Integer>>();
        var possibleMatchesMap = new HashMap<Integer, List<Integer>>();
        for (int i = 0; i < aList.size(); i++) {
            for (int j = i; j < aList.size(); j++) {
                var a = new ArrayList<Integer>();
                int sum = 0;
                for (int k = i; k < j + 1; k++) {
                    sum += aList.get(k);
                    a.add(aList.get(k));
                }
                if (possibleMatchesMap.containsKey(sum)) {
                    if (possibleMatchesMap.get(sum).size() + a.size() == aList.size()) {
                        result.add(a);
                        result.add(possibleMatchesMap.get(sum));
                        return result;
                    }
                }
                possibleMatchesMap.put(sum, a);
            }
        }
        return result;
    }
}
