package algorithmsdatastructures.div2131;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem1 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var splitted1 = br.readLine().split(" ");
                var splitted2 = br.readLine().split(" ");
                var item1 = Integer.parseInt(splitted1[0]);
                var item2 = Integer.parseInt(splitted1[1]);
                var item4 = Integer.parseInt(splitted2[0]);
                var item3 = Integer.parseInt(splitted2[1]);

                var sum = item1 + item2 + item3 + item4;

                var result1 = item1 + item2 + item3;
                var result12 = item4;
                var count1 = result1 > 0 ? 1 : 0;
                count1 += result12 > 0 ? 1 : 0;

                var result2 = item2 + item3 + item4;
                var result21 = item1;
                var count2 = result2 > 0 ? 1 : 0;
                count2 += result21 > 0 ? 1 : 0;

                var result3 = item3 + item4 + item1;
                var result31 = item2;
                var count3 = result3 > 0 ? 1 : 0;
                count3 += result31 > 0 ? 1 : 0;

                var result4 = item2 + item4 + item1;
                var result41 = item3;
                var count4 = result4 > 0 ? 1 : 0;
                count4 += result41 > 0 ? 1 : 0;

                var min = Integer.min(count1, count2);
                min = Integer.min(min, count3);
                min = Integer.min(min, count4);


                sb.append(min + "\n");
            }

            System.out.println(sb);
        }
    }
}
