package algorithmsdatastructures.div2131;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var n = Integer.parseInt(br.readLine());
                var max = 0;
                List<Long> finalResult = null;
                for (int i = 1; i <= n; i++) {
                    var factors = getFactors(i);
                    var result = getMaxD(factors);
                    if (max <= result.size()) {
                        max = result.size();
                        finalResult = result;
                    }
                }
                if(finalResult != null){
                    var d = finalResult.get(finalResult.size() - 1);
                    finalResult.remove(d);
                    var finalResultSet = finalResult.stream().collect(Collectors.toSet());
                    sb.append(d + "\n");
                    for (int i = 0; i < finalResult.size(); i++){
                        sb.append(finalResult.get(i) + " " + finalResult.get(i) * d + " ");
                        finalResultSet.add(finalResult.get(i) * d);
                    }
                    for (long i = 1; i <= n; i++){
                        if(!finalResultSet.contains(i)){
                            sb.append(i + " ");
                        }
                    }
                    sb.append("\n");
                }
            }

            System.out.println(sb);
        }
    }

    private static List<Long> getMaxD(List<Long> factors) {
        var f = factors.stream().sorted().collect(Collectors.toList());
        var factorSet = factors.stream().collect(Collectors.toSet());
        long max = 0;
        long d = 1;
        var result = new ArrayList<Long>();
        for (int i = 1; i < f.size(); i++) {
            int numbers = 0;
            var items = new ArrayList<Long>();
            for (int j = 0; j < f.size(); j++) {
                if(i == j) continue;
                if (factorSet.contains(f.get(i) * f.get(j))) {
                    numbers++;
                    items.add(f.get(j));
                }
            }
            if (max <= numbers) {
                max = numbers;
                d = f.get(i);
                result = items;
            }
        }
        result.add(d);
        return result;
    }

    private static List<Long> getFactors(long number) {
        var result = new HashSet<Long>();
        for (long i = 1; i <= number; i++) {
            if (number % i == 0) {
                result.add(i);
                if (number / i != i)
                    result.add(number / i);
            }
            if (number / i <= i) break;
        }
        return result.stream().collect(Collectors.toList());
    }
}
