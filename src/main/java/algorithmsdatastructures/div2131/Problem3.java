package algorithmsdatastructures.div2131;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var splitted = br.readLine().split(" ");
                var n = Integer.parseInt(splitted[0]);
                var k = Integer.parseInt(splitted[1]);
                var a = new int[n];
                splitted = br.readLine().split(" ");
                for (int i = 0; i < n; i++) {
                    a[i] = Integer.parseInt(splitted[i]);
                }
            }

            System.out.println(sb);
        }
    }
}
