package algorithmsdatastructures.codeforces787;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Problem4 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            for (int i = 0; i < numberOfTests; i++) {
                br.readLine();
                var parents = Arrays.stream(br.readLine().split(" ")).map(it -> Integer.valueOf(it)).collect(Collectors.toList());
                var result = solve(parents);
                println(result.size());
                result.forEach(it -> {
                    println(it.size());
                    println(String.join(" ", it.stream().map(x -> x + "").collect(Collectors.toList())));
                });
                println();
            }
        }
    }

    private static List<List<Integer>> solve(List<Integer> parents) {
        var adjMap = new ArrayList<List<Integer>>(parents.size() + 1);
        for (int i = 0; i < parents.size() + 1; i++){
            adjMap.add(new ArrayList<>());
        }
        var root = buildAdjacencyMapAndTreeRoot(parents, adjMap);
        var stack = new LinkedList<Integer>();
        var result = new ArrayList<List<Integer>>();
        stack.push(root);
        var path = new ArrayList<Integer>();
        while (!stack.isEmpty()) {
            var node = stack.pop();
            path.add(node);
            if (adjMap.get(node).size() == 0) {
                result.add(path);
                path = new ArrayList<>();
                continue;
            }
            for (var child : adjMap.get(node)) {
                stack.push(child);
            }
        }
        return result;
    }

    private static int buildAdjacencyMapAndTreeRoot(List<Integer> parents, List<List<Integer>> adjacencyMap) {
        var root = 0;
        for (int i = 1; i <= parents.size(); i++) {
            var parent = parents.get(i - 1);
            if (parent == i) {
                root = parent;
                continue;
            }
            adjacencyMap.get(parent).add(i);
        }
        return root;
    }

    private static void println(Object item) {
        System.out.println(item);
    }

    private static void println() {
        System.out.println();
    }

}
