package algorithmsdatastructures.codeforces787

fun main() {
    val numberOfTests = readLine()!!.toInt()
    repeat(numberOfTests) {
        readLine()
        val a = readLine()!!.split(" ").map { it.toInt() }
        println(solve(a.toMutableList()))
    }
}

fun solve(a: MutableList<Int>): Int {
    if (a.size <= 1) return 0

    var minimum = 0

    var i = 0
    var prev = -1

    // 5 3 2 1
    // 2 3 2 1
    // 2 1 2 1
    while (i >= 0 && i < a.size - 1) {
        if (a[i] < a[i + 1]) {
            prev = a[i]
            i++
            continue
        }
        if (a[i] == 0 && i > 0) return -1
        if (a[i + 1] == 0) return -1
        val (times, v) = getTimes(a[i], a[i + 1])
        minimum += times
        a[i] = v
        if (a[i] <= prev) {
            prev = if (i >= 2) {
                a[i - 2]
            } else {
                -1
            }
            i--
        } else {
            prev = a[i]
            i++
        }
    }
    if (i < a.size - 1) {
        return -1
    }
    return minimum
}

fun getTimes(a: Int, b: Int): Pair<Int, Int> {
    var x = a
    var times = 0
    while (x >= b) {
        times++
        x /= 2
    }
    return times to x
}
