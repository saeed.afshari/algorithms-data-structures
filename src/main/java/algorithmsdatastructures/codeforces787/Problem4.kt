package algorithmsdatastructures.codeforces787

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.LinkedList

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            br.readLine()
            val parents = br.readLine().split(" ").map { it.toInt() }
            val result = solve(parents)
            sb.appendLine("${result.size}")
            result.forEach {
                sb.appendLine("${it.size}")
                sb.appendLine("${it.joinToString(" ") { path -> path.toString() }}")
            }
            sb.appendLine()
        }
        print(sb)
    }
}

fun solve(parents: List<Int>): MutableList<MutableList<Int>> {
    val (adjMap, root) = buildAdjacencyMapAndTreeRoot(parents)
    val stack = LinkedList<Int>()
    val result = mutableListOf<MutableList<Int>>()
    stack.push(root)
    var path = mutableListOf<Int>()
    while (!stack.isEmpty()) {
        val node = stack.pop()
        path.add(node)
        if (adjMap[node] == null) {
            result.add(path)
            path = mutableListOf()
            continue
        }
        for (child in adjMap.getValue(node)) {
            stack.push(child)
        }
    }
    return result
}

fun buildAdjacencyMapAndTreeRoot(parents: List<Int>): Pair<Map<Int, List<Int>>, Int> {
    val adjacencyMap = mutableMapOf<Int, MutableList<Int>>()
    var root = 0
    for ((i, parent) in parents.withIndex()) {
        if (parent == i + 1) {
            root = parent
            continue
        }
        if (!adjacencyMap.containsKey(parent)) {
            adjacencyMap[parent] = mutableListOf()
        }
        adjacencyMap.getValue(parent).add(i + 1)
    }
    return adjacencyMap to root
}

