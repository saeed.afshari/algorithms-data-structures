package algorithmsdatastructures.codeforces787

fun main() {
    val numberOfTests = readLine()!!.toInt()
    repeat(numberOfTests) {
        println(solve(readLine()!!))
    }
}

fun solve(answeres: String): Int {
    if (answeres.length == 1) return 1
    return if (answeres.contains("1")) {
        val lastIndex1 = answeres.lastIndexOf('1')
        val lastIndex0 = answeres.lastIndexOf('0')
        if (lastIndex0 > lastIndex1) {
            var numberOfThieves = 0
            for (i in lastIndex1 until answeres.length) {
                numberOfThieves++
                if (answeres[i] == '0') break
            }
            numberOfThieves
        } else {
            answeres.substring(lastIndex1 until answeres.length).length
        }
    } else if (answeres.contains("0")) {
        if (answeres.filter { it == '0' }.length > 1) {
            val first0Index = answeres.indexOf('0')
            var numberOfThieves = 0
            for (i in first0Index downTo 0) {
                numberOfThieves++
            }
            numberOfThieves
        } else {
            answeres.substring(0..answeres.lastIndexOf('0')).length
        }
    } else {
        answeres.length
    }
}
