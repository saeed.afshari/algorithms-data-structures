package algorithmsdatastructures.codeforces787

fun main() {
    val numberOfTests = readLine()!!.toInt()
    repeat(numberOfTests) {
        val (a, b, c, x, y) = readLine()!!.split(" ").map { it.toInt() }
        println(solve(a, b, c, x, y))
    }
}

fun solve(a: Int, b: Int, c: Int, x: Int, y: Int): String {
    val dogsResult = x - a
    val catsResult = y - b

    if (dogsResult <= 0 && catsResult <= 0) return "YES"

    var universal = c
    if (dogsResult > 0) {
        universal -= dogsResult
    }
    if (catsResult > 0) {
        universal -= catsResult
    }

    return if (universal < 0) {
        "NO"
    } else {
        "YES"
    }
}
