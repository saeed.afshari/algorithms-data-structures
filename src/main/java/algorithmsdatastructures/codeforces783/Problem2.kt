package algorithmsdatastructures.codeforces783

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            val (_, m) = br.readLine().split(" ").map { it.toInt() }
            val requiredEmptyList = br.readLine().split(" ").map { it.toInt() }
            sb.appendLine(solve(m, requiredEmptyList.sorted()))
        }
        print(sb)
    }
}

fun solve(m: Int, requiredEmptyList: List<Int>): String {
    var requiredSeats = 0L
    for (element in requiredEmptyList) {
        requiredSeats += element + 1;
    }
    requiredSeats += requiredEmptyList[requiredEmptyList.size - 1] - requiredEmptyList[0];
    return if (requiredSeats > m) "NO" else "YES"
}
