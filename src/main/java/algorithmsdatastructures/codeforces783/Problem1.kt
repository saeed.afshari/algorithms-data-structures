package algorithmsdatastructures.codeforces783

import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.max
import kotlin.math.min

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            val (n, m) = br.readLine().split(" ").map { it.toInt() }
            val max = max(n, m)
            if (max >= 3 && minOf(n, m) == 1) sb.appendLine("-1")
            else if (n == 1 && m == 1) sb.appendLine("0")
            else {
                if ((min(n, m)+max) % 2 == 0)
                    sb.appendLine(max * 2 - 2)
                else
                    sb.appendLine(max * 2 - 3)
            }
        }
        print(sb)
    }
}
