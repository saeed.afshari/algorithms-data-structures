package algorithmsdatastructures.codeforces783;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;

public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            int n = Integer.parseInt(br.readLine());
            var a = Arrays.stream(br.readLine().split(" ")).map(Integer::parseInt).toList();
            var count = 1;
            // if sorted asc or desc -> n - 1
            //1 2 1 2 1 2 1
            // 1 1 3 2 5 3 7
            //-3 0 1 2 3 4 5
            // 1 8 2 7 3 6 4 5
            //2 2 2 2
            // -6 -2 0 2
            if (a.size() > 1) {

            }

            System.out.println(count + "\n");
        }
    }
}
