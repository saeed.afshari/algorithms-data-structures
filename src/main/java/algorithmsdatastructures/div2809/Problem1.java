package algorithmsdatastructures.div2809;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem1 {
    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var splitted = br.readLine().split(" ");
                Integer n = Integer.parseInt(splitted[0]);
                Integer m = Integer.parseInt(splitted[1]);

                splitted = br.readLine().split(" ");
                char[] result = new char[m + 1];
                int[] a = new int[n + 1];
                for (int i = 1; i <= n; i++) {
                    a[i] = Integer.parseInt(splitted[i-1]);
                }
                for (int i = 0; i <= m; i++) {
                    result[i] = 'B';
                }
                for (int i = 1; i <= n; i++) {
                    var aI = a[i];
                    var aMI = m + 1 - a[i];
                    if (aI <= aMI && aI <= m && result[aI] != 'A') {
                        result[aI] = 'A';
                        continue;
                    }
                    if (aMI <= m && result[aMI] != 'A') {
                        result[aMI] = 'A';
                        continue;
                    }
                    if (aI <= m && result[aI] != 'A') {
                        result[aI] = 'A';
                    }
                }

                for (int i = 1; i <= m; i++) {
                    sb.append(result[i]);
                }
                sb.append("\n");
            }
            System.out.println(sb);
        }

    }
}
