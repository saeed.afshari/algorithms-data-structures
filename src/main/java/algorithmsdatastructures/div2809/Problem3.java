package algorithmsdatastructures.div2809;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem3 {
    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var n = Integer.parseInt(br.readLine());
                var splitted = br.readLine().split(" ");
                int[] a = new int[n];
                for (int i = 0; i < n; i++) {
                    a[i] = Integer.parseInt(splitted[i]);
                }
                int result = 0;

                if (n % 2 == 0) {
                    var result1 = 0;
                    for (int i = 1; i < n - 1; i += 1) {
                        var maxSides = Integer.max(a[i - 1], a[i + 1]);
                        var state1 = 0;
                        if (a[i] <= maxSides) {
                            state1 = maxSides - a[i] + 1;
                        }

                        maxSides = Integer.max(a[i], a[i + 2]);
                        var state2 = 0;
                        if (a[i+1] <= maxSides) {
                            state2 = maxSides - a[i+1] + 1;
                        }

                        result1 += Integer.min(state1, state2);
                    }

                    var result2 = 0;
                    for (int i = 2; i < n - 1; i += 2) {
                        var maxSides = Integer.max(a[i - 1], a[i + 1]);
                        var state1 = 0;
                        if (a[i] <= maxSides) {
                            state1 = maxSides - a[i] + 1;
                        }

                        maxSides = Integer.max(a[i], a[i + 2]);
                        var state2 = 0;
                        if (a[i+1] <= maxSides) {
                            state2 = maxSides - a[i+1] + 1;
                        }

                        result += Integer.min(state1, state2);
                    }
                } else {
                    for (int i = 1; i < n - 1; i += 2) {
                        var maxSides = Integer.max(a[i - 1], a[i + 1]);
                        if (a[i] <= maxSides) {
                            result += maxSides - a[i] + 1;
                        }
                    }
                }

                sb.append(result + "\n");
            }
            System.out.println(sb);
        }

    }
}
