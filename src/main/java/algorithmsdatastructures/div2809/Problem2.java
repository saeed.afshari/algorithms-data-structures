package algorithmsdatastructures.div2809;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem2 {
    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var sb = new StringBuilder();
            var splitted = br.readLine().split(" ");
            Integer n = Integer.parseInt(splitted[0]);
            Integer m = Integer.parseInt(splitted[1]);

            splitted = br.readLine().split(" ");
            int[] a = new int[n + 1];
            for (int i = 1; i <= n; i++) {
                a[i] = Integer.parseInt(splitted[i - 1]);
            }
            int[] prefixSum = new int[n + 1];
            for (int i = 1; i <= n; i++) {
                if(a[i] < a[i - 1]) {
                    prefixSum[i] = prefixSum[i-1] + a[i - 1] - a[i];
                } else {
                    prefixSum[i] = prefixSum[i - 1];
                }
            }
            int[] suffixSum = new int[n + 1];
            for (int i = n-1; i > 0; i--) {
                if(a[i] < a[i + 1]) {
                    suffixSum[n - i+1] = suffixSum[n - i] + a[i + 1] - a[i];
                } else {
                    suffixSum[n - i+1] = suffixSum[n - i];
                }
            }
            for (int i = 1; i <= m; i++) {
                splitted = br.readLine().split(" ");
                var first = Integer.parseInt(splitted[0]);
                var second = Integer.parseInt(splitted[1]);
                if(first < second)
                    sb.append(prefixSum[second] - prefixSum[first] + "\n");
                else
                    sb.append(suffixSum[first] - suffixSum[second+1] + "\n");
            }

            System.out.println(sb);
        }

    }
}
