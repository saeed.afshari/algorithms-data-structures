package algorithmsdatastructures.consecutivepoints;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(reader.readLine());
            for (int i = 0; i < numberOfTests; i++) {
                reader.readLine();
                var numbers = Arrays.stream(reader.readLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());
                System.out.println(canMakeConsecutiveSequence(numbers));
            }
        }
    }

    private static String canMakeConsecutiveSequence(List<Integer> numbers) {
        if (numbers.size() <= 1) return "YES";
        // 1 2 3 4 -> YES
        // 1 2 5 6 -> NO
        // 1 3 5 6 -> NO
        // 1 2 5 7 -> NO
        // 1 3 5 -> NO
        var initItem = numbers.get(numbers.size() - 1) - numbers.size() + 1;
        if (initItem == numbers.get(0) || initItem == numbers.get(0) + 1) return "YES";
        if (initItem - 1 == numbers.get(0) || initItem - 1 == numbers.get(0) + 1) return "YES";
        return "NO";
    }
}
