package algorithmsdatastructures.div2798

import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.max

fun main() {
    val br = BufferedReader(InputStreamReader(System.`in`))
    br.use {
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            val n = br.readLine().toInt() - 1
            val adjacencyMap = mutableMapOf<Int, MutableList<Int>>()
            repeat(n) {
                val (a, b) = br.readLine().split(" ").map { it.toInt() }
                val aList = adjacencyMap.getOrDefault(a, mutableListOf())
                aList.add(b)
                adjacencyMap[a] = aList
            }

            if (adjacencyMap.getOrDefault(1, 0) == 0) {
                sb.appendLine(0)
            } else {
                val dp = mutableMapOf<Int, Int>()
                buildDP(adjacencyMap, 1, dp)
                sb.appendLine(findMax(adjacencyMap, dp, 1))
            }
        }
        println(sb)
    }
}

fun buildDP(
    adjacencyMap: MutableMap<Int, MutableList<Int>>,
    node: Int,
    dp: MutableMap<Int, Int>
): Int {
    return when (adjacencyMap.getOrDefault(node, emptyList()).size) {
        0 -> {
            dp[node] = 1
            1
        }
        1 -> {
            val value = 1 + buildDP(adjacencyMap, adjacencyMap.getValue(node)[0], dp)
            dp[node] = value
            value
        }
        else -> {
            val value = 1 + buildDP(adjacencyMap, adjacencyMap.getValue(node)[0], dp) + buildDP(
                adjacencyMap,
                adjacencyMap.getValue(node)[1],
                dp
            )
            dp[node] = value
            value
        }
    }
}

fun findMax(adjacencyMap: MutableMap<Int, MutableList<Int>>, dp: MutableMap<Int, Int>, node: Int): Int {
    var max = 0
    return when (adjacencyMap.getOrDefault(node, emptyList()).size) {
        0 -> 0
        1 -> dp.getValue(adjacencyMap.getValue(node)[0]) - 1
        else -> {
            val left = dp.getValue(adjacencyMap.getValue(node)[0])
            val right = dp.getValue(adjacencyMap.getValue(node)[1])
            if (left > right) {
                max += left - 1 + findMax(adjacencyMap, dp, adjacencyMap.getValue(node)[1])
                max
            } else if (left < right) {
                max += right - 1 + findMax(adjacencyMap, dp, adjacencyMap.getValue(node)[0])
                max
            } else {
                val max1 = left - 1 + findMax(adjacencyMap, dp, adjacencyMap.getValue(node)[1])
                val max2 = right - 1 + findMax(adjacencyMap, dp, adjacencyMap.getValue(node)[0])
                max(max1, max2)
            }
        }
    }
}
