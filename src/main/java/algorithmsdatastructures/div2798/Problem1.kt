import java.io.BufferedReader
import java.io.InputStreamReader

fun main(args: Array<String>) {
    val br = BufferedReader(InputStreamReader(System.`in`))
    br.use {
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()

        fun findMinimumStr(a: List<Char>, b: List<Char>, k: Int, sb: StringBuilder) {
            var i = 0
            var j = 0
            var turn = if (a[0] < b[0]) 1 else 2
            while (i < a.size && j < b.size) {
                if (turn == 1) {
                    for (l in 0 until k) {
                        sb.append(a[i])
                        i++
                        if (i == a.size) return
                        if (a[i] > b[j]) break
                    }
                    turn = 2
                } else {
                    for (l in 0 until k) {
                        sb.append(b[j])
                        j++
                        if (j == b.size) return
                        if (b[j] > a[i]) break
                    }
                    turn = 1
                }
            }
        }

        repeat(numberOfTests) {
            val (_, _, k) = br.readLine().split(" ").map { it.toInt() }
            val a = br.readLine()
            val b = br.readLine()
            val sortedA = a.toCharArray().sorted()
            val sortedB = b.toCharArray().sorted()
            findMinimumStr(sortedA, sortedB, k, sb)
            sb.append("\n")
        }
        println(sb)
    }
}
