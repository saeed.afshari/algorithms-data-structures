package algorithmsdatastructures.div2798

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.PriorityQueue

fun main() {
    val br = BufferedReader(InputStreamReader(System.`in`))
    br.use {
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            br.readLine()
            val a = br.readLine().split(" ").map { it.toInt() }
            if (a.size == 1) sb.appendLine("-1")
            else {
                val result = mutableListOf<Int>()
                val pq = PriorityQueue<Int>()
                pq.addAll(a)
                for (i in a.indices) {
                    if (pq.isEmpty()) break
                    val item = pq.remove()
                    if (i < a.size - 2) {
                        if (item != a[i]) {
                            result.add(item)
                        } else {
                            result.add(pq.remove())
                            pq.add(item)
                        }
                    } else {
                        val item2 = pq.remove()
                        if (item != a[a.size - 2] && item2 != a[a.size - 1]) {
                            result.add(item)
                            result.add(item2)
                        } else {
                            result.add(item2)
                            result.add(item)
                        }
                    }
                }
                sb.appendLine(result.joinToString(" "))
            }
        }
        println(sb)
    }
}
