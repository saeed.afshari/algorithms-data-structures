package algorithmsdatastructures.div2798;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Problem3 {
    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                var n = Integer.parseInt(br.readLine()) - 1;
                var adjacencyMap = new HashMap<Integer, List<Integer>>();
                while (n > 0) {
                    var splitted = br.readLine().split(" ");
                    int a = Integer.parseInt(splitted[0]);
                    int b = Integer.parseInt(splitted[1]);
                    var aList = adjacencyMap.getOrDefault(a, new ArrayList<>());
                    aList.add(b);
                    adjacencyMap.put(a, aList);

                    var aList2 = adjacencyMap.getOrDefault(b, new ArrayList<>());
                    aList2.add(a);
                    adjacencyMap.put(b, aList2);
                    n--;
                }

                if (adjacencyMap.getOrDefault(1, Collections.emptyList()).size() == 0) {
                    sb.append("0\n");
                } else {
                    var dp = new HashMap<Integer, Integer>();
                    buildDP(adjacencyMap, 1, dp);
                    sb.append(findMax(adjacencyMap, dp, 1) + "\n");
                }
                numberOfTests--;
            }

            System.out.println(sb);
        }
    }

    private static Integer buildDP(
            Map<Integer, List<Integer>> adjacencyMap,
            Integer node,
            Map<Integer, Integer> dp
    ) {
        switch (adjacencyMap.getOrDefault(node, Collections.emptyList()).size()) {
            case 0: {
                dp.put(node, 1);
                return 1;
            }
            case 1: {
                var value = 1 + buildDP(adjacencyMap, adjacencyMap.get(node).get(0), dp);
                dp.put(node, value);
                return value;
            }
            default: {
                var value = 1 + buildDP(adjacencyMap, adjacencyMap.get(node).get(0), dp) + buildDP(
                        adjacencyMap,
                        adjacencyMap.get(node).get(1),
                        dp
                );
                dp.put(node, value);
                return value;
            }
        }
    }

    private static Integer findMax(Map<Integer, List<Integer>> adjacencyMap, Map<Integer, Integer> dp, Integer node) {
        var max = 0;
        switch (adjacencyMap.getOrDefault(node, Collections.emptyList()).size()) {
            case 0:
                return 0;
            case 1:
                return dp.get(adjacencyMap.get(node).get(0)) - 1;
            default: {
                var left = dp.get(adjacencyMap.get(node).get(0));
                var right = dp.get(adjacencyMap.get(node).get(1));
                if (left > right) {
                    max += left - 1 + findMax(adjacencyMap, dp, adjacencyMap.get(node).get(1));
                    return max;
                } else if (left < right) {
                    max += right - 1 + findMax(adjacencyMap, dp, adjacencyMap.get(node).get(0));
                    return max;
                } else {
                    var max1 = left - 1 + findMax(adjacencyMap, dp, adjacencyMap.get(node).get(1));
                    var max2 = right - 1 + findMax(adjacencyMap, dp, adjacencyMap.get(node).get(0));
                    return Math.max(max1, max2);
                }
            }
        }
    }

}
