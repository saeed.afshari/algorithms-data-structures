package algorithmsdatastructures.codeforces128div2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Program3 {

    public static void main(String args[]) throws Exception {

        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            StringBuilder res = new StringBuilder();


            int tc = Integer.parseInt(br.readLine());

            while (tc-- > 0) {

                String s = br.readLine();

                int one = 0;
                int zero = 0;

                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) == '1') {
                        one++;
                    } else {
                        zero++;
                    }
                }

                int cnt = Math.min(one, zero);

                int pre[] = new int[one + 1];
                int ind = 0;
                int numberOfZeros = 0;

                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) == '1') {
                        ind++;
                    } else {
                        numberOfZeros++;
                    }
                    pre[ind] = numberOfZeros;
                }

                int suf[] = new int[one + 1];
                ind = 0;
                numberOfZeros = 0;

                for (int i = s.length() - 1; i >= 0; i--) {
                    if (s.charAt(i) == '1') {
                        ind++;
                    } else {
                        numberOfZeros++;
                    }
                    suf[ind] = numberOfZeros;
                }

                int l = 0;
                int r = Math.min(one, zero);

                while (l <= r) {

                    int mid = l + (r - l) / 2;

                    boolean ok = false;

                    for (int i = 0; i <= mid; i++) {
                        int temp = numberOfZeros - pre[i] - suf[mid - i];
                        if (temp <= mid) {
                            ok = true;
                            break;
                        }
                    }

                    if (ok) {
                        cnt = mid;
                        r = mid - 1;
                    } else {
                        l = mid + 1;
                    }
                }

                res.append(cnt + "\n");
            }
            System.out.println(res);
        }
    }
}
