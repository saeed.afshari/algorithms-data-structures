package algorithmsdatastructures.codeforces128div2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Program32 {

    public static void main(String args[]) throws Exception {

        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            int tc = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (tc-- > 0) {
                char[] s = br.readLine().toCharArray();
                int numberOfOnes = 0;
                for (int i = 0; i < s.length; i++) {
                    if (s[i] == '1') numberOfOnes++;
                }

                if (numberOfOnes == s.length || numberOfOnes == 0) {
                    sb.append("0\n");
                    continue;
                }

                var prefixes = new int[numberOfOnes + 1];
                var index = 0;
                var numberOfZero = 0;
                for (int i = 0; i < s.length; i++) {
                    if (s[i] == '0') {
                        numberOfZero++;
                    } else {
                        prefixes[index] = numberOfZero;
                        index++;
                    }
                }
                prefixes[index] = numberOfZero;

                var postfixes = new int[numberOfOnes + 1];
                numberOfZero = 0;
                index = 0;
                for (int i = s.length - 1; i >= 0; i--) {
                    if (s[i] == '0') {
                        numberOfZero++;
                    } else {
                        postfixes[index] = numberOfZero;
                        index++;
                    }
                }
                postfixes[index] = numberOfZero;

                var goal = numberOfZero - 1;
                var answer = numberOfZero;
                while (goal >= 0) {
                    if (isItPossible(goal, prefixes, postfixes, numberOfZero)) {
                        answer = goal;
                    } else {
                        break;
                    }
                    goal--;
                }

                sb.append(answer + "\n");
            }
            System.out.println(sb);
        }
    }

    private static boolean isItPossible(int cost, int[] prefixes, int[] postfixes, int numberOfZeros) {
        if(prefixes.length - 1 <= cost) return true;
        for (int j = 0; j <= cost; j++) {
            var newAnswer = numberOfZeros - (prefixes[j] + postfixes[cost - j]);
            if (newAnswer <= cost) return true;
        }
        return false;
    }

}
