package algorithmsdatastructures.codeforces128div2

import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.max

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            val str = br.readLine()
            sb.appendLine(findMinimumCost(str))
        }
        print(sb)
    }
}

fun findMinimumCost(str: String): Int {
    if (str.length == 1) {
        return 0
    }

    var zerosCount = 0
    var oneIndex = 0
    val prefixes = mutableListOf<Int>()
    val postfixes = mutableListOf<Int>()
    prefixes.add(0)
    postfixes.add(0)
    str.forEach {
        if (it == '0') {
            zerosCount++
            prefixes[oneIndex] = zerosCount
        } else {
            oneIndex++
            prefixes.add(zerosCount)
        }
    }
    zerosCount = 0
    oneIndex = 0
    str.reversed().forEach {
        if (it == '0') {
            zerosCount++
            postfixes[oneIndex] = zerosCount
        } else {
            oneIndex++
            postfixes.add(zerosCount)
        }
    }

    if (oneIndex == 0) return 0

    val numberOfZeros = prefixes[prefixes.size - 1]
    var min = numberOfZeros
    for (prefix in prefixes.indices) {
        for (postfix in postfixes.indices) {
            val numberOfRemainingZeros = numberOfZeros - postfixes[postfix] - prefixes[prefix]
            val numberOfOnesPostfix = prefix + postfix
            val maxPostfix = max(numberOfRemainingZeros, numberOfOnesPostfix)
            if (min > maxPostfix) {
                min = maxPostfix
            }
            if (min == 0) return 0
            if (numberOfOnesPostfix > numberOfRemainingZeros) {
                break
            }
        }
        val numberOfRemainingZerosPrefix = numberOfZeros - prefixes[prefix]
        val maxPrefix = max(prefix, numberOfRemainingZerosPrefix)
        if (min > maxPrefix) {
            min = maxPrefix
        }
        if (min == 0) return 0
        if (prefix > numberOfRemainingZerosPrefix) {
            break
        }
    }
    return min
}
