package algorithmsdatastructures.codeforces128div2

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            val ticket = br.readLine().split(" ").map { it.toInt() }
            sb.appendLine(minimumNumberOfElementsInBeautifulArray(ticket))
        }
        print(sb)
    }
}

fun minimumNumberOfElementsInBeautifulArray(items: List<Int>): Int {
    val l1 = items[0]
    val r1 = items[1]
    val l2 = items[2]
    val r2 = items[3]
    val maxL1L2 = maxOf(l1, l2)
    if (maxL1L2 <= r1 && maxL1L2 <= r2) return maxL1L2
    return l1 + l2
}
