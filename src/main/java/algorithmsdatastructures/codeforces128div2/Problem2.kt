package algorithmsdatastructures.codeforces128div2

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            val rowsCols = br.readLine().split(" ").map { it.toInt() }
            var i = 0
            val rootList = mutableListOf<Pair<Int, Int>>()
            repeat(rowsCols[0]) {
                val str = br.readLine()
                for ((col, ch) in str.indices.withIndex()) {
                    if (str[ch] == 'R') {
                        rootList.add(i to col)
                    }
                }
                i++
            }
            sb.appendLine(canFinish(rootList))
        }
        print(sb)
    }
}

fun canFinish(rootList: List<Pair<Int, Int>>): String {
    for (i in rootList.indices) {
        var j = 0
        while (j < rootList.size) {
            if (i != j) {
                if (rootList[j].first < rootList[i].first || rootList[j].second < rootList[i].second) {
                    break
                }
            }
            j++
        }
        if (j == rootList.size) {
            return "YES"
        }
    }
    return "NO"
}
