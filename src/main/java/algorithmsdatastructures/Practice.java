package algorithmsdatastructures;

import java.io.*;
import java.util.*;

public class Practice {

//    private static void mergeSort(int[] arr){
//        mergeSort(arr, 0, arr.length);
//    }

//    private static void mergeSort(int[] arr, int left, int right){
//        if(left >= right) return;
//        var mid = left + right / 2;
//        mergeSort(arr, left, mid);
//        mergeSort(arr, mid + 1, right);
//        merge(arr, left, right);
//    }

//    private static void merge(int[] arr, int left, int right){
//        int[] tmp = new int[arr.length];
//        int index = 0;
//        while (left < right){
//            if(arr[left] <= arr[right]){
//                tmp
//            }
//        }
//    }

    public static String isBalanced(String s) {
        var stack = new LinkedList<Character>();
        var charMap = new HashMap<Character, Character>();
        charMap.put('[', ']');
        charMap.put('(', ')');
        charMap.put('{', '}');
        for (int i = 0; i < s.length(); i++) {
            var currentChar = s.charAt(i);
            if (charMap.containsKey(s.charAt(i))) {
                stack.push(currentChar);
            } else {
                Character match = charMap.entrySet().stream().filter(it -> it.getValue() == currentChar).findFirst().get().getKey();
                if (stack.pop() != match) return "NO";
            }
        }
        return stack.isEmpty() ? "YES" : "NO";
    }

    public static List<Integer> g(){
        return List.of(1, 2);
    }

    private static class MyQueue {
        private Stack<Integer> stack1 = new Stack<Integer>();
        private Stack<Integer> stack2 = new Stack<Integer>();

        void enqueue(Integer item) {
            stack1.push(item);
        }

        Integer dequeue() {
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
            var result = stack2.pop();
            while (!stack2.isEmpty()) {
                stack1.push(stack2.pop());
            }
            return result;
        }

        Integer print() {
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
            var result = stack2.peek();
            while (!stack2.isEmpty()) {
                stack1.push(stack2.pop());
            }
            return result;
        }
    }

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var q = Integer.parseInt(br.readLine());
            var myQueue = new MyQueue();
            for (int i = 0; i < q; i++) {
                var splitted = br.readLine().split(" ");
                System.out.println(splitted[0]);
                if (splitted[0].equals("1")) {
                    myQueue.enqueue(Integer.parseInt(splitted[1]));
                } else if (splitted[0].equals("2")) {
                    myQueue.dequeue();
                } else {
                    System.out.println(myQueue.print());
                }
            }
        }
    }
}
