package algorithmsdatastructures.div3805;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                br.readLine();
                var splitted = br.readLine().split(" ");
                var n = Integer.parseInt(splitted[0]);
                var k = Integer.parseInt(splitted[1]);
                splitted = br.readLine().split(" ");
                var stations = new int[n];
                Map<Integer, List<Integer>> indexMap = new HashMap<>();
                for (int i = 0; i < n; i++) {
                    stations[i] = Integer.parseInt(splitted[i]);
                    List<Integer> values = indexMap.getOrDefault(stations[i], new ArrayList<>());
                    values.add(i);
                    indexMap.put(stations[i], values);
                }
                var copyOfStations = Arrays.copyOf(stations, stations.length);
                Arrays.sort(copyOfStations);
                for (int i = 0; i < k; i++) {
                    splitted = br.readLine().split(" ");
                    var start = Integer.parseInt(splitted[0]);
                    var end = Integer.parseInt(splitted[1]);
                    sb.append(isPossible(indexMap, copyOfStations, start, end));
                }
            }

            System.out.println(sb);
        }
    }

    private static String isPossible(Map<Integer, List<Integer>> stationsMap, int[] stations, int start, int end) {
        List<Integer> values = stationsMap.getOrDefault(start, Collections.emptyList());
        if (values.isEmpty())
            return "NO\n";
        List<Integer> valuesEnd = stationsMap.getOrDefault(end, Collections.emptyList());
        if (valuesEnd.isEmpty())
            return "NO\n";
        var index = binarySearch(stations, end);
        if (index == -1) return "NO\n";

        var lastEndIndex = valuesEnd.get(valuesEnd.size() - 1);
        if (lastEndIndex > values.get(0)) {
            return "YES\n";
        }
        return "NO\n";
    }

    private static int binarySearch(int[] stations, int end) {
        var l = 0;
        var r = stations.length;
        while (l < r) {
            var mid = (l + r) / 2;
            if (stations[mid] == end) {
                return mid;
            } else if (stations[mid] < end) {
                l = mid + 1;
            } else {
                r = mid;
            }
        }
        return -1;
    }
}
