package algorithmsdatastructures.div3805;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var str = br.readLine();
                Set<Character> charSet = new HashSet<>();
                var days = 0;
                for (int i = 0; i < str.length(); i++) {
                    if (charSet.contains(str.charAt(i))) {
                        continue;
                    }
                    charSet.add(str.charAt(i));
                    if (charSet.size() > 3) {
                        days++;
                        charSet.clear();
                        charSet.add(str.charAt(i));
                    }
                }
                if (charSet.size() > 0) {
                    days++;
                }
                sb.append(days).append("\n");
            }

            System.out.println(sb);
        }
    }
}
