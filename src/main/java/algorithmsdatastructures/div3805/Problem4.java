package algorithmsdatastructures.div3805;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem4 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var str = br.readLine();
                Integer w = Integer.parseInt(br.readLine());
                var charArray = str.toCharArray();
                var sum = getStrWeight(str, charArray);
                if (sum <= w) {
                    sb.append(str + "\n");
                } else {
                    var found = false;
                    for (int i = 26; i >= 1; i--) {
                        for (int j = 0; j < charArray.length; j++) {
                            int charCost = charArray[j] - 'a' + 1;
                            if (charCost == i) {
                                charArray[j] = 'A';
                                sum -= charCost;
                                if (sum <= w) {
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (found) {
                            break;
                        }
                    }
                    if (found) {
                        for (int i = 0; i < charArray.length; i++) {
                            if (charArray[i] != 'A') sb.append(charArray[i]);
                        }
                    }
                    sb.append("\n");
                }
            }

            System.out.println(sb);
        }
    }

    private static int getStrWeight(String str, char[] charArray) {
        int sum = 0;
        for (int i = 0; i < charArray.length; i++) {
            sum += str.charAt(i) - 'a' + 1;
        }
        return sum;
    }
}
