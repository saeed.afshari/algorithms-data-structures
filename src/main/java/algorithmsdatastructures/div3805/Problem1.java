package algorithmsdatastructures.div3805;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem1 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                Integer n = Integer.parseInt(br.readLine());
                sb.append(((int)(n - (Math.pow(10, (n.toString().length() - 1))))) + "\n");
            }

            System.out.println(sb);
        }
    }
}
