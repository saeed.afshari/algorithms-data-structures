//package algorithmsdatastructures.div2804;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Map;
//
//public class Problem4 {
//    public static void main(String[] args) throws IOException {
//        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
//            var numberOfTests = Integer.parseInt(br.readLine());
//            var sb = new StringBuilder();
//            while (numberOfTests > 0) {
//                numberOfTests--;
//                var n = Integer.parseInt(br.readLine());
//                var a = Arrays.stream(br.readLine().split(" ")).map(Integer::parseInt).toList();
//                var max = Integer.MIN_VALUE;
//                for (int i = 0; i < a.size(); i++) {
//                    max = Integer.max(max, a.get(i));
//                }
//                var map = new HashMap<Integer, Integer>();
//                for (int i = 0; i < a.size(); i++) {
//                    Integer value = map.getOrDefault(a.get(i), 0);
//                    map.put(a.get(i), value + 1);
//                }
//                var items = map.entrySet().stream().sorted(Map.Entry.comparingByValue()).toList();
//
//                max = Integer.MIN_VALUE;
//
//                for (int i = items.size() - 1; i >= 0; i--) {
//                    var key = items.get(i).getKey();
//                    int k = items.get(i).getValue();
//                    for (int j = 0; j < a.size(); j++) {
//                        if (a.get(i) == key) continue;
//                        k = Math.abs(k - items.get(i).getValue());
//                    }
//                }
//                max = Integer.max(max, items.get(j).getValue() - k);
//
//                sb.append(max + "\n");
//            }
//            System.out.println(sb);
//        }
//    }
//}
