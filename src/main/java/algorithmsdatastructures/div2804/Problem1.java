package algorithmsdatastructures.div2804;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem1 {
    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                Integer n = Integer.parseInt(br.readLine());
                if (n == 1) sb.append("-1\n");
                else {
                    var bits = Integer.toBinaryString(n);
                    var a = "";
                    var b = "";
                    var c = "";
                    for (int i = 0; i < bits.length(); i++) {
                        if (bits.charAt(i) == '0') {
                            a += 1;
                            b += 1;
                            c += 1;
                        } else {
                            a += 1;
                            b += 0;
                            c += 0;
                        }
                    }
                    sb.append(Integer.parseInt(a, 2) + " " + Integer.parseInt(b, 2) + " " + Integer.parseInt(c, 2) + "\n");
                }
            }

            System.out.println(sb);
        }
    }
}
