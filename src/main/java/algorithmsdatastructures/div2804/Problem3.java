package algorithmsdatastructures.div2804;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem3 {
    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var splitted = br.readLine().split(" ");
                var n = Integer.parseInt(splitted[0]);
                var k = Integer.parseInt(splitted[1]);
            }
            System.out.println(sb);
        }
    }
}
