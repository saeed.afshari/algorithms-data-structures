package algorithmsdatastructures.`794div2`

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            bufferedReader.readLine()
            val numbers = bufferedReader.readLine().split(" ").map { it.toInt() }
            sb.appendLine(findTheNumber(numbers))
        }
        println(sb)
    }
}

fun findTheNumber(numbers: List<Int>): String {
    val sum = numbers.sum()
    for (i in numbers.indices) {
        val tmp = (sum - numbers[i]) * 1F / (numbers.size - 1)
        if (tmp == numbers[i] * 1F) return "YES"
    }
    return "NO"
}
