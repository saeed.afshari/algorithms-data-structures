//package algorithmsdatastructures.vonage;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.PriorityQueue;
//
//public class Problem1 {
//
//    public static void main(String[] args) throws IOException {
//        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
//            var queue = new PriorityQueue<Integer>(Integer::compareTo);
//        }
//    }
//
//    public String s(Integer n) {
//        var numberString = Integer.toString(n);
//        var sum = 0;
//        for (var i = 0; i < numberString.length(); i++) {
//            sum += numberString.charAt(i) - '0';
//        }
//        if (aNumber > 500) {
//            throw new IllegalStateException("The input value must be between 1 and 500");
//        }
//        return sum
//
//    }
//
//    private static final int MAX_ALLOWED_NUMBER = 500;
//
//    public int solution(int aNumber) {
//        if (aNumber > MAX_ALLOWED_NUMBER) {
//            throw new IllegalStateException("The input value must be between 1 and 500");
//        }
//        var doubleOfSumOfDigits = getSumOfDigits(aNumber) * 2;
//        for (var i = aNumber; i <= 500; i++) {
//            if (getSumOfDigits(i) == doubleOfSumOfDigits) {
//                return i;
//            }
//        }
//        return -1;
//    }
//
//    private long getSumOfDigits(int aNumber) {
//        var aNumberString = Integer.toString(aNumber);
//        var sum = 0;
//        for (var i = 0; i < aNumberString.length(); i++) {
//            sum += aNumberString.charAt(i) - '0';
//        }
//        return sum;
//    }
//
//}
//
//
//class Solution {
//    private static final int MAX_ALLOWED_NUMBER = 500;
//    private static final int MIN_ALLOWED_NUMBER = 1;
//
//    public int solution(int aNumber) {
//        if (MIN_ALLOWED_NUMBER < 1 || aNumber > MAX_ALLOWED_NUMBER) {
//            throw new IllegalStateException("The input value must be between 1 and 500");
//        }
//        var doubleOfSumOfDigits = getSumOfDigits(aNumber) * 2;
//        for (var i = aNumber; i <= Integer.MAX_VALUE; i++) {
//            if (getSumOfDigits(i) == doubleOfSumOfDigits) {
//                return i;
//            }
//        }
//        throw new IllegalStateException("Unable to find the expected number!");
//    }
//
//    private long getSumOfDigits(int aNumber) {
//        var aNumberString = Integer.toString(aNumber);
//        var sum = 0;
//        for (var i = 0; i < aNumberString.length(); i++) {
//            sum += aNumberString.charAt(i) - '0';
//        }
//        return sum;
//    }
//}
