package algorithmsdatastructures.tree

import java.util.LinkedList

fun main() {
    val leafA = Node("A", mutableListOf())
    val leafC = Node("C", mutableListOf())
    val leafE = Node("E", mutableListOf())
    val leafH = Node("H", mutableListOf())
    val leafK = Node("K", mutableListOf())

    val nodeD = Node("D", mutableListOf(leafC, leafE, leafK))
    val nodeI = Node("I", mutableListOf(leafH))

    val nodeB = Node("B", mutableListOf(leafA, nodeD))
    val nodeG = Node("G", mutableListOf(nodeI))

    val root = Node("F", mutableListOf(nodeB, nodeG))

    val tree = Tree(root)
    println("PreOrder: ${tree.preOrder()}")
    println("PreOrderStack: ${tree.preOrderStack()}")
    println("PostOrder: ${tree.postOrder()}")
    println("LevelOrder: ${tree.levelOrder()}")

    println("DFS recursive(B): ${tree.dfs("B")}")
    println("DFS recursive(C): ${tree.dfs("C")}")
    println("DFS recursive(J): ${tree.dfs("J")}")

    println("DFS stack(B): ${tree.dfsStack("B")}")
    println("DFS stack(C): ${tree.dfsStack("C")}")
    println("DFS stack(J): ${tree.dfsStack("J")}")

    println("BFS queue(B): ${tree.bfs("B")}")
    println("BFS queue(C): ${tree.bfs("C")}")
    println("BFS queue(J): ${tree.bfs("J")}")

    tree.print()

    tree.invert()
    tree.print()

    println("Height of tree is ${tree.height()}")

    println("Depth of Node:I is ${tree.depth("I")}")
    println("Depth of Node:J is ${tree.depth("J")}")

    println("Degree of Node:B is ${tree.degree(nodeB)}")
    println("Degree of Node:I is ${tree.degree(nodeI)}")

    println("Number of Edges is: ${tree.edges()}")

    println("Leaves are: ${tree.leaves()}")

    println("Is a binary tree: ${tree.isBinary()}")
}

data class Tree<T>(private val root: Node<T>) {

    fun dfs(value: T): Node<T>? {
        return dfsRecursive(root, value)
    }

    private fun dfsRecursive(node: Node<T>, value: T): Node<T>? {
        if (node.value == value) {
            return node
        }
        for (n in node.children) {
            val result = dfsRecursive(n, value)
            if (result != null) return result
        }
        return null
    }

    fun dfsStack(value: T): Node<T>? {
        return dfsStack(root, value)
    }

    private fun dfsStack(root: Node<T>, value: T): Node<T>? {
        val stack = LinkedList<Node<T>>()
        stack.push(root)
        while (!stack.isEmpty()) {
            val node = stack.pop()
            if (node.value == value) {
                return node
            }
            for (child in node.children) {
                stack.push(child)
            }
        }
        return null
    }

    fun bfs(value: T): Node<T>? {
        return bfsQueue(root, value)
    }

    private fun bfsQueue(root: Node<T>, value: T): Node<T>? {
        val queue = LinkedList<Node<T>>()
        queue.offer(root)
        while (!queue.isEmpty()) {
            val node = queue.remove()
            if (node.value == value) return node
            for (child in node.children) queue.add(child)
        }
        return null
    }

    fun preOrder(): List<T> {
        val result = mutableListOf<T>()
        traversePreOrder(root, result)
        return result
    }

    fun preOrderStack(): List<T> {
        val result = mutableListOf<T>()
        val queue = LinkedList<Node<T>>()
        queue.push(root)

        while (!queue.isEmpty()) {
            val node = queue.remove()
            result.add(node.value)
            for (n in node.children) {
                queue.add(n)
            }
        }
        return result
    }

    private fun traversePreOrder(node: Node<T>, result: MutableList<T>) {
        result.add(node.value)
        for (child in node.children) {
            traversePreOrder(child, result)
        }
    }

    fun inOrder(): List<T> {
        val result = mutableListOf<T>()
        traversePreOrder(root, result)
        return result
    }

    fun postOrder(): List<T> {
        val result = mutableListOf<T>()
        traversePostOrder(root, result)
        return result
    }

    private fun traversePostOrder(node: Node<T>, result: MutableList<T>) {
        for (child in node.children) {
            traversePostOrder(child, result)
        }
        result.add(node.value)
    }

    fun levelOrder(): List<T> {
        val result = mutableListOf<T>()
        val queue = LinkedList<Node<T>>()
        queue.add(root)
        while (!queue.isEmpty()) {
            val node = queue.remove()
            result.add(node.value)
            for (child in node.children) {
                queue.add(child)
            }
        }
        return result
    }

    fun invert() {
        val queue = LinkedList<Node<T>>()
        queue.add(root)
        while (!queue.isEmpty()) {
            val node = queue.remove()
            node.children.reverse()
        }
    }

    fun height(): Int {
        val levels = traverseWithLevel()
        return levels.groupBy { it.second }.size
    }

    fun depth(node: T): Int {
        val levels = traverseWithLevel()
        return levels.find { it.first == node }?.second ?: -1
    }

    fun degree(node: Node<T>): Int {
        val result = mutableListOf<T>()
        traversePreOrder(node, result)
        return result.size - 1
    }

    fun edges(): Int {
        val result = mutableListOf<T>()
        traversePreOrder(root, result)
        return result.size - 1
    }

    fun leaves(): List<T> {
        val leaves = mutableListOf<T>()
        val stack = LinkedList<Node<T>>()
        stack.push(root)
        while (!stack.isEmpty()) {
            val node = stack.pop()
            if (node.children.isEmpty()) {
                leaves.add(node.value)
            } else {
                for (child in node.children) {
                    stack.push(child)
                }
            }
        }
        return leaves
    }

    fun isBinary(): Boolean {
        val stack = LinkedList<Node<T>>()
        stack.push(root)
        while (!stack.isEmpty()) {
            val node = stack.pop()
            if (node.children.size !in 0..2)
                return false
            for (child in node.children) {
                stack.push(child)
            }
        }
        return true
    }

    fun print() {
        val result = traverseWithLevel()
        val maxNumberOfNodes = result.maxOf { it.second }
        result.groupBy { it.second }.forEach {
            val prefix = (0 until (maxNumberOfNodes - it.key) / 2).map { " " }.joinToString { item -> item }
            print(
                "${
                    it.value.joinToString(
                        " ",
                        prefix = prefix
                    ) { v -> v.first.toString() }
                } "
            )
            println()
        }
    }

    private fun traverseWithLevel(): Set<Pair<T, Int>> {
        val result = mutableSetOf<Pair<T, Int>>()
        val queue = LinkedList<Pair<Node<T>, Int>>()
        queue.add(root to 0)
        while (!queue.isEmpty()) {
            val pair = queue.remove()
            result.add(pair.first.value to pair.second)
            for (child in pair.first.children) {
                queue.add(child to pair.second + 1)
            }
        }
        return result
    }
}
