package algorithmsdatastructures.tree

data class Node<T>(
    val value: T,
    val children: MutableList<Node<T>>
)
