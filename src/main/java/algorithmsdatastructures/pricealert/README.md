# Challenge

In any trading platform there is a feature which allows users to be notified by price changes. I call this feature Price
Alert. I am going to show you a potential solution for implementing such feature.

You should assume that each user can set multiple price alerts. Also, the price changes will be received from an
external system via a websocket or queue every millisecond.

The price event ingestion from the external system and also implementing the notification system is out of the scope of
this challenge. I am going to implement a class called PriceAlertService in order to detect users whom the notification
should be sent to them.

The class should provide a mechanism for accepting new target price setups. Also, it should return a list of users which
the target price reached for those and remove the prices from the pool.

Finally, you should consider that the price can grow and drops in both directions. However, for the simplicity we should
imagine that the price is only growing and if we can solve the problem in one direction we can solve it on the other
direction as well.

## Step 1: Designing the system interface

The expected behaviour can be translated to the following interface

```java
public interface PriceAlert {

    void addPriceAlert(TargetPriceDto targetPriceDto);

    List<PriceAlertDto> popPriceAlerts(BigDecimal price);
}
```

## Algorithm and Data Structure

First off we should be able to come up with a solution to solve the basic problems. In this phase, I pick different data
structures one by one and then come up with some time complexity for each expected behaviour.

### behaviour 1 insertion

```java
void addPriceAlert(TargetPriceDto targetPriceDto)
```

The time complexity for inserting a new TargetPriceDto into the pool should be linear, and it should not be quadratic in
the worst case.

#### Sorted list or linked list

If we pick a sorted list: (O(N) in the worst case scenario)

- the time complexity for the insertion would be O(N) and N is the number of available price alerts.
- the time complexity for the lookup will be Log(N) using binary search.

#### HashMap, HashSet (X)

We cannot solve the lookup problem with HashMap or HashSet as the price is a range value, and we should remove prices if
the current price is higher or lower than the PriceAlerts

#### PriorityQueue

If we pick a PriorityQueue: (O(Log N) in the worst case scenario)

- the time complexity for the insertion would be O(Log N) and N is the number of available price alerts.
- the time complexity for the lookup will be Log(N) in the worst case scenario, and it will be O(1) in the best case
  scenario because the priority queue uses Min/Max heap depending on the direction of price growth. Min heap for the
  case that the price is growing and Max heap for the case that the price is dropping.

We stop here because Log(N) solutions are ideal in many scenarios, and we only do the further improvement if it was
needed.

# Step 2:Implementation

We could solve the problem, and it is now time to implement the solution.

```java
public class PriceAlertService implements PriceAlert {

    private PriorityQueue<PriceAlertDto> minHeap = new PriorityQueue<>();
    private PriorityQueue<PriceAlertDto> maxHeap = new PriorityQueue<>(
            Comparator.comparing(PriceAlertDto::price, Comparator.reverseOrder())
    );

    @Override
    public void addNewTargetPrice(PriceAlertDto priceAlertDto) {
        if (priceAlertDto.priceGrowthDirection() == PriceGrowthDirection.GROWING)
            minHeap.add(priceAlertDto);
        else
            maxHeap.add(priceAlertDto);
    }

    @Override
    public List<PriceAlertDto> popTargetPrices(BigDecimal currentPrice) {
        var result = new LinkedList<PriceAlertDto>();
        while (minHeap.peek() != null) {
            if (minHeap.peek().price().compareTo(currentPrice) <= 0)
                result.add(minHeap.poll());
            else
                break;
        }
        while (maxHeap.peek() != null) {
            if (maxHeap.peek().price().compareTo(currentPrice) >= 0)
                result.add(maxHeap.poll());
            else
                break;
        }
        return result;
    }
}
```

In the implementation, I used min and max heaps in order to cover the price dropping and growing scenarios. 


# Step 3:Scale the solution
The solution is super fast and it can manage millions of PriceAlert per second.
However, in order to make the solution scalable we can partition the data.

# Step 4: Single point of failure
In order to avoid having a single point of failure in the system, we can simply launch more than one
instance of the application and put them behind a load balancer.

