package algorithmsdatastructures.pricealert;

public enum PriceGrowthDirection {
    GROWING,
    DROPPING
}
