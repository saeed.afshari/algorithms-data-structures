package algorithmsdatastructures.pricealert;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

public class PriceAlertService implements PriceAlert {

    private final PriorityQueue<PriceAlertDto> minHeap = new PriorityQueue<>();
    private final PriorityQueue<PriceAlertDto> maxHeap = new PriorityQueue<>(
            Comparator.comparing(PriceAlertDto::price, Comparator.reverseOrder())
    );

    @Override
    public void addNewTargetPrice(PriceAlertDto priceAlertDto) {
        if (priceAlertDto.priceGrowthDirection() == PriceGrowthDirection.GROWING)
            minHeap.add(priceAlertDto);
        else
            maxHeap.add(priceAlertDto);
    }

    @Override
    public List<PriceAlertDto> popTargetPrices(BigDecimal currentPrice) {
        var result = new LinkedList<PriceAlertDto>();
        while (minHeap.peek() != null) {
            if (minHeap.peek().price().compareTo(currentPrice) <= 0)
                result.add(minHeap.poll());
            else
                break;
        }
        while (maxHeap.peek() != null) {
            if (maxHeap.peek().price().compareTo(currentPrice) >= 0)
                result.add(maxHeap.poll());
            else
                break;
        }
        return result;
    }
}
