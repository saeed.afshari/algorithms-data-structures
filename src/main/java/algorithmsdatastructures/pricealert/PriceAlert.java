package algorithmsdatastructures.pricealert;

import java.math.BigDecimal;
import java.util.List;

public interface PriceAlert {

    void addNewTargetPrice(PriceAlertDto targetPriceDto);

    List<PriceAlertDto> popTargetPrices(BigDecimal currentPrice);
}
