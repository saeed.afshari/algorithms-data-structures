package algorithmsdatastructures.pricealert;

import java.math.BigDecimal;

public record PriceAlertDto(
        String username,
        PriceGrowthDirection priceGrowthDirection,
        BigDecimal price
) implements Comparable<PriceAlertDto> {

    @Override
    public int compareTo(PriceAlertDto targetPriceDto) {
        return this.price.compareTo(targetPriceDto.price);
    }
}
