package algorithmsdatastructures.winner;

import java.util.*;

public class Winner {

    private static class WinnerScore {
        private final String name;
        private final int score;

        WinnerScore(String name, int score) {
            this.name = name;
            this.score = score;
        }
    }

    /*
    each line composed of a space separated a round winner name and the score for the winner
     */
    public static String wins(List<String> lines) {
        var rounds = new ArrayList<WinnerScore>(lines.size());
        for (var line : lines) {
            String[] winnerScore = line.split(" ");
            rounds.add(new WinnerScore(winnerScore[0], Integer.parseInt(winnerScore[1])));
        }
        var sumScoreMap = new HashMap<String, Integer>();
        for (var round : rounds) {
            Integer previousScore = sumScoreMap.putIfAbsent(round.name, round.score);
            if (previousScore != null) {
                sumScoreMap.put(round.name, round.score + previousScore);
            }
        }
        Integer max = sumScoreMap.values().stream().max(Comparator.comparing(a -> a, Integer::compareTo)).get();
        var winnerScoreMap = new HashMap<String, Integer>();
        for (var round : rounds) {
            Integer previousScore = winnerScoreMap.putIfAbsent(round.name, round.score);
            if (previousScore != null) {
                winnerScoreMap.put(round.name, round.score + previousScore);
            }
            if (sumScoreMap.get(round.name) >= max && winnerScoreMap.get(round.name) >= max) {
                return round.name;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        var lines = new ArrayList<String>();
        int numberOfRounds = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < numberOfRounds; i++) {
            lines.add(scanner.nextLine());
        }
        String winner = wins(lines);

        System.out.println(winner);
    }
}
