package algorithmsdatastructures.codeforces788

import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.absoluteValue

fun main() {
    BufferedReader(InputStreamReader(System.`in`)).use { br ->
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) { test ->
            br.readLine()
            val integers = br.readLine().split(" ").map { it.toInt() }.toMutableList()
            sb.appendLine(solve(integers))
        }
        print(sb)
    }
}

fun solve(integers: MutableList<Int>): String {
    var numberOfNegatives = 0
    for (i in integers.indices) {
        if (integers[i] < 0) {
            numberOfNegatives++
        }
    }
    for (i in 0 until integers.size) {
        if (i < numberOfNegatives) {
            integers[i] = integers[i].absoluteValue * -1
        } else {
            integers[i] = integers[i].absoluteValue
        }
    }
    for (i in 0 until integers.size - 1) {
        if (integers[i] > integers[i + 1]) {
            return "NO"
        }
    }
    return "YES"
}

