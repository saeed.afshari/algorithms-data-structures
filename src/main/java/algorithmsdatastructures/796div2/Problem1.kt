package algorithmsdatastructures.`796div2`

import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.pow

fun main() {
    val br = BufferedReader(InputStreamReader(System.`in`))
    br.use {
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            val x = br.readLine().toInt()
            if (x == 1 || x == 2) {
                sb.appendLine(3)
            } else if (x % 2 > 0) {
                sb.appendLine(1)
            } else {
                val bits = x.toString(2)
                val lastIndex = bits.lastIndexOf('1')
                val lastBit = bits.length - lastIndex - 1
                val pow = 2.0.pow(lastBit).toInt()
                if((pow and x > 0) && (pow xor x > 0)) sb.appendLine(pow)
                else sb.appendLine(pow + 1)
            }
        }
        println(sb)
    }
}
