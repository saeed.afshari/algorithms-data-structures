package algorithmsdatastructures.`796div2`

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val br = BufferedReader(InputStreamReader(System.`in`))
    br.use {
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            val (_, k) = br.readLine().split(" ")
            val mashrooms = br.readLine().split(" ").map { it.toInt() }
            val adjacencyMap = buildList(mashrooms)
            var max = Int.MIN_VALUE
            for (i in adjacencyMap.keys) {

            }
        }
        println(sb)
    }
}

data class Point(
    val index: Int,
    val numberOfMashrooms: Int
)

fun buildList(mashrooms: List<Int>): Map<Int, List<Point>> {
    if (mashrooms.size == 1) {
        return mapOf(0 to listOf(Point(0, mashrooms[0])))
    }
    val adjacencyMap = mutableMapOf<Int, List<Point>>()
    for (i in mashrooms.indices) {
        if (i == 0) {
            adjacencyMap[i] = listOf(Point(i, mashrooms[i]), Point(i, mashrooms[i]))
            continue
        }
        adjacencyMap[i] = listOf(Point(i - 1, mashrooms[i - 1]), Point(i, mashrooms[i]), Point(i, mashrooms[i]))
    }
    return adjacencyMap
}
