package algorithmsdatastructures.`796div2`

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val br = BufferedReader(InputStreamReader(System.`in`))
    br.use {
        val numberOfTests = br.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            br.readLine()
            val tokens = br.readLine().split(" ").map { it.toInt() }
            if (tokens.all { it % 2 > 0 }) {
                sb.appendLine(0)
            } else if (tokens.any { it % 2 > 0 }) {
                sb.appendLine(tokens.filter { it % 2 == 0 }.size)
            } else {
                val min = tokens.minOf { it }
                var reduction = 0
                if (min in 4..100) {
                    if (tokens.any { it / 10 % 2 > 0 }) {
                        sb.appendLine(tokens.size)
                        return
                    }
                    if (tokens.any { ((it + min) / 2) % 2 == 0 }) {
                        reduction = -1
                    }
                }
                sb.appendLine(numbers(min) + tokens.size - 1 + reduction)
            }
        }
        println(sb)
    }
}

fun numbers(value: Int): Int {
    var v = value
    var i = 0
    while (v % 2 == 0) {
        i++
        v /= 2
    }
    return i
}
