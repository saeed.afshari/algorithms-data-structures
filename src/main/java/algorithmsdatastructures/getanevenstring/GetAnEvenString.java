package algorithmsdatastructures.getanevenstring;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class GetAnEvenString {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfTests = Integer.parseInt(scanner.nextLine());
        var tests = new ArrayList<String>();
        for (int i = 0; i < numberOfTests; i++) {
            tests.add(scanner.nextLine());
        }
        List<Integer> minimumNumberOfRemovalPeTest = findTheMinimumNumberOfCharactersToBeRemoved(tests);

        minimumNumberOfRemovalPeTest.forEach(item -> System.out.println(item));
    }

    static List<Integer> findTheMinimumNumberOfCharactersToBeRemoved(List<String> tests) {
        if (tests.size() == 0) {
            return new ArrayList<>(0);
        }
        var result = new ArrayList<Integer>(tests.size());
        for (var test : tests) {
            if (test.length() == 0) {
                result.add(0);
                continue;
            }
            if (test.length() == 1) {
                result.add(1);
                continue;
            }
            var map = new HashMap<Character, Integer>();
            int numberOfOdd = 0;
            for (int i = 0; i < test.length(); i++) {
                char ch = test.charAt(i);
                if (map.containsKey(ch)) {
                    map.put(ch, map.get(ch) + 1);
                    if (map.get(ch) % 2 == 0) {
                        map.remove(ch);
                        numberOfOdd += map.size();
                        map.clear();
                    }
                } else {
                    map.put(ch, 1);
                }
            }
            numberOfOdd += map.size();
            result.add(numberOfOdd);
        }
        return result;
    }
}
