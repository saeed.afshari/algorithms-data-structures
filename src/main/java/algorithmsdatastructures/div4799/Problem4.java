package algorithmsdatastructures.div4799;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class Problem4 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            var possibleOptions = new HashSet<String>();
            possibleOptions.add("00:00");
            possibleOptions.add("01:10");
            possibleOptions.add("02:20");
            possibleOptions.add("03:30");
            possibleOptions.add("04:40");
            possibleOptions.add("05:50");
            possibleOptions.add("10:01");
            possibleOptions.add("11:11");
            possibleOptions.add("12:21");
            possibleOptions.add("13:31");
            possibleOptions.add("14:41");
            possibleOptions.add("15:51");
            possibleOptions.add("20:02");
            possibleOptions.add("22:22");
            possibleOptions.add("23:32");
            possibleOptions.add("21:12");
            while (numberOfTests > 0) {
                numberOfTests--;
                var strings = br.readLine().split(" ");
                var visited = new HashSet<String>();
                var result = new HashSet<String>();
                var item = strings[0];
                var additional = Integer.parseInt(strings[1]);
                while (!visited.contains(item)) {
                    visited.add(item);
                    if (possibleOptions.contains(item)) {
                        result.add(item);
                    }
                    item = addMinutes(item, additional);
                }
                sb.append(result.size() + "\n");
            }

            System.out.println(sb);
        }
    }

    private static String addMinutes(String someString, Integer addition) {
        String[] split = someString.split(":");
        var hour = Integer.parseInt(split[0]);
        var minutes = Integer.parseInt(split[1]);

        var additionalHours = addition / 60;
        var additionalMinutes = addition % 60;

        if (hour + additionalHours >= 24) {
            hour = (hour + additionalHours) % 24;
        } else {
            hour = hour + additionalHours;
        }
        if (minutes + additionalMinutes >= 60) {
            minutes = (minutes + additionalMinutes) % 60;
            hour += 1;
            if (hour >= 24) {
                hour %= 24;
            }
        } else {
            minutes = minutes + additionalMinutes;
        }
        return String.format("%02d:%02d", hour, minutes);
    }

}
