package algorithmsdatastructures.div4799;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                br.readLine();
                var rows = new ArrayList<String>(8);
                for (int i = 0; i < 8; i++) {
                    rows.add(br.readLine());
                }
                var prevRowHashs = countOccurences(rows.get(0), '#');
                var result = "";
                for (int i = 1; i < 8; i++) {
                    if (prevRowHashs == 2 && countOccurences(rows.get(i), '#') == 1 && countOccurences(rows.get(i + 1), '#') == 2) {
                        result += (i + 1) + " " + (rows.get(i).indexOf("#") + 1);
                        break;
                    }
                    prevRowHashs = countOccurences(rows.get(i), '#');
                }
                sb.append(result + "\n");
            }

            System.out.println(sb);
        }
    }

    private static int countOccurences(
            String someString, char searchedChar) {
        var count = 0;
        for (int i = 0; i < someString.length(); i++) {
            if (someString.charAt(i) == searchedChar) {
                count++;
            }
        }
        return count;
    }

}
