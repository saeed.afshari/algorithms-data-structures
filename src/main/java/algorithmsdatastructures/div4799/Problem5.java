//package algorithmsdatastructures.div4799;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.Arrays;
//import java.util.stream.Collectors;
//
//public class Problem5 {
//
//    public static void main(String[] args) throws IOException {
//        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
//            var numberOfTests = Integer.parseInt(br.readLine());
//            var sb = new StringBuilder();
//            while (numberOfTests > 0) {
//                numberOfTests--;
//                var s = Integer.parseInt(br.readLine().split(" ")[1]);
//                var a = Arrays.stream(br.readLine().split(" ")).map(it -> Integer.parseInt(it)).collect(Collectors.toList());
//                if (a.size() == 1) {
//                    if (a.get(0) == s) sb.append("1\n");
//                    else sb.append("-1\n");
//                } else {
//                    // 1 0 0
//                    // 1
//                    // [0, 1, 1, 1]
//                    // [0, 0, 0, 1]
//                    var prefixes = new int[a.size() + 1];
//                    for (int i = 0; i < a.size(); i++) {
//                        prefixes[i] = prefixes[i - 1] + a.get(i);
//                    }
//
//                    var postfixes = new int[a.size() + 1];
//                    for (int i = a.size() - 2; i >= 0; i--) {
//                        postfixes[i] = postfixes[i + 1] + a.get(i);
//                    }
//
//                    var answer = Integer.MAX_VALUE;
//                    for (int i = 0; i < prefixes.length; i++) {
//                        var l = 0;
//                        var r = prefixes.length - i;
//                        // i = 0, l = 0, r = 4
//                        // mid = 2
//                        while (l < r) {
//                            var mid = (l + r) / 2;
//                            if (prefixes[i] + postfixes[mid] == s) {
//                                answer = Math.min(answer, i + mid);
//                            } else if() {
//
//                            } else {
//                                r = mid - 1;
//                            }
//                        }
//                    }
//
//                    if (answer == Integer.MAX_VALUE) {
//                        sb.append("-1\n");
//                    } else {
//                        sb.append(answer + "\n");
//                    }
//                }
//            }
//
//            System.out.println(sb);
//        }
//    }
//}
