package algorithmsdatastructures.div4799;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                br.readLine();
                var a = Arrays.stream(br.readLine().split(" ")).map(it -> Integer.parseInt(it)).collect(Collectors.toList());
                var map = new HashMap<Integer, Integer>();
                for (int i = 0; i < a.size(); i++) {
                    var value = map.getOrDefault(a.get(i), 0);
                    value++;
                    map.put(a.get(i), value);
                }
                var max = 0;
                var numbers = 0;
                for (var entry : map.entrySet()) {
                    if (entry.getValue() == 1) max++;
                    else if (entry.getValue() % 2 == 1) max++;
                    else {
                        numbers++;
                    }
                }
                if (numbers % 2 == 0)
                    max += numbers;
                else
                    max += numbers - 1;
                sb.append(max + "\n");
            }

            System.out.println(sb);
        }
    }

}
