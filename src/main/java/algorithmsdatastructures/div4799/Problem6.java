package algorithmsdatastructures.div4799;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Problem6 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                br.readLine();
                var a = Arrays.stream(br.readLine().split(" ")).map(it -> Integer.parseInt(it)).collect(Collectors.toList());
                var aMap = new int[10];
                a.forEach(it -> {
                    int key = it % 10;
                    aMap[key] = aMap[key] + 1;
                });

                var aList = new ArrayList<Integer>();
                for (int i = 0; i < aMap.length; i++) {
                    for (int j = 0; j < Math.min(3, aMap[i]); j++) {
                        aList.add(i);
                    }
                }

                sb.append(found(aList) + "\n");
            }

            System.out.println(sb);
        }
    }

    private static String found(List<Integer> aMap) {
        for (int i = 0; i < aMap.size(); i++) {
            for (int j = i + 1; j < aMap.size(); j++) {
                for (int k = j + 1; k < aMap.size(); k++) {
                    if ((aMap.get(i) + aMap.get(j) + aMap.get(k)) % 10 == 3) {
                        return "YES";
                    }
                }
            }
        }
        return "NO";
    }
}
