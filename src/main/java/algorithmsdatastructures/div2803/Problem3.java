package algorithmsdatastructures.div2803;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class Problem3 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var n = Integer.parseInt(br.readLine());
                var a = Arrays.stream(br.readLine().split(" ")).map(Integer::parseInt).sorted().collect(Collectors.toList());

                sb.append(isClosed(a, n));
            }
            System.out.println(sb);
        }
    }

    private static String isClosed(List<Integer> a, int n) {
        var aSet = new HashSet<Integer>();

        int numberOfPositives = 0;
        int numberOfNegative = 0;
        int numberZero = 0;
        for (int i = 0; i < n; i++) {
            aSet.add(a.get(i));
            if (a.get(i) == 0) numberZero++;
            else if (a.get(i) > 0) numberOfPositives++;
            else numberOfNegative++;
        }

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                for (int k = j + 1; k < n; k++) {
                    if (!aSet.contains(a.get(i) + a.get(j) + a.get(k)))
                        return "NO\n";
                }
            }
        }
        return "YES\n";
    }
}
