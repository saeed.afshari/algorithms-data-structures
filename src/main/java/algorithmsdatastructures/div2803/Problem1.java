package algorithmsdatastructures.div2803;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Problem1 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var n = Integer.parseInt(br.readLine());
                var a = Arrays.stream(br.readLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());
                long x = 0;
                for (int i = 0; i < n; i++) {
                    long xorResult;
                    int j;
                    if (i == 0) {
                        j = 2;
                        xorResult = a.get(1);
                    } else {
                        xorResult = a.get(0);
                        if (i == 1) j = 2;
                        else j = i - 1;
                    }
                    for (; j < n; j++) {
                        if (i == j) continue;
                        xorResult ^= a.get(j);
                    }
                    if (xorResult == a.get(i)) {
                        x = a.get(i);
                        break;
                    }
                }
                sb.append(x + "\n");
            }
            System.out.println(sb);
        }
    }
}
