package algorithmsdatastructures.div2803;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Problem2 {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var splitted = br.readLine().split(" ");
                var n = Integer.parseInt(splitted[0]);
                var k = Integer.parseInt(splitted[1]);
                var a = Arrays.stream(br.readLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());

                sb.append(getResult(a, n, k));
            }
            System.out.println(sb);
        }
    }

    private static String getResult(List<Integer> a, int n, int k) {
        if (n <= 2) return "0\n";
        else if (k == 1) {
            return ((n - 1) / 2) + "\n";
        } else {
            int numberOfTops = 0;
            for (int i = 1; i < n - 1; i++) {
                if (a.get(i) > (a.get(i - 1) + a.get(i + 1))) numberOfTops++;
            }
            return numberOfTops + "\n";
        }
    }
}
