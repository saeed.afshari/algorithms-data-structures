//package algorithmsdatastructures.div2122;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.Arrays;
//import java.util.List;
//
//public class Problem4 {
//
//    public static void main(String[] args) throws IOException {
//        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
//            var numberOfTests = Integer.parseInt(br.readLine());
//            var sb = new StringBuilder();
//            while (numberOfTests > 0) {
//                numberOfTests--;
//                var splitted = br.readLine().split(" ");
//                var n = Integer.parseInt(splitted[0]);
//                var k = Integer.parseInt(splitted[1]);
//                var targetArray = Arrays.stream(br.readLine().split(" ")).map(Integer::parseInt).toList();
//                var coins = Arrays.stream(br.readLine().split(" ")).map(Integer::parseInt).toList();
//
//                var operationsArray = calculateRequiredOperations(targetArray);
//                var maxNumberOfOperations = getMax(operationsArray);
//                var takenCoinsPerOperationPerIndexMatrix = new int[maxNumberOfOperations][targetArray.size()];
//                var max = findMaximumCoins(0, n, k, operationsArray, coins, takenCoinsPerOperationPerIndexMatrix);
//                sb.append(String.format("%d", max))
//                sb.toString();
//            }
//
//            System.out.println(sb);
//        }
//    }
//
//    private static int findMaximumCoins(int index, int n, int k, int[] operationsArray, List<Integer> coins, int[][] takenCoinsPerOperationPerIndexMatrix) {
//        if (k==0) {
//            return 0;
//        }
//        var take = coins.get(index) + findMaximumCoins(index+1, n, k, operationsArray, coins, takenCoinsPerOperationPerIndexMatrix);
//        var skip = findMaximumCoins(index+1, n, k, operationsArray, coins, takenCoinsPerOperationPerIndexMatrix);
//        takenCoinsPerOperationPerIndexMatrix[]
//    }
//
//    private static int[] calculateRequiredOperations(List<Integer> targetArray) {
//        var result = new int[targetArray.size()];
//        for (int i = 0; i < targetArray.size(); i++) {
//            result[i] = 1;
//        }
//        for (int i = 0; i < targetArray.size(); i++) {
//            result[i] = calculateRequiredOperations(targetArray.get(i));
//        }
//        return result;
//    }
//
//    private static int calculateRequiredOperations(int target) {
//        var requiredOperations = 0;
//        var value = 1;
//        if (target == value) return 0;
//        while (value < target) {
//            value *= 2;
//            requiredOperations++;
//        }
//        if (value == target) return requiredOperations;
//        requiredOperations--;
//        value /= 2;
//        int x = 2;
//        while (value != target) {
//            var newValue = value + value / x;
//            if (newValue > target) {
//                x++;
//                continue;
//            }
//            value = newValue;
//            requiredOperations++;
//        }
//        return requiredOperations;
//    }
//
//    private static int getMax(int[] anArray) {
//        int max = Integer.MIN_VALUE;
//        for (int i = 0; i < anArray.length; i++) {
//            max = Integer.max(max, anArray[i]);
//        }
//        return max;
//    }
//}
