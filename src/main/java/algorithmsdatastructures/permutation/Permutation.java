package algorithmsdatastructures.permutation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class Permutation {

    public static void main(String[] args) throws IOException {
//        List<List<Integer>> lists = calculatePermutations(Collections.emptyList(), List.of(1, 2, 3));
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            var aList = new ArrayList<TreeNode>();
            aList.add(new TreeNode(-1, "A"));
            aList.add(new TreeNode(0, "B"));
            aList.add(new TreeNode(0, "C"));
            aList.add(new TreeNode(1, "D"));
            aList.add(new TreeNode(1, "E"));
            aList.add(new TreeNode(2, "F"));
            aList.add(new TreeNode(2, "G"));

            deleteNode(aList, 2);

            aList.toString();
        }
    }

    static class TreeNode {
        private String value;
        private int parentNode;

        TreeNode(int parentNode, String value) {
            this.value = value;
            this.parentNode = parentNode;
        }
    }

    // [A(-1),B(0),C(0),D(1),E(1),F(2),G(2)]
    private static void deleteNode(List<TreeNode> nodes, int index) {
        int nodeParentIndex = nodes.get(index).parentNode;
        for (int i = index+1; i < nodes.size(); i++) {
            var node = nodes.get(i);
            if (node.parentNode == index) {
                node.parentNode = nodeParentIndex;
                deleteNode(nodes, i);
                i--;
            } else if (node.parentNode > index) {
                node.parentNode--;
            }
        }
        nodes.remove(index);
    }

    private static void deleteNode2(List<TreeNode> nodes, int index) {
        nodes.remove(index);
        Iterator<TreeNode> iterator = nodes.iterator();
        while (iterator.hasNext()) {
            var node = iterator.next();
            if (node.parentNode == index) {
                iterator.remove();
            }
            if (node.parentNode > index) {
                node.parentNode--;
            }
        }
    }

    private static List<List<Integer>> calculatePermutations(List<Integer> prefix, List<Integer> remaining) {
        if (remaining.isEmpty()) {
            return List.of(prefix);
        }
        int remainingSize = remaining.size();
        var result = new ArrayList<List<Integer>>(remainingSize);
        for (int i = 0; i < remainingSize; i++) {
            var newPrefix = new ArrayList<>(prefix);
            newPrefix.add(remaining.get(i));
            var newRemaining = new ArrayList<>(remaining.subList(0, i));
            if (i + 1 < remainingSize)
                newRemaining.addAll(remaining.subList(i + 1, remainingSize));
            result.addAll(calculatePermutations(newPrefix, newRemaining));
        }
        return result;
    }

    private static Set<String> possiblePermutation(String[] remaining) {
        if (remaining == null || remaining.length == 0) return Collections.emptySet();
        Set<String> permutations = new HashSet<>();
        possiblePermutation(new ArrayList<>(), Arrays.stream(remaining).collect(Collectors.toList()));
        return permutations;
    }

    private static void possiblePermutation(List<String> prefix, List<String> remaining) {
        if (remaining.size() == 0) {
            System.out.println(String.join("", prefix));
            return;
        }
        for (int i = 0; i < remaining.size(); i++) {
            var beforeAfter = new ArrayList<String>(remaining.size());
            for (int j = 0; j < i; j++) {
                beforeAfter.set(j, remaining.get(j));
            }
            for (int j = i + 1; j < remaining.size(); j++) {
                beforeAfter.set(j, remaining.get(j));
            }
            prefix.add(remaining.get(i));
            possiblePermutation(prefix, beforeAfter);
        }
    }
}
