package algorithmsdatastructures.permutation

import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.LinkedList

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        repeat(numberOfTests) {
            PermutationUsingStack().permutation(bufferedReader.readLine().split(" ").map { it.toInt() })
        }
    }
}

class PermutationUsingStack {

    internal data class Entry(val prefix: List<Int>, val remining: List<Int>)

    private val stack = LinkedList<Entry>()

    fun permutation(allIntegers: List<Int>) {
        if (allIntegers.isEmpty()) {
            return
        }
        permutation(emptyList(), allIntegers)
        while (!stack.isEmpty()) {
            val entry = stack.remove()
            if (entry.remining.isEmpty()) {
                println(entry.prefix)
            } else {
                permutation(entry.prefix, entry.remining)
            }
        }
    }

    fun permutation(prefix: List<Int>, remaining: List<Int>) {
        for (i in remaining.indices) {
            val newPrefix = prefix + remaining[i]
            val newRemaining = remaining.subList(0, i) + remaining.subList(i + 1, remaining.size)
            stack.offer(Entry(newPrefix, newRemaining))
        }
    }
}
