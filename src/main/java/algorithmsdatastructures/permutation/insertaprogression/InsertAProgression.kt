package algorithmsdatastructures.permutation.insertaprogression

import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.absoluteValue
import kotlin.math.max
import kotlin.math.min

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        repeat(numberOfTests) {
            val (n, x) = bufferedReader.readLine().split(" ")
            calculateMinimumScore(bufferedReader.readLine().split(" ").map { it.toInt() }, x.toInt())
        }
    }
}

fun calculateMinimumScore(integers: List<Int>, x: Int) {
    val minIntegers = integers.minOf { it }
    val maxIntegers = integers.maxOf { it }
    var ansMax = max(0, (x - maxIntegers) * 2)
    var ansMin = (minIntegers - 1) * 2
    val minEdges = min(integers[0], integers[integers.size - 1])
    val maxEdges = max(integers[0], integers[integers.size - 1])
    ansMin = min(ansMin, minEdges - 1)
    ansMax = min(ansMax, max(0, (x - maxEdges)))
    var ans = ansMax + ansMin
    ans += sumUpAdjacentItems(integers)
    println(ans)
}

fun sumUpAdjacentItems(aList: List<Int>): Int {
    var sum = 0
    for (i in aList.indices) {
        sum += if (i < aList.size - 1) {
            (aList[i] - aList[i + 1]).absoluteValue
        } else {
            0
        }
    }
    return sum
}
