package algorithmsdatastructures.permutation

import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    val bufferedReader = BufferedReader(InputStreamReader(System.`in`))
    bufferedReader.use {
        val numberOfTests = bufferedReader.readLine().toInt()
        val sb = StringBuilder()
        repeat(numberOfTests) {
            bufferedReader.readLine()
            val a = bufferedReader.readLine().split(" ").map { it.toInt() }
            permutation(emptyList(), a, a)
            val tmp = mutableListOf<Int>()
            for (i in a.indices) {
                tmp.add(Int.MAX_VALUE)
            }
            var min = tmp.toList()
            for (i in allPermutations.indices) {
                min = getMin(min, allPermutations[i])
            }
            if (different(min, tmp)) {
                sb.appendLine(min.joinToString(" "))
            } else {
                sb.appendLine("-1")
            }
            allPermutations = mutableListOf()
        }
        println(sb)
    }
}

fun getMin(min: List<Int>, ints: List<Int>): List<Int> {
    for (i in min.indices) {
        if (min[i] > ints[i]) {
            return ints
        } else if (min[i] < ints[i]) {
            return min
        }
    }
    return min
}

fun notEquals(min: List<Int>, ints: List<Int>): Boolean {
    for (i in min.indices) {
        if (min[i] == ints[i]) {
            return false
        }
    }
    return true
}

fun different(min: List<Int>, ints: List<Int>): Boolean {
    for (i in min.indices) {
        if (min[i] != ints[i]) {
            return true
        }
    }
    return false
}

var allPermutations = mutableListOf<List<Int>>()
fun permutation(prefix: List<Int>, remaining: List<Int>, a: List<Int>) {
    if (remaining.isEmpty()) {
        if (notEquals(a, prefix)) {
            allPermutations.add(prefix)
        }
        return
    }
    for (i in remaining.indices) {
        val newPrefix = prefix + remaining[i]
        val newRemaining = remaining.subList(0, i) + remaining.subList(i + 1, remaining.size)
        permutation(newPrefix, newRemaining, a)
    }
}
