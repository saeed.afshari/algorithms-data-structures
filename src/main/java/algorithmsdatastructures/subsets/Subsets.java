package algorithmsdatastructures.subsets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Subsets {

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(reader.readLine());
            for (int i = 0; i < numberOfTests; i++) {
                var aList = Arrays.stream(reader.readLine().split(" ")).collect(Collectors.toList());

                List<List<String>> result = createSubsets(aList);
                for (var l : result) {
                    System.out.println(String.join(",", l));
                }
            }
        }
    }

    private static List<List<String>> createSubsets(List<String> aList) {
        var result = new ArrayList<List<String>>();
        for (int i = 0; i < aList.size(); i++) {
            for (int j = i; j < aList.size(); j++) {
                var a = new ArrayList<String>();
                for (int k = i; k < j + 1; k++) {
                    a.add(aList.get(k));
                }
                result.add(a);
            }
        }
        return result;
    }
}
