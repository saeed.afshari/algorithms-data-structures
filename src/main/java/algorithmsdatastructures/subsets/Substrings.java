package algorithmsdatastructures.subsets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Substrings {

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(reader.readLine());
            for (int i = 0; i < numberOfTests; i++) {
                var str = reader.readLine();

                var result = createSubstringsRecursive(0, str);
                System.out.println("$$$$$$$$" + result.size());
                for (var l : result) {
                    System.out.println(l);
                }
            }
        }
    }

    private static Set<String> createSubstrings(String str) {
        var size = str.length();
        var result = new HashSet<String>();
        for (int i = 0; i < Math.pow(2, size); i++) {
            String s = Integer.toString(i, 2);
            int charIndex = 0;
            var sb = new StringBuilder();
            for (int j = s.length() - 1; j >= 0; j--) {
                if (s.charAt(j) == '1') {
                    sb.append(str.charAt(charIndex));
                }
                charIndex++;
            }
            String newItem = sb.toString();
            result.add(newItem);
            result.addAll(calculatePermutations("", newItem, new ArrayList<>()));
        }
        return result;
    }

    private static List<String> createSubstringsRecursive(int index, String str) {
        if(index >= str.length()) return List.of("");

        var currentCharacter = str.charAt(index);
        var answer = new ArrayList<String>();
        answer.add(String.valueOf(currentCharacter));
        for(int i = index + 1; i < str.length(); i++){
            for(var subString: createSubstringsRecursive(index + 1, str)){
                answer.add(currentCharacter + subString);
            }
        }
        return answer;
    }

    // 012 max = 10, item=[banana,cucumber,apricot],weight=[5,6,4],value=[10,5,10]
    private static List<String> calculatePermutations(String prefix, String full, List<String> permutations){
        if(full.length() == 0){
            permutations.add(prefix);
            return permutations;
        }
        for (int i = 0; i < full.length(); i++){
            var newPrefix = prefix + full.charAt(i);
            var newRemaining = full.substring(0, i) + full.substring(i+1);
            calculatePermutations(newPrefix, newRemaining, permutations);
        }
        return permutations;
    }
}
