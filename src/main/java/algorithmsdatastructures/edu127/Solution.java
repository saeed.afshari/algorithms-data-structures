package algorithmsdatastructures.edu127;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int numberOfTests = Integer.parseInt(reader.readLine());
            for (int i = 0; i < numberOfTests; i++) {
                var budget = Integer.parseInt(reader.readLine().split(" ")[1]);
                var shopsInitialPrice = Arrays.stream(reader.readLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());

                System.out.println(howManyPacksCanIBuy(budget, shopsInitialPrice));
            }
        }
    }

    private static long howManyPacksCanIBuy(Integer budget, List<Integer> shopsInitialPrice) {
        shopsInitialPrice.sort(Comparator.comparing(a -> a, Integer::compareTo));
        long result = 0;
        long sum = 0;
        for (int i = 0; i < shopsInitialPrice.size(); i++) {
            sum += shopsInitialPrice.get(i);
            if (sum <= budget) {
                long index = i + 1;
                long val = ((budget - sum) / index) + 1;
                result += val;
            }
        }
        return result;
    }
}
