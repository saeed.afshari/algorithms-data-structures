package algorithmsdatastructures.leetcode.easy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TwoSum {

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, List<Integer>> numsMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            var value = numsMap.getOrDefault(nums[i], new ArrayList<>());
            value.add(i);
            numsMap.put(nums[i], value);
        }
        int[] result = new int[2];
        for (int i = 0; i < nums.length; i++) {
            var delta = target - nums[i];
            if (numsMap.containsKey(delta)) {
                if (delta == nums[i]) {
                    if (numsMap.get(delta).size() > 1) {
                        result[0] = i;
                        result[1] = numsMap.get(delta).get(1);
                    }
                } else {
                    result[0] = i;
                    result[1] = numsMap.get(delta).get(0);
                }
            }
        }
        return result;
    }
}
