package algorithmsdatastructures.leetcode.easy;

// https://leetcode.com/problems/add-two-numbers/
public class AddTwo {

    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public static void main(String[] args) {
        var l1 = new ListNode();
        l1.val = 2;
        l1.next = new ListNode();
        l1.next.val = 4;
        l1.next.next = new ListNode();
        l1.next.next.val = 9;

        var l2 = new ListNode();
        l2.val = 5;
        l2.next = new ListNode();
        l2.next.val = 6;
        l2.next.next = new ListNode();
        l2.next.next.val = 4;
        l2.next.next.next = new ListNode();
        l2.next.next.next.val = 9;


        ListNode listNode = addTwoNumbers(l1, l2);
        var sb = new StringBuilder();
        while (listNode != null) {
            sb.append(listNode.val).append("->");
            listNode = listNode.next;
        }
        System.out.println("[" + sb + "]");
    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode sumNode = new ListNode();
        var tmp1 = l1;
        var tmp2 = l2;
        var tmp3 = sumNode;
        int previousSumReminder = 0;
        while (tmp1 != null && tmp2 != null) {
            var newValue = (tmp1.val + tmp2.val + previousSumReminder) % 10;
            previousSumReminder = (tmp1.val + tmp2.val + previousSumReminder) / 10;
            tmp3.val = newValue;
            tmp1 = tmp1.next;
            tmp2 = tmp2.next;
            if (tmp1 != null && tmp2 != null) {
                tmp3.next = new ListNode();
                tmp3 = tmp3.next;
            }
        }
        if(tmp1 != null || tmp2 != null || previousSumReminder == 1){
            tmp3.next = new ListNode();
            tmp3 = tmp3.next;
        }
        while (tmp1 != null) {
            tmp3.val = (tmp1.val + previousSumReminder) % 10;
            previousSumReminder = (tmp1.val + previousSumReminder) / 10;
            tmp1 = tmp1.next;
            if (tmp1 != null) {
                tmp3.next = new ListNode();
                tmp3 = tmp3.next;
            } else {
                if (previousSumReminder > 0) {
                    tmp3.next = new ListNode();
                    tmp3.next.val = previousSumReminder;
                    previousSumReminder = 0;
                }
            }
        }
        while (tmp2 != null) {
            tmp3.val = (tmp2.val + previousSumReminder) % 10;
            previousSumReminder = (tmp2.val + previousSumReminder) / 10;
            tmp2 = tmp2.next;
            if (tmp2 != null) {
                tmp3.next = new ListNode();
                tmp3 = tmp3.next;
            } else {
                if (previousSumReminder > 0) {
                    tmp3.next = new ListNode();
                    tmp3.next.val = previousSumReminder;
                    previousSumReminder = 0;
                }
            }
        }
        if(previousSumReminder == 1){
            tmp3.val = previousSumReminder;
        }
        return sumNode;
    }
}
