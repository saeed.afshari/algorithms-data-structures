package algorithmsdatastructures.binarysearch

class BinarySearch {

    fun findIndex(source: List<Int>, item: Int): Int {
        var (l, r) = 0 to source.size
        while (l < r) {
            val mid = l + (r - l) / 2
            if (item == source[mid]) {
                return mid
            } else if (item < source[mid]) {
                r = mid - 1
            } else {
                l = mid + 1
            }
        }
        return -1
    }

    fun findIndexLowerBound(source: List<Int>, item: Int): Int {
        var (l, r) = 0 to source.size
        while (l < r) {
            val mid = l + (r - l) / 2
            if (item <= source[mid]) {
                r = mid
            } else {
                l = mid + 1
            }
        }
        return l
    }

    fun findIndexUpperBound(source: List<Int>, item: Int): Int {
        var (l, r) = 0 to source.size
        while (l < r) {
            val mid = l + (r - l) / 2
            if (item < source[mid]) {
                r = mid
            } else {
                l = mid + 1
            }
        }
        return if (source[l - 1] != item) -1 else l
    }
}
