package algorithmsdatastructures.datastructure;

import java.util.ArrayList;
import java.util.List;

public class TreeNode<T extends Comparable<T>> {

    public TreeNode<T> left;
    public TreeNode<T> right;
    public T value;

    TreeNode(T value) {
        this.value = value;
    }

    TreeNode() {
    }

    public void insert(T newValue) {
        if (value.compareTo(newValue) > 0) {
            if (left == null) {
                left = new TreeNode<>(newValue);
            } else {
                left.insert(newValue);
            }
        } else {
            if (right == null) {
                right = new TreeNode<>(newValue);
            } else {
                right.insert(newValue);
            }
        }
    }

    public boolean contains(T newValue) {
        if (value.compareTo(newValue) == 0) {
            return true;
        } else if (value.compareTo(newValue) > 0) {
            if (left == null) {
                return false;
            } else {
                return left.contains(newValue);
            }
        } else {
            if (right == null) {
                return false;
            } else {
                return right.contains(newValue);
            }
        }
    }

    public TreeNode<T> parseBalancedTree(T[] sortedArray) {
        return toBalancedTree(sortedArray, 0, sortedArray.length - 1);
    }

    private TreeNode<T> toBalancedTree(T[] sortedArray, int start, int end) {
        if (start > end) return null;
        int mid = (start + end) / 2;
        TreeNode<T> node = new TreeNode<>(sortedArray[mid]);
        node.left = toBalancedTree(sortedArray, start, mid - 1);
        node.right = toBalancedTree(sortedArray, mid + 1, end);
        return node;
    }

    public List<T> preOrder() {
        List<T> result = new ArrayList<>();
        preOrderRecursive(result);
        return result;
    }

    private void preOrderRecursive(List<T> result) {
        result.add(value);
        if (left != null) {
            left.preOrderRecursive(result);
        }
        if (right != null) {
            right.preOrderRecursive(result);
        }
    }

    public List<T> inOrder() {
        List<T> result = new ArrayList<>();
        inOrderRecursive(result);
        return result;
    }

    private void inOrderRecursive(List<T> result) {
        if (left != null) {
            left.inOrderRecursive(result);
        }
        result.add(value);
        if (right != null) {
            right.inOrderRecursive(result);
        }
    }

    public List<T> postOrder() {
        List<T> result = new ArrayList<>();
        postOrderRecursive(result);
        return result;
    }

    private void postOrderRecursive(List<T> result) {
        if (left != null) {
            left.postOrderRecursive(result);
        }
        if (right != null) {
            right.postOrderRecursive(result);
        }
        result.add(value);
    }
}
