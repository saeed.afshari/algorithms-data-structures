package algorithmsdatastructures.datastructure;

public class Node<T> {

    public Node<T> prev;
    public Node<T> next;
    public T value;

    Node(Node<T> prev, Node<T> next, T value) {

        this.prev = prev;
        this.next = next;
        this.value = value;
    }
}
