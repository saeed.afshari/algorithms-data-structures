package algorithmsdatastructures.datastructure;

public class MyLinkedList<T> {

    private Node<T> head;
    private Node<T> tail;
    private int size = 0;

    void add(T nextValue) {
        var node = new Node<>(null, null, nextValue);
        if (head == null) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            node.prev = tail;
            tail = node;
        }
        size++;
    }

    Node<T> get(T value) {
        var tmp = head;
        while (tmp != null) {
            if (tmp.value == value) return tmp;
            tmp = tmp.next;
        }
        return null;
    }

    void remove(T value) {
        var node = get(value);
        if (node == null) return;
        if (node.prev != null)
            node.prev.next = node.next;
        if (node.next != null)
            node.next.prev = node.prev;
        node = null;
    }

    void reverse() {
        if (head == null) return;
        var headTmp = head;
        var tailTmp = tail;
        while (headTmp != tailTmp) {
            var tmp = headTmp.value;
            headTmp.value = tailTmp.value;
            tailTmp.value = tmp;
            headTmp = headTmp.next;
            tailTmp = tailTmp.next;
            if (head.prev == tailTmp) break;
        }
    }

     static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    public ListNode swapPairs(ListNode head) {
        if(head == null || head.next == null) return head;
        var tmp1 = head;
        var front = head.next;
        var tmp2 = head.next;
        while(tmp1 != null && tmp2 != null){
            var rest = tmp2.next;
            tmp2.next = tmp1;
            tmp1 = rest;
        }
        return head;
    }

}

