package algorithmsdatastructures.googlekickstart1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

class Solution {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        var numberOfTests = scanner.nextInt();
        scanner.nextLine();
        var tests = new ArrayList<Test>(numberOfTests);
        for (var i = 0; i < numberOfTests; i++) {
            String[] firstLine = scanner.nextLine().split(" ");
            int kids = Integer.parseInt(firstLine[1]);
            String[] secondLine = scanner.nextLine().split(" ");
            tests.add(new Test(kids, Arrays.stream(secondLine).map(Integer::parseInt).collect(Collectors.toList())));
        }

        List<Integer> result = distributeCandies(tests);
        var sb = new StringBuilder();
        for (int i = 1; i <= result.size(); i++) {
            sb.append("Case #" + i + ": " + result.get(i - 1) + "\n");
        }
        System.out.println(sb);
    }

    private static List<Integer> distributeCandies(ArrayList<Test> tests) {
        return tests.stream().map(it -> it.candies.stream().reduce(Integer::sum).orElse(0) % it.kids)
                .collect(Collectors.toList());
    }

    private static class Test {
        private final Integer kids;
        private final List<Integer> candies;

        Test(Integer kids, List<Integer> candies) {
            this.kids = kids;
            this.candies = candies;
        }
    }
}
