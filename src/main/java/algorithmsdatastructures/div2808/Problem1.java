package algorithmsdatastructures.div2808;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem1 {
    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var splitted = br.readLine().split(" ");
                Integer n = Integer.parseInt(splitted[0]);
                Integer q = Integer.parseInt(splitted[1]);
                splitted = br.readLine().split(" ");
                int[] difficulties = new int[n + 1];
                String[][] dp = new String[difficulties.length + 1][q + 10];
                int sum = 0;
                for (int i = 0; i < n; i++) {
                    difficulties[i + 1] = Integer.parseInt(splitted[i]);
                    sum += difficulties[i + 1];
                }
                if (q >= n) {
                    for (int i = 0; i < n; i++) {
                        sb.append("1");
                    }
                    sb.append("\n");
                } else {
                    sb.append(knapsack(1, "", q, difficulties, dp) + "\n");
                }
            }
            System.out.println(sb);
        }

    }

    private static String knapsack(int day, String numberOfTests, int q, int[] diff, String[][] dp) {
        if (day >= diff.length) return "";

        if (dp[day][q] != null) return dp[day][q];

        String takeCost1 = "";
        String takeCost2 = "";
        String skipCost = "";
        if (diff[day] <= q) {
            takeCost1 = 1 + knapsack(day + 1, numberOfTests + 1, q, diff, dp);
        } else if (q > 0) {
            takeCost2 = 1 + knapsack(day + 1, numberOfTests + 1, q - 1, diff, dp);
        }
        skipCost = 0 + knapsack(day + 1, numberOfTests, q, diff, dp);
        int numberOfOneTake1 = takeCost1.equals("") ? -1 : Long.bitCount(Long.parseLong(takeCost1, 2));
        int numberOfOneTake2 = takeCost2.equals("") ? -1 : Long.bitCount(Long.parseLong(takeCost2, 2));
        int numberOfOneSkip = skipCost.equals("") ? -1 : Long.bitCount(Long.parseLong(skipCost, 2));
        var takeCost = Long.max(numberOfOneTake1, numberOfOneTake2);
        if (takeCost >= numberOfOneSkip) {
            if (takeCost == numberOfOneTake1) {
                dp[day][q] = takeCost1;
                return takeCost1;
            } else {
                dp[day][q] = takeCost2;
                return takeCost2;
            }
        } else {
            dp[day][q] = skipCost;
            return skipCost;
        }
    }

}
