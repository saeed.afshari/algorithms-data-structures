package algorithmsdatastructures.div2808;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem3 {
    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                Integer n = Integer.parseInt(br.readLine());

                sb.append("\n");
            }

            System.out.println(sb);
        }
    }
}
