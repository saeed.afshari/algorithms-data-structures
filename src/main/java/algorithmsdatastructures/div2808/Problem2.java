package algorithmsdatastructures.div2808;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Problem2 {
    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sbGlobal = new StringBuilder();
            while (numberOfTests > 0) {
                numberOfTests--;
                var splitted = br.readLine().split(" ");
                int n = Integer.parseInt(splitted[0]);
                int l = Integer.parseInt(splitted[1]);
                int r = Integer.parseInt(splitted[2]);
                boolean possible = true;
                var sb = new StringBuilder();
                for(int i = 1; i<=n; i++){
                    var rem = l % i;
                    if(rem == 0){
                        sb.append(l + " ");
                    } else if(l + (i - rem) <= r){
                        sb.append((l + (i - rem)) + " ");
                    } else{
                        possible = false;
                        break;
                    }
                }
                if(!possible){
                    sb = new StringBuilder();
                    sb.append("NO\n");
                } else{
                    sb.insert(0, "YES\n");
                    sb.append("\n");
                }
                sbGlobal.append(sb);
            }
            System.out.println(sbGlobal);
        }

    }

    private static String knapsack(int day, String numberOfTests, int q, int[] diff, String[][] dp) {
        if (day >= diff.length) return "";

        if (dp[day][q] != null) return dp[day][q];

        String takeCost1 = "";
        String takeCost2 = "";
        String skipCost = "";
        if (diff[day] <= q) {
            takeCost1 = 1 + knapsack(day + 1, numberOfTests + 1, q, diff, dp);
        } else if (q > 0) {
            takeCost2 = 1 + knapsack(day + 1, numberOfTests + 1, q - 1, diff, dp);
        }
        skipCost = 0 + knapsack(day + 1, numberOfTests, q, diff, dp);
        int numberOfOneTake1 = takeCost1.equals("") ? -1 : Long.bitCount(Long.parseLong(takeCost1, 2));
        int numberOfOneTake2 = takeCost2.equals("") ? -1 : Long.bitCount(Long.parseLong(takeCost2, 2));
        int numberOfOneSkip = skipCost.equals("") ? -1 : Long.bitCount(Long.parseLong(skipCost, 2));
        var takeCost = Long.max(numberOfOneTake1, numberOfOneTake2);
        if (takeCost >= numberOfOneSkip) {
            if (takeCost == numberOfOneTake1) {
                dp[day][q] = takeCost1;
                return takeCost1;
            } else {
                dp[day][q] = takeCost2;
                return takeCost2;
            }
        } else {
            dp[day][q] = skipCost;
            return skipCost;
        }
    }

}
