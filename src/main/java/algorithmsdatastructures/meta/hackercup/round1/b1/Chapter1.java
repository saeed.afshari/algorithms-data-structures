package algorithmsdatastructures.meta.hackercup.round1.b1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Chapter1 {

    private static class Point {
        private final int x;
        private final int y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public static void main(String[] args) throws IOException {
        List<String> lines = Files.readAllLines(Path.of("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/java/algorithmsdatastructures/meta/hackercup/round1/b1/watering_well_chapter_1_input.txt"));

        var numberOfTests = Integer.parseInt(lines.get(0));
        var sb = new StringBuilder();
        int caseNumber = 0;
        int lineNumber = 1;
        while (numberOfTests > 0) {
            caseNumber++;

            long n = Long.parseLong(lines.get(lineNumber));
            lineNumber++;

            var trees = new ArrayList<Point>();
            for (int i = 0; i < n; i++) {
                var splitted = lines.get(lineNumber).split(" ");
                lineNumber++;
                trees.add(new Point(Integer.parseInt(splitted[0]), Integer.parseInt(splitted[1])));
            }

            long q = Long.parseLong(lines.get(lineNumber));
            lineNumber++;
            var wells = new ArrayList<Point>();
            for (int i = 0; i < q; i++) {
                var splitted = lines.get(lineNumber).split(" ");
                lineNumber++;
                wells.add(new Point(Integer.parseInt(splitted[0]), Integer.parseInt(splitted[1])));
            }


            long sum = 0L;
            for (int i = 0; i < q; i++) {
                Point p = wells.get(i);
                long sumDist = 0L;
                for (int j = 0; j < n; j++) {
                    Point t = trees.get(j);
                    if (p.x == t.x && p.y != t.y) {
                        sumDist += Math.abs(((long) p.y * p.y) + ((long) t.y * t.y) - (2L * Math.abs(p.y) * Math.abs(t.y)));
                    } else if (p.x != t.x && p.y == t.y) {
                        sumDist += Math.abs(((long) p.x * p.x) + ((long) t.x * t.x) - (2L * Math.abs(p.x) * Math.abs(t.x)));
                    } else if (p.x != t.x && p.y != t.y){
                        sumDist += (long) Math.abs(p.x - t.x) * Math.abs(p.x - t.x) + (long) Math.abs(p.y - t.y) * Math.abs(p.y - t.y);
                    }
                }
                sum += sumDist;
            }
            sum%=1000000007;

            sb.append(String.format("Case #%d: %d\n", caseNumber, sum));

            numberOfTests--;
        }

        Files.writeString(Path.of("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/java/algorithmsdatastructures/meta/hackercup/round1/b1/chapter1-output2.txt"), sb.toString());
    }
}
