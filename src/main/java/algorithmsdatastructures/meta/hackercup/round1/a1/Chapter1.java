package algorithmsdatastructures.meta.hackercup.round1.a1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Chapter1 {

    public static void main(String[] args) throws IOException {
        List<String> lines = Files.readAllLines(Path.of("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/java/algorithmsdatastructures/meta/hackercup/round1/a1/consecutive_cuts_chapter_1_input.txt"));

        var numberOfTests = Integer.parseInt(lines.get(0));
        var sb = new StringBuilder();
        int caseNumber = 0;
        int lineNumber = 1;
        while (numberOfTests > 0) {
            caseNumber++;

            String[] splitted = lines.get(lineNumber).split(" ");
            lineNumber++;
            var n = Integer.parseInt(splitted[0]);
            var k = Integer.parseInt(splitted[1]);

            splitted = lines.get(lineNumber).split(" ");
            lineNumber++;
            var a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(splitted[i]);
            }
            splitted = lines.get(lineNumber).split(" ");
            lineNumber++;
            var b = new int[n];
            for (int i = 0; i < n; i++) {
                b[i] = Integer.parseInt(splitted[i]);
            }

            int j = 0;
            int i = 0;
            boolean possible = true;
            for (; i < n; i++) {
                if (a[i] != b[j]) {
                    continue;
                }
                break;
            }
            int index = 0;
            for (int z = i; z < n; z++) {
                if (a[z] == b[index]) {
                    index++;
                    continue;
                }
                possible = false;
            }
            int aIndex = 0;
            for (int z = index; z < n; z++) {
                if (a[aIndex] == b[z]) {
                    aIndex++;
                    continue;
                }
                possible = false;
            }
            if (!possible) {
                sb.append(String.format("Case #%d: NO\n", caseNumber));
            } else if (a[0] != b[0] && k % 2 == 0 && n == 2) {
                sb.append(String.format("Case #%d: NO\n", caseNumber));
            } else if (a[0] == b[0] && k == 1) {
                sb.append(String.format("Case #%d: NO\n", caseNumber));
            } else if (a[0] == b[0]) {
                sb.append(String.format("Case #%d: YES\n", caseNumber));
            } else if (k == 0) {
                sb.append(String.format("Case #%d: NO\n", caseNumber));
            } else
                sb.append(String.format("Case #%d: YES\n", caseNumber));

            numberOfTests--;
        }

        Files.writeString(Path.of("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/java/algorithmsdatastructures/meta/hackercup/round1/a1/chapter1-output2.txt"), sb.toString());
    }
}
