package algorithmsdatastructures.meta.hackercup.round1.a1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Chapter2 {

    public static void main(String[] args) throws IOException {
        List<String> lines = Files.readAllLines(Path.of("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/java/algorithmsdatastructures/meta/hackercup/round1/a1/consecutive_cuts_chapter_2_input.txt"));

        var numberOfTests = Integer.parseInt(lines.get(0));
        var sb = new StringBuilder();
        int caseNumber = 0;
        int lineNumber = 1;
        while (numberOfTests > 0) {
            caseNumber++;

            String[] splitted = lines.get(lineNumber).split(" ");
            lineNumber++;
            var n = Integer.parseInt(splitted[0]);
            var k = Integer.parseInt(splitted[1]);

            splitted = lines.get(lineNumber).split(" ");
            lineNumber++;
            var a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(splitted[i]);
            }
            splitted = lines.get(lineNumber).split(" ");
            lineNumber++;
            var b = new int[n];
            for (int i = 0; i < n; i++) {
                b[i] = Integer.parseInt(splitted[i]);
            }

            List<Integer> possibleBreaks = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                if (a[i] != b[0]) {
                    continue;
                }
                possibleBreaks.add(i);
            }
            Map<Integer, Integer> deltaa = new HashMap<>();
            for (int i = 0; i < n; i++) {
                Integer value = deltaa.getOrDefault(a[i], 0);
                value++;
                deltaa.put(a[i], value);
            }
            Map<Integer, Integer> delta = new HashMap<>();
            for (int i = 0; i < n; i++) {
                Integer value = delta.getOrDefault(b[i], 0);
                value++;
                delta.put(b[i], value);
            }
            var possible = isPossible(n, a, b, possibleBreaks, deltaa, delta);
            if (!possible) {
                sb.append(String.format("Case #%d: NO\n", caseNumber));
            } else if (a[0] != b[0] && k % 2 == 0 && n == 2) {
                sb.append(String.format("Case #%d: NO\n", caseNumber));
            } else if (a[0] == b[0] && k == 1) {
                sb.append(String.format("Case #%d: NO\n", caseNumber));
            } else if (a[0] == b[0]) {
                sb.append(String.format("Case #%d: YES\n", caseNumber));
            } else if (k == 0) {
                sb.append(String.format("Case #%d: NO\n", caseNumber));
            } else
                sb.append(String.format("Case #%d: YES\n", caseNumber));

            numberOfTests--;
        }

        Files.writeString(Path.of("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/java/algorithmsdatastructures/meta/hackercup/round1/a1/chapter2-output2.txt"), sb.toString());
    }

    private static boolean isPossible(int n, int[] a, int[] b, List<Integer> possibleBreaks, Map<Integer, Integer> deltaa, Map<Integer, Integer> deltab) {
        if (deltab.get(b[0]) != possibleBreaks.size()) return false;
        for (int i = 0; i < a.length; i++) {
            if (!deltab.containsKey(a[i])) return false;
            if(deltaa.get(a[i]) != deltab.get(a[i])) return false;
        }
        boolean possible = true;
        if (possibleBreaks.size() != a.length) {
            for (int w = 0; w < possibleBreaks.size(); w++) {
                possible = true;
                int index = 0;
                for (int z = possibleBreaks.get(w); z < n; z++) {
                    if (a[z] == b[index]) {
                        index++;
                        continue;
                    }
                    possible = false;
                }
                int aIndex = 0;
                for (int z = index; z < n; z++) {
                    if (a[aIndex] == b[z]) {
                        aIndex++;
                        continue;
                    }
                    possible = false;
                }
                if (possible) break;
            }
        }
        return possible;
    }
}
