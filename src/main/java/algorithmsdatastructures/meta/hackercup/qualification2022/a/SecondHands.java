package algorithmsdatastructures.meta.hackercup.qualification2022.a;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class SecondHands {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            int caseNumber = 0;
            while (numberOfTests > 0) {
                caseNumber++;

                String[] splitted = br.readLine().split(" ");
                var n = Integer.parseInt(splitted[0]);
                var k = Integer.parseInt(splitted[1]);

                var styles = br.readLine().split(" ");

                if (styles.length > 2 * k) {
                    sb.append(String.format("Case #%d: NO\n", caseNumber));
                } else {
                    var aMap = new HashMap<String, Integer>();
                    int max = 0;
                    for (int i = 0; i < styles.length; i++) {
                        String theStyle = styles[i];
                        var value = aMap.getOrDefault(theStyle, 0);
                        value++;
                        max = Integer.max(max, value);
                        aMap.put(theStyle, value);
                    }
                    if (max > 2) {
                        sb.append(String.format("Case #%d: NO\n", caseNumber));
                    } else {
                        sb.append(String.format("Case #%d: YES\n", caseNumber));
                    }
                }

                numberOfTests--;
            }

            System.out.println(sb);
        }
    }
}
