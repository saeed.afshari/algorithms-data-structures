package algorithmsdatastructures.meta.hackercup.qualification2022.b1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SecondFriend {

    public static void main(String[] args) throws IOException {
        try (var br = new BufferedReader(new InputStreamReader(System.in))) {
            var numberOfTests = Integer.parseInt(br.readLine());
            var sb = new StringBuilder();
            int caseNumber = 0;
            while (numberOfTests > 0) {
                caseNumber++;

                String[] splitted = br.readLine().split(" ");
                var r = Integer.parseInt(splitted[0]);
                var c = Integer.parseInt(splitted[1]);
                if (r == 1 || c == 1) {
                    boolean found = false;
                    for (int i = 0; i < r; i++) {
                        if (br.readLine().contains("^")) {
                            found = true;
                        }
                    }
                    if (found) {
                        sb.append(String.format("Case #%d: Impossible\n", caseNumber));
                    } else {
                        sb.append(String.format("Case #%d: Possible\n", caseNumber));
                        for (int i = 0; i < r; i++) {
                            for (int j = 0; j < c; j++) {
                                sb.append('.');
                            }
                            sb.append("\n");
                        }
                    }
                } else {
                    for (int i = 0; i < r; i++) {
                        br.readLine();
                    }
                    sb.append(String.format("Case #%d: Possible\n", caseNumber));
                    for (int i = 0; i < r; i++) {
                        for (int j = 0; j < c; j++) {
                            sb.append('^');
                        }
                        sb.append("\n");
                    }
                }

                numberOfTests--;
            }

            System.out.println(sb);
        }
    }
}
