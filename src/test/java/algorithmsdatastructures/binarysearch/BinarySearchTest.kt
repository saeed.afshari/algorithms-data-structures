package algorithmsdatastructures.binarysearch

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class BinarySearchTest {

    @Test
    fun `findIndex 1`() {
        val binarySearch = BinarySearch()

        val index = binarySearch.findIndex(listOf(1, 2, 3, 3), 3)

        assertEquals(2, index)
    }

    @Test
    fun `findIndex 2`() {
        val binarySearch = BinarySearch()

        val index = binarySearch.findIndex(listOf(1, 2, 3, 3), 0)

        assertEquals(-1, index)
    }

    @Test
    fun `findIndex lower bound`() {
        val binarySearch = BinarySearch()

        val index = binarySearch.findIndexLowerBound(listOf(1, 2, 3, 3), 3)

        assertEquals(2, index)
    }

    @Test
    fun `findIndex upper bound`() {
        val binarySearch = BinarySearch()

        val index = binarySearch.findIndexUpperBound(listOf(1, 2, 3, 3), 3)

        assertEquals(4, index)
    }

    @Test
    fun `findIndex upper bound 2`() {
        val binarySearch = BinarySearch()

        val index = binarySearch.findIndexUpperBound(listOf(1, 2, 3, 3), 4)

        assertEquals(-1, index)
    }
}
