package algorithmsdatastructures.getanevenstring;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GetAnEvenStringTest {

    @Test
    void test1() {
        List<Integer> result = GetAnEvenString.findTheMinimumNumberOfCharactersToBeRemoved(List.of("aabbdabdccc"));

        assertEquals(3, result.get(0));
    }

    @Test
    void test2() {
        List<Integer> result = GetAnEvenString.findTheMinimumNumberOfCharactersToBeRemoved(List.of("zyx"));

        assertEquals(3, result.get(0));
    }

    @Test
    void test3() {
        List<Integer> result = GetAnEvenString.findTheMinimumNumberOfCharactersToBeRemoved(List.of("aaababbb"));

        assertEquals(2, result.get(0));
    }

    @Test
    void test4() {
        List<Integer> result = GetAnEvenString.findTheMinimumNumberOfCharactersToBeRemoved(List.of("aabbcc"));

        assertEquals(0, result.get(0));
    }

    @Test
    void test5() {
        List<Integer> result = GetAnEvenString.findTheMinimumNumberOfCharactersToBeRemoved(List.of("oaoaaaoo"));

        assertEquals(2, result.get(0));
    }

    @Test
    void test6() {
        List<Integer> result = GetAnEvenString.findTheMinimumNumberOfCharactersToBeRemoved(List.of("bmefbmuyw"));

        assertEquals(7, result.get(0));
    }

}
