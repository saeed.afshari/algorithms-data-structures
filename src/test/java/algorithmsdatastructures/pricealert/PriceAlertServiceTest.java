package algorithmsdatastructures.pricealert;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PriceAlertServiceTest {

    @Test
    void returns_correct_results_when_calls_PriceAlertService_popTargetPrices() {
        PriceAlertService priceAlertService = new PriceAlertService();
        PriceAlertDto priceAlertDto1 = new PriceAlertDto("", PriceGrowthDirection.GROWING, BigDecimal.ONE);
        priceAlertService.addNewTargetPrice(priceAlertDto1);
        PriceAlertDto priceAlertDto10 = new PriceAlertDto("", PriceGrowthDirection.GROWING, BigDecimal.TEN);
        priceAlertService.addNewTargetPrice(priceAlertDto10);

        List<PriceAlertDto> priceAlertDtoList = priceAlertService.popTargetPrices(BigDecimal.ONE.add(BigDecimal.ONE));

        assertEquals(List.of(priceAlertDto1), priceAlertDtoList);
    }

    @Test
    void returns_correct_results_when_calls_PriceAlertService_popTargetPrices_price_dropping_case() {
        PriceAlertService priceAlertService = new PriceAlertService();
        PriceAlertDto priceAlertDto1 = new PriceAlertDto("", PriceGrowthDirection.DROPPING, BigDecimal.ONE);
        priceAlertService.addNewTargetPrice(priceAlertDto1);
        PriceAlertDto priceAlertDto10 = new PriceAlertDto("", PriceGrowthDirection.DROPPING, BigDecimal.TEN);
        priceAlertService.addNewTargetPrice(priceAlertDto10);

        List<PriceAlertDto> priceAlertDtoList = priceAlertService.popTargetPrices(BigDecimal.ONE.add(BigDecimal.ONE));

        assertEquals(List.of(priceAlertDto10), priceAlertDtoList);
    }

    @Test
    void returns_correct_results_when_calls_PriceAlertService_popTargetPrices_mixed_case() {
        PriceAlertService priceAlertService = new PriceAlertService();
        PriceAlertDto priceAlertDto1 = new PriceAlertDto("", PriceGrowthDirection.GROWING, BigDecimal.ONE);
        priceAlertService.addNewTargetPrice(priceAlertDto1);
        PriceAlertDto priceAlertDto10 = new PriceAlertDto("", PriceGrowthDirection.DROPPING, BigDecimal.TEN);
        priceAlertService.addNewTargetPrice(priceAlertDto10);

        List<PriceAlertDto> priceAlertDtoList = priceAlertService.popTargetPrices(BigDecimal.ONE.add(BigDecimal.ONE));

        assertEquals(List.of(priceAlertDto1, priceAlertDto10), priceAlertDtoList);
    }

    @Test
    void returns_correct_results_worst_case_huge_input() {
        PriceAlertService priceAlertService = new PriceAlertService();
        for (var i = 0L; i < 1000000L; i++) {
            PriceAlertDto priceAlertDto = new PriceAlertDto("", PriceGrowthDirection.GROWING, BigDecimal.valueOf(i));
            priceAlertService.addNewTargetPrice(priceAlertDto);

            PriceAlertDto priceAlertDto1 = new PriceAlertDto("", PriceGrowthDirection.DROPPING, BigDecimal.valueOf(i));
            priceAlertService.addNewTargetPrice(priceAlertDto1);
        }

        List<PriceAlertDto> priceAlertDtoList = priceAlertService.popTargetPrices(BigDecimal.ONE.add(BigDecimal.ONE));

        assertEquals(1000001, priceAlertDtoList.size());
    }

}
