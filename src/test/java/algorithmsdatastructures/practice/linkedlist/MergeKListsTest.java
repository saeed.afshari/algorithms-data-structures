package algorithmsdatastructures.practice.linkedlist;

import org.junit.jupiter.api.Test;

class MergeKListsTest {
    @Test
    void swap() {
        var node = new MergeKLists.ListNode(1);
        node.next = new MergeKLists.ListNode(2);
        node.next.next = new MergeKLists.ListNode(3);
        node.next.next.next = new MergeKLists.ListNode(4);

        var node2 = new MergeKLists.ListNode(1);
        node2.next = new MergeKLists.ListNode(2);
        node2.next.next = new MergeKLists.ListNode(3);
        node2.next.next.next = new MergeKLists.ListNode(4);

        MergeKLists.ListNode listNode = new MergeKLists().mergeKLists(new MergeKLists.ListNode[]{node, node2});
        listNode.toString();
    }
}
