package algorithmsdatastructures.datastructure;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TreeNodeTest {

    @Test
    void parseBalancedTreeWorksCorrectlyPreOrder() {
        Integer[] sortedArray = new Integer[]{1, 2, 3, 4, 5, 6};
        TreeNode<Integer> treeNode = new TreeNode<Integer>().parseBalancedTree(sortedArray);

        String actual = String.join(",", treeNode.preOrder().stream().map(String::valueOf).toList());

        assertEquals("3,1,2,5,4,6", actual);
    }

    @Test
    void parseBalancedTreeWorksCorrectlyPostOrder() {
        Integer[] sortedArray = new Integer[]{1, 2, 3, 4, 5, 6};
        TreeNode<Integer> treeNode = new TreeNode<Integer>().parseBalancedTree(sortedArray);

        String actual = String.join(",", treeNode.postOrder().stream().map(String::valueOf).toList());

        assertEquals("2,1,4,6,5,3", actual);
    }

    @Test
    void parseBalancedTreeWorksCorrectlyInOrder() {
        Integer[] sortedArray = new Integer[]{1, 2, 3, 4, 5, 6};
        TreeNode<Integer> treeNode = new TreeNode<Integer>().parseBalancedTree(sortedArray);

        String actual = String.join(",", treeNode.inOrder().stream().map(String::valueOf).toList());

        assertEquals("1,2,3,4,5,6", actual);
    }

    @MethodSource
    @ParameterizedTest
    void insertWorksCorrectlyInOrder() {
        Integer[] sortedArray = new Integer[]{1, 2, 3, 4, 5, 6};
        TreeNode<Integer> treeNode = new TreeNode<Integer>().parseBalancedTree(sortedArray);
        treeNode.insert(7);

        String actual = String.join(",", treeNode.inOrder().stream().map(String::valueOf).toList());

        assertEquals("1,2,3,4,5,6,7", actual);
    }
}
