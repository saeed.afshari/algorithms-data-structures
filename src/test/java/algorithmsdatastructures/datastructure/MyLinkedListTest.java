package algorithmsdatastructures.datastructure;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MyLinkedListTest {

    @Test
    void reverse1() {
        var integerMyLinkedList = new MyLinkedList<>();
        integerMyLinkedList.add(1);
        integerMyLinkedList.add(2);
        integerMyLinkedList.add(3);

        integerMyLinkedList.reverse();

        assertEquals(integerMyLinkedList.get(3).next.value, 2);
        assertEquals(integerMyLinkedList.get(2).next.value, 1);
    }

    @Test
    void swap() {
        var integerMyLinkedList = new MyLinkedList.ListNode(1);
        integerMyLinkedList.next = new MyLinkedList.ListNode(2);
        integerMyLinkedList.next.next = new MyLinkedList.ListNode(3);
        integerMyLinkedList.next.next.next = new MyLinkedList.ListNode(4);

        MyLinkedList.ListNode listNode = new MyLinkedList().swapPairs(integerMyLinkedList);
    }
}
