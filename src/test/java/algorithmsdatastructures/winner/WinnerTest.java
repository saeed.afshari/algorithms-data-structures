package algorithmsdatastructures.winner;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WinnerTest {

    @Test
    void example1() {
        String winner = Winner.wins(List.of("mike 3", "andrew 5", "mike 2"));

        assertEquals("andrew", winner);
    }

    @Test
    void example2() {
        String winner = Winner.wins(List.of("andrew 3", "andrew 2", "mike 5"));

        assertEquals("andrew", winner);
    }

    @Test
    void example3() {
        String winner = Winner.wins(List.of("andrew 3", "andrew -3", "mike 1"));

        assertEquals("mike", winner);
    }

    @Test
    void example4() {

        String winner = Winner.wins(List.of("ksjuuerbnlklcfdjeyq 312", "dthjlkrvvbyahttifpdewvyslsh -983", "ksjuuerbnlklcfdjeyq 268", "dthjlkrvvbyahttifpdewvyslsh 788", "ksjuuerbnlklcfdjeyq -79", "ksjuuerbnlklcfdjeyq -593", "ksjuuerbnlklcfdjeyq 734"));

        assertEquals("ksjuuerbnlklcfdjeyq", winner);
    }
}
