package algorithmsdatastructures.google.hashcode.mentorshipandteam;

import algorithmsdatastructures.google.hashcode.mentorshipandteam.contributor.ContributorPoolService;
import algorithmsdatastructures.google.hashcode.mentorshipandteam.input.InputFileParserService;
import algorithmsdatastructures.google.hashcode.mentorshipandteam.output.OutputFileWriterService;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Set;

class PlannerServiceTest {

//    @Test
//    void test() throws IOException {
//        ContributorPoolService contributorPoolService = new ContributorPoolService();
//        PlannerService plannerService = new PlannerService(contributorPoolService);
//        List<ContributorDto> contributors = new ArrayList<>();
//        HashMap<String, Integer> skillsMap = new HashMap<>();
//        skillsMap.put("C++", 2);
//        contributors.add(new ContributorDto("Anna", skillsMap, null));
//        var bobSkillsMap = new HashMap<String, Integer>();
//        bobSkillsMap.put("HTML", 5);
//        bobSkillsMap.put("CSS", 5);
//        contributors.add(new ContributorDto("Bob", bobSkillsMap, null));
//        var mariaSkillsMap = new HashMap<String, Integer>();
//        mariaSkillsMap.put("Python", 3);
//        contributors.add(new ContributorDto("Maria", mariaSkillsMap, null));
//
//        ArrayList<ProjectDto> projects = new ArrayList<>();
//        ArrayList<SkillDto> requiredSkills = new ArrayList<>();
//        requiredSkills.add(new SkillDto("C++", 3));
//        projects.add(new ProjectDto("Logging", requiredSkills, 5, 10, 5, 0));
//        ArrayList<SkillDto> requiredSkills2 = new ArrayList<>();
//        requiredSkills2.add(new SkillDto("HTML", 3));
//        requiredSkills2.add(new SkillDto("C++", 2));
//        projects.add(new ProjectDto("WebServer", requiredSkills2, 7, 10, 7, 0));
//        ArrayList<SkillDto> requiredSkills3 = new ArrayList<>();
//        requiredSkills3.add(new SkillDto("Python", 3));
//        requiredSkills3.add(new SkillDto("HTML", 3));
//        projects.add(new ProjectDto("WebChat", requiredSkills3, 10, 20, 20, 0));
//
//        Set<ProjectContributors> plan = plannerService.plan(new PlanningDataSource(contributors, projects));
//
//        OutputFileWriterService outputFileWriterService = new OutputFileWriterService();
//        String generate = outputFileWriterService.generate(plan, "");
//        System.out.println(generate);
//    }

    @Test
    void test2() throws IOException {
        ContributorPoolService contributorPoolService = new ContributorPoolService();
        PlannerService plannerService = new PlannerService(contributorPoolService);
        InputFileParserService inputFileParserService = new InputFileParserService();
        PlanningDataSource planningDataSource = inputFileParserService.parse("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/a_an_example.in.txt");
        var plan = plannerService.plan(planningDataSource);

        OutputFileWriterService outputFileWriterService = new OutputFileWriterService();
        String generate = outputFileWriterService.generate(plan, "/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/a_an_example.in.txt.out.txt");
        System.out.println(generate);
    }

    @Test
    void test3() throws IOException {
        ContributorPoolService contributorPoolService = new ContributorPoolService();
        PlannerService plannerService = new PlannerService(contributorPoolService);
        InputFileParserService inputFileParserService = new InputFileParserService();
        PlanningDataSource planningDataSource = inputFileParserService.parse("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/b_better_start_small.in.txt");
        var plan = plannerService.plan(planningDataSource);

        OutputFileWriterService outputFileWriterService = new OutputFileWriterService();
        String generate = outputFileWriterService.generate(plan, "/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/b_better_start_small.in.txt.out.txt");
        System.out.println(generate);
    }

    @Test
    void test4() throws IOException {
        ContributorPoolService contributorPoolService = new ContributorPoolService();
        PlannerService plannerService = new PlannerService(contributorPoolService);
        InputFileParserService inputFileParserService = new InputFileParserService();
        PlanningDataSource planningDataSource = inputFileParserService.parse("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/c_collaboration.in.txt");
        var plan = plannerService.plan(planningDataSource);

        OutputFileWriterService outputFileWriterService = new OutputFileWriterService();
        String generate = outputFileWriterService.generate(plan, "/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/c_collaboration.in.txt.out.txt");
        System.out.println(generate);
    }

    @Test
    void test5() throws IOException {
        ContributorPoolService contributorPoolService = new ContributorPoolService();
        PlannerService plannerService = new PlannerService(contributorPoolService);
        InputFileParserService inputFileParserService = new InputFileParserService();
        PlanningDataSource planningDataSource = inputFileParserService.parse("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/d_dense_schedule.in.txt");
        var plan = plannerService.plan(planningDataSource);

        OutputFileWriterService outputFileWriterService = new OutputFileWriterService();
        String generate = outputFileWriterService.generate(plan, "/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/d_dense_schedule.in.txt.out.txt");
        System.out.println(generate);
    }

    @Test
    void test6() throws IOException {
        ContributorPoolService contributorPoolService = new ContributorPoolService();
        PlannerService plannerService = new PlannerService(contributorPoolService);
        InputFileParserService inputFileParserService = new InputFileParserService();
        PlanningDataSource planningDataSource = inputFileParserService.parse("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/e_exceptional_skills.in.txt");
        var plan = plannerService.plan(planningDataSource);

        OutputFileWriterService outputFileWriterService = new OutputFileWriterService();
        String generate = outputFileWriterService.generate(plan, "/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/e_exceptional_skills.in.txt.out.txt");
        System.out.println(generate);
    }

    @Test
    void test7() throws IOException {
        ContributorPoolService contributorPoolService = new ContributorPoolService();
        PlannerService plannerService = new PlannerService(contributorPoolService);
        InputFileParserService inputFileParserService = new InputFileParserService();
        PlanningDataSource planningDataSource = inputFileParserService.parse("/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/f_find_great_mentors.in.txt");
        var plan = plannerService.plan(planningDataSource);

        OutputFileWriterService outputFileWriterService = new OutputFileWriterService();
        String generate = outputFileWriterService.generate(plan, "/Users/saeed/Workspaces/personal/gitlab/public/algorithms-data-structures/src/main/resources/algorithmsdatastructures.google.hashcode.mentorshipandteam/f_find_great_mentors.in.txt.out.txt");
        System.out.println(generate);
    }

}
