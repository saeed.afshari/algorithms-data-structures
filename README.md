# Algorithms Data Structures

## Getting started
It has been a very long time I have been thinking about reviewing Algorithms and Data Structures and improving my problem-solving skills.
In this repository I am going to constantly find great real world challenges and questions and answer them with pieces of code.


##Google interview preparation
- Top to bottom dynamic programming (Knapsack)
- Longest increasing sequence
- 1D range sum
- 2d range sum
- Coin change problem
- Prefix sum ()
- String manipulation [substring, indexOf, split, endswith, startswith, length(), charAt(), toString(x, radix), StringBuilder, sb.reverse]
- Complete search (Brute forth)
  - Selection m of n
  - All subsets problem
  - Permutations
- Greedy
  - [Tasks & People problem](https://codeforces.com/contest/1701/problem/C)
- Binary search
  - lower band
  - upper band
- Divide and conquer
- Recursion
  - Hanoi towers
- Palindromes (two pointers and reverse)
- Merge sort, Selection sort, Insertion sort, bubble sort
- Backtracking
  - Queen chess problem
- DFS (recursive, stack)
- BFS (Queue)
- Tree depth (height)
- Tree traverse
- Inverting tree
- Tree in-degree(inbound), out-degree
- Finding connected components in undirected graphs
- Labeling; coloring connected components
- Topological sort
- Finding strongly connected components
- Minimum spanning trees
  - Kruskal's algorithm
- Single source Shortest path algorithm (BFS) in unweighted graph
- Single source Shortest path algorithm (BFS) in weighted graph
- Math basics
  - Pow (recursive)
  - Fib
  - Factorial
  - GCD
  - Factors
  - Prime numbers
  - Combinations (n!, n!/(n-r!)r!)
  - Mod (plus, minus, mul)
    - plus = a%md + b%md
    - minus = a%md - b%md
    - mul = ((a%md)*b)%md
  - Bitwise operation (xor, or, and, shift left|right)
  - min
  - max
- Data structures
  - Heaps
  - Trees
  - Map
  - List
  - Array
  - Set
  - TreeSet
  - TreeMap
  - Queue
  - Stack
  - String
  - Boxing, Unboxing (Integer, Long, Math lib)
  - LinkedList (2 pointers, faster and slower pointers, reverse a linked list)

General advice:
- Stay calm and current during the interview
- Fully understand the problem and clarify the edge cases as much as possible
- Come up with more examples if necessary
- Divide the problem into Sub Problems *
- Focus on clean coding
- Do not rush and walk the interviewer through the solution


To be reviewed:
- https://leetcode.com/problems/letter-combinations-of-a-phone-number/submissions/
- https://leetcode.com/problems/continuous-subarray-sum/
- https://leetcode.com/problems/regular-expression-matching/submissions/
